import tensorflow as tf
import tensorflow.contrib.slim as slim
import tensorflow_hub as hub
import numpy as np

from nets.mobilenet import mobilenet_v2


def load_mobilenet_v2(images, is_training=True):
    with tf.contrib.slim.arg_scope(mobilenet_v2.training_scope(is_training=is_training)):
        logits, endpoints = mobilenet_v2.mobilenet(images)

    # the feature map of shape (_,7,7,1280)
    feature_map = endpoints['layer_19']
    return feature_map


def score(feature_vec):
    """
    inputs:
        feature_vec n dimensional vector
    outputs:
        score scalar such that feature_vec (1, n) x W (n, 1)
    Define of reuse variable W of shape (n, 1), and return feature_vec x W
    """
    W = tf.get_variable("score_w",
                        shape=[feature_vec.get_shape()[1], 1],
                        initializer=tf.uniform_unit_scaling_initializer())

    scores = tf.matmul(feature_vec, W, name="score_func")

    return scores


def build_mvfn(feature_map):
    # NOTE
    # mobilenet 224 -> [None, 7, 7, 1280]
    # mobilenet 192 -> [None, 6, 6, 1280]
    # mobilenet 160 -> [None, 5, 5, 1280]
    # mobilenet  96 -> [None, 3, 3, 1280]

    feature_map_shape = feature_map.shape
    spp = []
    with tf.variable_scope('spp'):
        if feature_map_shape[1] >= 5:
            maxpool3 = tf.nn.max_pool(feature_map,
                                    ksize=[1, 5, 5, 1],
                                    strides=[1, 4, 4, 1],
                                    padding='VALID')
            spp.append(maxpool3)

        if feature_map_shape[1] >= 7:
            maxpool2 = tf.nn.max_pool(feature_map,
                                    ksize=[1, 7, 7, 1],
                                    strides=[1, 6, 6, 1],
                                    padding='VALID')
            spp.append(maxpool2)

        if feature_map_shape[1] >= 3:
            maxpool1 = tf.nn.max_pool(feature_map,
                                    ksize=[1, 3, 3, 1],
                                    strides=[1, 2, 2, 1],
                                    padding='VALID')
            spp.append(maxpool1)

        spp = map(lambda x: tf.contrib.layers.flatten(x), spp)
        spp = tf.concat(spp, 1)

    with tf.variable_scope('fc'):
        flattened_dim = int(np.prod(spp.get_shape()[1:]))  
        fc_w = tf.get_variable("fc_w",
                               [flattened_dim, 1000],
                               initializer=tf.uniform_unit_scaling_initializer())  # init_weight((flattened_dim, embedding_dim))
        fc_b = tf.get_variable("fc_b",
                               [1000],
                               initializer=tf.constant_initializer())  # init_bias([embedding_dim])

        fc = tf.nn.relu_layer(spp, fc_w, fc_b)

    q = score(fc)
    return q


def build_loss_matrix(batch_size):
    """
    <-------- batch_size *2 -------->
    [ [1, 0, 0, ... -1,  0,  0, ...],  ^
      [0, 1, 0, ...  0, -1,  0, ...],  |
      [0, 0, 1, ...  0,  0, -1, ...],  |
                    ...                |  batch_size
      [... 1, 0, 0, ...  -1,  0,  0],  |
      [... 0, 1, 0, ...   0, -1,  0],  |
      [... 0, 0, 1, ...   0,  0, -1] ] v
    """
    loss_matrix = np.zeros(
        shape=(batch_size, batch_size * 2), dtype=np.float32)
    for k in range(batch_size):
        loss_matrix[k, k] = 1
        loss_matrix[k, k+batch_size] = -1
    return loss_matrix


def loss(q, batch_size):
    """
    Args:
        q scores (batch_size)
    return:
        L scalar of mean loss over batch
        p batch of score(crop) - score(origin)

    images[0:batch_size-1] are cropped images
    images[batch_size:batch_size*2] are original images
    such that (crop, origin) = (images[n], images[batch_size+n]) for n < batch_size

    same apply for feature_vec, score

    loss_matrix x q = [ score(images[n]) - score(images[batch_size + n]) for n < batch_size ]
                    = [ score(cropped_image) - score(original_image) ]

    p_hinge = [ max(0, 1 + score(cropped_image) - score(original_image)) ] 
        ps. 1 is a parameter, but fixed by vfn to 1

    L = mean over batch of p_hinge 
    reduce L = try to encourage score(original_image) > score(cropped_image)
               such that score(original_image) >= score(cropped_image) + 1
    """
    loss_matrix = build_loss_matrix(batch_size)
    p = tf.matmul(loss_matrix, q)
    zero = tf.constant(0.0, shape=[1], dtype=tf.float32)
    p_hinge = tf.maximum(zero, 1+p)
    L = tf.reduce_mean(p_hinge)
    return L, p, p_hinge