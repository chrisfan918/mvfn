# Mobile View Finding Network MVFN

This repository contians implementation of MVFN and the iOS mobile application for the dissertation. This work is developed based on View Finding Network VFN (https://github.com/yiling-chen/view-finding-network). 

## Dependencies

This repository is using Python 2.7

### Install python dependencies

`pip install -r requirements.txt`

## Training

### Download images

Raw images will be downloaded to `./images`
`python download_images.py`

### Create TFRecords

```bash
python create_dbs.py \
    --trn_db <output_training_tfrecord_file_path> \
    --val_db <output_validation_tfrecord_file_path> \
    --dim <target_input_image_dim>
```

### Train

```bash

python mvfn_train.py \
    --training_db <output_training_tfrecord_file_path> \
    --validation_db <output_validation_tfrecord_file_path> \
    --mobilenet_ckpt "./models/mobilenet_v2_1.0_$dim/mobilenet_v2_1.0_$dim.ckpt" \
    --batch_size_trn 64 \
    --batch_size_val 32 \
    --num_parallel_calls 4 \
    --dim <target_input_image_dim>
```

## Conversion to TFLite

```bash
python ckpt_to_tflite.py \
    --ckpt <CKPT_PATH> \
    --input_node "input_image" \
    --input_batch <INPUT_BATCH_SIZE> \
    --input_dim <INPUT_IMAGE_SIZE> \
    --output_node "ranker/score_func" \
    --output_path <OUTPUT_PATH> \
    --quant <USE_QUANTIZATION_OR_NOT>
```

## Evaluate

### Evaluation for checkpoint with FCDB

```bash
python mvfn_eval_fcdb.py \
    --ckpt <trained_ckpt_path_prefix> \
    --dim <target_input_image_dim>
```

### Evaluation for tflite with FCDB

```bash
python mvfn_eval_fcdb.py \
    --tflite <path_to_tflite_file> \
    --dim <target_input_image_dim>
```

## Setup xCode Project

1. clone Tensorflow (https://github.com/tensorflow/tensorflow) repository to sibling directory
2. build TFLite dynamic library according to official guide (https://www.tensorflow.org/lite/guide/build_ios)
3. open mvfn_ios/mvfn_ios.xcworkspace
4. setup build settings according to official guide (https://www.tensorflow.org/lite/guide/build_ios)
5. config xcode build certificate with apple id
6. run xcode project on physical device


## Pretrained MVFN models locations

`./models_lite`
`./mvfn_ios/mvfn_ios/models`
>>>>>>> init
