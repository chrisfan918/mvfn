# -*- coding: utf-8 -*-
"""
Created on Tue Sep 27 18:28:02 2016

@author: jan
"""
import numpy as np
from skimage import img_as_ubyte, img_as_float
import skimage.transform as transform
import skimage.io as io
import tensorflow as tf
import cPickle as pkl
import os
import re
import argparse
import string
import random
import cv
import logging
import sys

logging.basicConfig(
    level=logging.DEBUG,
    format="[%(asctime)s] %(levelname)s %(message)s",
    datefmt="%H:%M:%S",
    stream=sys.stdout)


def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def _float_feature(value):
    return tf.train.Feature(float_list=tf.train.FloatList(value=[value]))


def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def createExample(filename, image_raw):
    return tf.train.Example(features=tf.train.Features(feature={
        'img_raw': _bytes_feature(image_raw.tostring()),
        'img_file': _bytes_feature(filename)}))


def createExampleFCD(filename, img, img_full, crop, size):
    img_crop = transform.resize(
        img[crop[1]:crop[1]+crop[3], crop[0]:crop[0]+crop[2]], size)
    img_comb = (np.append(img_crop, img_full, axis=2)
                * 255.).astype(np.uint8)
    return createExample(filename, img_comb)


def createExampleRotateCrop(filename, img, img_full, size):
    cv_img = img_as_ubyte(img)
    cv_img = cv.random_rotate_crop(cv_img)
    img_crop = img_as_float(cv_img)
    img_crop = transform.resize(img_crop, size)

    img_comb = (np.append(img_crop, img_full, axis=2)
                * 255.).astype(np.uint8)
    return createExample(filename, img_comb)


def create_database(db, tfrecord_path, image_folder, n_crops, size):

    logging.info("creating {}".format(tfrecord_path))
    logging.info("raw_images={}, n_crops={}, total_entries={}, output_size={}".format(
        len(db), n_crops*2, len(db)*n_crops*2, size))

    entries_count = 0

    with tf.python_io.TFRecordWriter(tfrecord_path) as writer:
        for filename in db:
            img_path = os.path.join(image_folder, filename)
            # skip images of small size, which is very likely to be an image already deleted by user
            img_info = os.stat(img_path)
            if img_info.st_size < 9999:
                continue

            img = io.imread(img_path).astype(np.float32)/255.
            # handle BW
            if img.ndim == 2:
                img = np.expand_dims(img, axis=-1)
                img = np.repeat(img, 3, 2)
            img_full = transform.resize(img, size)

            crops = db[filename][:n_crops]
            for i in range(n_crops):
                try:
                    crop = crops[i]
                    example = createExampleFCD(
                        filename, img, img_full, crop, size)
                    writer.write(example.SerializeToString())
                    entries_count += 1
                except Exception as e:
                    logging.error("Error processing image FCD crop {} of image {}".format(
                        crop, img_path))
                    logging.error(e)
                    pass
                if (entries_count +
                        1) % 100 == 0:
                    logging.info("Wrote {} examples".format(entries_count+1))

                try:
                    example = createExampleRotateCrop(
                        filename, img, img_full, size)
                    writer.write(example.SerializeToString())
                    entries_count += 1
                except Exception as e:
                    logging.error("Error processing image FCD crop {} of image {}".format(
                        crop, img_path))
                    logging.error(e)
                    pass

                if (entries_count +
                        1) % 100 == 0:
                    logging.info("Wrote {} examples".format(entries_count+1))
    return entries_count


def count_tfrecords(path):
    cnt = 0
    for record in tf.python_io.tf_record_iterator(path):
        cnt += 1
    return cnt


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--crop_data", help="Path to crop dataset by filename", type=str, default="dataset_by_filename.pkl")
    parser.add_argument(
        "--image_folder", help="Folder containing training & validation images as downloaded from Flickr", type=str, default="images/")

    parser.add_argument(
        "--trn_db", help="Path to training database", type=str, default="trn.tfrecords")
    parser.add_argument(
        "--val_db", help="Path to validation database", type=str, default="val.tfrecords")

    parser.add_argument(
        "--n_trn", help="Number of images for generating training set", type=int, default=17000)
    parser.add_argument(
        "--n_val", help="Number of images for generating validation set", type=int, default=4040)

    parser.add_argument(
        "--n_crops", help="Number of crops per image per type (Flickr Cropping Dataset Method, and Rotate Crop Method), aka total crops per image = n_crops *2", type=int, default=14)
    parser.add_argument(
        "--dim", help="output dimension of data item N for NxN", type=int, default=224)
    args = parser.parse_args()

    if os.path.isfile(args.trn_db):
        logging.info("removing existing {}".format(args.trn_db))
        os.remove(args.trn_db)
    if os.path.isfile(args.val_db):
        logging.info("removing existing {}".format(args.val_db))
        os.remove(args.val_db)

    logging.info("loading {}".format(args.crop_data))
    with open(args.crop_data, 'r') as f:
        db = pkl.load(f)

    n_images = len(db)
    size = (args.dim, args.dim)
    logging.info("output size: {}".format(size))

    if (n_images < args.n_trn + args.n_val):
        logging.error("Error: {} images available, {} required for train/validation".format(
            n_images, args.n_trn+args.n_val))
        exit()

    # [               db                  ]
    # <-----n_trn-----><--n_val-->
    trn_db = dict(db.items()[:args.n_trn])
    val_db = dict(db.items()[args.n_trn: args.n_trn + args.n_val])

    create_database(
        trn_db,
        args.trn_db,
        args.image_folder,
        args.n_crops,
        size)

    create_database(
        val_db,
        args.val_db,
        args.image_folder,
        args.n_crops,
        size)

    logging.info("{}: {} records".format(
        args.trn_db, count_tfrecords(args.trn_db)))
    logging.info("{}: {} records".format(
        args.val_db, count_tfrecords(args.val_db)))
