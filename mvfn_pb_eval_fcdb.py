import skimage.io as io
import numpy as np
import tensorflow as tf
import skimage.transform as transform
from os.path import join
import network as nw
import argparse
import json
import time
from mvfn import load_mobilenet_v2, score, build_loss_matrix, build_mvfn, loss
import logging
import sys

logging.basicConfig(
    level=logging.DEBUG,
    format="[%(asctime)s] %(levelname)s %(message)s",
    datefmt="%H:%M:%S",
    stream=sys.stdout)

global_dtype = tf.float32
global_dtype_np = np.float32
batch_size = 200


def overlap_ratio(x1, y1, w1, h1, x2, y2, w2, h2):
    intersection = max(0, min(x1 + w1, x2 + w2) - max(x1, x2)) * \
        max(0, min(y1 + h1, y2 + h2) - max(y1, y2))
    union = (w1 * h1) + (w2 * h2) - intersection
    return float(intersection) / float(union)


def evaluate_sliding_window(model, img_filename, crops, dim=224):
    img = io.imread(img_filename).astype(np.float32)
    if img.ndim == 2:  # Handle B/W images
        img = np.expand_dims(img, axis=-1)
        img = np.repeat(img, 3, 2)

    img_crops = np.zeros((batch_size, dim, dim, 3))
    for i in xrange(len(crops)):
        crop = crops[i]
        try:
            img_crop = img[crop[1]:crop[1]+crop[3], crop[0]:crop[0]+crop[2]]
            shape = img_crop.shape
            if (shape[0] == 0 or shape[1] == 0):
                continue
            img_crop = transform.resize(img_crop, (dim, dim))
            img_crop = np.expand_dims(img_crop, axis=0)
            img_crops[i, :, :, :] = img_crop
        except Exception as e:
            print e
	    continue

    # compute ranking scores
    scores = model.run(img_crops)

    # find the optimal crop
    idx = np.argmax(scores[:len(crops)])
    best_window = crops[idx]

    # return the best crop
    return (best_window[0], best_window[1], best_window[2], best_window[3])


def evaluate_FCDB(model, dim):
    slidling_windows_string = open('./sliding_window.json', 'r').read()
    sliding_windows = json.loads(slidling_windows_string)
    def filter_func(item):
        img_filename = join('FCDB', item['filename'])
        if os.stat(img_filename).st_size < 15000:
            # filesize < 15KB are all lost in flickr already
            return False
        else:
            return True
        
    sliding_windows = filter(filter_func, sliding_windows)

    cnt = 0
    alpha = 0.75
    alpha_cnt = 0
    accum_boundary_displacement = 0
    accum_overlap_ratio = 0
    crop_cnt = 0

    logging.info("evaluating {} images".format(len(sliding_windows)))
    for i, item in enumerate(sliding_windows):
        crops = item['crops']
        img_filename = join('FCDB', item['filename'])

        logging.info("evaluating image {} with {} crops ({}/{})".format(
            img_filename, len(crops), i, len(sliding_windows)))

        img = io.imread(img_filename)
        height = img.shape[0]
        width = img.shape[1]

        # ground truth
        x = crops[0][0]
        y = crops[0][1]
        w = crops[0][2]
        h = crops[0][3]

        best_x, best_y, best_w, best_h = evaluate_sliding_window(model,
            img_filename, crops, dim=dim)
        boundary_displacement = (abs(best_x - x) + abs(best_x + best_w - x - w))/float(
            width) + (abs(best_y - y) + abs(best_y + best_h - y - h))/float(height)
        accum_boundary_displacement += boundary_displacement
        ratio = overlap_ratio(x, y, w, h, best_x, best_y, best_w, best_h)
        if ratio >= alpha:
            alpha_cnt += 1
        accum_overlap_ratio += ratio
        cnt += 1
        crop_cnt += len(crops)

        if i % 10 == 0 and i > 0:
            print 'Average overlap ratio: {:.4f}'.format(
                accum_overlap_ratio / cnt)
            print 'Average boundary displacement: {:.4f}'.format(
                accum_boundary_displacement / (cnt * 4.0))
            print 'Alpha recall: {:.4f}'.format(100 * float(alpha_cnt) / cnt)
            print 'Total image evaluated:', cnt
            print 'Average crops per image:', float(crop_cnt) / cnt

    print 'Average overlap ratio: {:.4f}'.format(accum_overlap_ratio / cnt)
    print 'Average boundary displacement: {:.4f}'.format(
        accum_boundary_displacement / (cnt * 4.0))
    print 'Alpha recall: {:.4f}'.format(100 * float(alpha_cnt) / cnt)
    print 'Total image evaluated:', cnt
    print 'Average crops per image:', float(crop_cnt) / cnt

    

def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

class MVFNPB:
    def __init__(self, pb, dim):
        self.pb = pb
        self.dim = dim
        with tf.gfile.GFile(pb, "rb") as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())

        with tf.Graph().as_default() as graph:
            tf.import_graph_def(graph_def, name='')
            self.input_image = graph.get_tensor_by_name("input_image:0")
            self.score_func = graph.get_tensor_by_name("ranker/score_func:0")
            self.sess = tf.Session(graph=graph)
    
    def run(self, images):
        with self.sess.as_default():
            scores = np.zeros(shape=(len(images),))
            avg_time = 0
            for i in range(len(images)):
                img = images[i].astype(np.float32) / 128. - 1
                img_resize = transform.resize(img, (self.dim, self.dim))
                img_resize = np.expand_dims(img_resize, axis=0)
                t1 = time.time()
                scores[i] = self.sess.run(self.score_func, feed_dict={self.input_image: img_resize})
                avg_time += time.time() - t1
            avg_time /= len(images)
            logging.debug("{} takes {:.3f} for each image on average".format(
                self.pb, avg_time))
            return scores

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--pb", help="path to pb file",
                        type=str)
    
    parser.add_argument(
        "--dim", help="Dimension of Network input image", type=int, default=224)

    args = parser.parse_args()
    


    logging.info("pb: {}".format(args.pb))
    start_time = time.time()

    model = MVFNPB(args.pb, args.dim)
    evaluate_FCDB(model, dim=args.dim)
    logging.info("--- %s seconds ---" % (time.time() - start_time))
