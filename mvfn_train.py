import argparse
import tensorflow as tf
import tensorflow.contrib.slim as slim
import tensorflow_hub as hub
import numpy as np
import time
import imp
import logging
import sys
import os.path as path
from mvfn import load_mobilenet_v2, score, build_loss_matrix, build_mvfn, loss

tabulate_available = False
try:
    imp.find_module('tabulate')
    tabulate_available = True
except ImportError:
    pass
if tabulate_available:
    from tabulate import tabulate

logging.basicConfig(
    level=logging.DEBUG,
    format="[%(asctime)s] %(levelname)s %(message)s",
    datefmt="%H:%M:%S",
    stream=sys.stdout)

# susppress tf warnings
tf.logging.set_verbosity(tf.logging.ERROR)

def read_and_decode(filename_queue, dim=224):
    reader = tf.TFRecordReader()
    _, serialized_example = reader.read(filename_queue)
    features = tf.parse_single_example(
        serialized_example,
        # Defaults are not specified since both keys are required.
        features={
            'img_raw': tf.FixedLenFeature([], tf.string),
        })

    image = tf.decode_raw(features['img_raw'], tf.uint8)

    # Convert from [0, 255] -> [-1.0, 1.0] floats.
    image = tf.reshape(image, [dim, dim, 6])
    image = tf.cast(image, tf.float32) / 128.0 - 1
    return tf.split(image, 2, 2)  # 3rd dimension two parts


def read_and_decode_aug(filename_queue, dim=224):
    reader = tf.TFRecordReader()
    _, serialized_example = reader.read(filename_queue)
    features = tf.parse_single_example(
        serialized_example,
        # Defaults are not specified since both keys are required.
        features={
            'img_raw': tf.FixedLenFeature([], tf.string),
        })

    image = tf.decode_raw(features['img_raw'], tf.uint8)
    image = tf.image.random_flip_left_right(tf.reshape(image, [dim, dim, 6]))
    # Convert from [0, 255] -> [-1.0, 1.0] floats.
    image = tf.cast(image, tf.float32) / 128.0 - 1
    image = tf.image.random_brightness(image, 0.01)
    image = tf.image.random_contrast(image, 0.95, 1.05)
    return tf.split(image, 2, 2)  # 3rd dimension two parts


def inputs(filename, batch_size, shuffle=False, aug=False, num_parallel_calls=4, dim=224):

    with tf.name_scope('input'):
        filename_queue = tf.train.string_input_producer(
            [filename], num_epochs=None)

    # Even when reading in multiple threads, share the filename
    # queue.
    if aug:
        crop, full = read_and_decode_aug(filename_queue, dim=dim)
    else:
        crop, full = read_and_decode(filename_queue, dim=dim)

    if shuffle:
        crops, fulls = tf.train.shuffle_batch([crop, full], batch_size=batch_size,
                                              num_threads=num_parallel_calls, capacity=2000 + 4 * batch_size,
                                              enqueue_many=False, min_after_dequeue=1000)
    else:
        crops, fulls = tf.train.batch([crop, full], batch_size=batch_size,
                                      num_threads=num_parallel_calls, capacity=100 + 3 * batch_size,
                                      allow_smaller_final_batch=False)

    return tf.concat([crops, fulls], 0)

""" HELPER FUNCTIONS """


def count_tfrecords(path):
    cnt = 0
    for record in tf.python_io.tf_record_iterator(path):
        cnt += 1
    return cnt


def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--dim",
                        help="input images dimension", type=int, default=224)
    parser.add_argument("--starting_ckpt",
                        help="checkpoint path prefix to begin training with", type=str)
    parser.add_argument("--mobilenet_ckpt", 
                        help="path to mobilenet base check point", type=str)
    parser.add_argument("--validation_interval",
                        help="Number of iterations after which validation is run", type=int, default=500)
    parser.add_argument("--batch_size_trn",
                        help="Batch size for training", type=int, default=15)
    parser.add_argument("--batch_size_val",
                        help="Batch size for validation", type=int, default=5)
    parser.add_argument("--checkpoint_interval",
                        help="Number of iterations after which a checkpoint file is written", type=int, default=1000)
    parser.add_argument("--total_steps",
                        help="Number of total training iterations", type=int, default=15000)
    parser.add_argument("--training_db",
                        help="Path to training database", type=str, default='trn.tfrecords')
    parser.add_argument("--validation_db",
                        help="Path to validation database", type=str, default='val.tfrecords')
    parser.add_argument("--validation_instances",
                        help="validation_instances, default=all of val db", type=int)
    parser.add_argument("--num_parallel_calls",
                        help="num_parallel_calls", type=int, default=4)
    parser.add_argument("--name",
                        help="name for this experiment", type=str)

    args = parser.parse_args()
    dim = args.dim

    if args.validation_instances is None:
        validation_instances = args.validation_instances = count_tfrecords(args.validation_db)
    else:
        validation_instances = args.validation_instances

    experiment_name = "mvfn_{}_{}".format(dim, time.strftime("%Y%m%d%H%M"))
    if args.name is not None:
        experiment_name += "_" + args.name
    tb_log_dir = "./tb_log"

    parameter_table = list()
    parameter_table.append(['Experiment', experiment_name])
    parameter_table.append(['Validation instances', validation_instances])
    args_dict = vars(args)
    for k in sorted(args_dict.keys()):
        parameter_table.append([k, args_dict[k]])
    print tabulate(parameter_table)

    trn_next_batch = inputs(
        args.training_db, args.batch_size_trn, True, True, args.num_parallel_calls, dim=dim)
    val_next_batch = inputs(args.validation_db, args.batch_size_val, False, dim=dim)

    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    sess = tf.Session(config=config)

    input_ph = tf.placeholder(
        dtype=tf.float32,
        shape=(None, dim, dim, 3),
        name="input_image")
    feature_vec = load_mobilenet_v2(input_ph, is_training=True)

    ema = tf.train.ExponentialMovingAverage(0.999)
    variables_to_restore = ema.variables_to_restore()
    # Saver for loading pre-trained mobilenet
    if args.mobilenet_ckpt is not None and args.starting_ckpt is None:
        logging.info("loading mobilenet pretrained weights from {}".format(args.mobilenet_ckpt))
        saver_mobilenet = tf.train.Saver(variables_to_restore)
        saver_mobilenet.restore(sess,  args.mobilenet_ckpt)

    with tf.variable_scope("ranker", reuse=tf.AUTO_REUSE) as scope:
        q = build_mvfn(feature_vec)

    with tf.variable_scope("train"):
        L_op, p_op, p_hinge_op = loss(q, args.batch_size_trn)
        with tf.name_scope('loss'):
            tf.summary.scalar('mean', tf.reduce_mean(L_op))
        with tf.name_scope('p'):
            tf.summary.scalar('mean', tf.reduce_mean(p_op))
            tf.summary.scalar('stddev', tf.sqrt(tf.reduce_mean(tf.square(p_op - tf.reduce_mean(p_op)))))
        non_zero_t = tf.count_nonzero(p_hinge_op)
        batch_size_t = tf.constant(
            args.batch_size_trn, dtype=non_zero_t.dtype)
        accuracy_op = tf.divide(tf.subtract(
            batch_size_t, non_zero_t), batch_size_t)
        tf.summary.scalar('accuracy', accuracy_op)

    with tf.variable_scope("val"):
        L_val_op, p_val_op, p_hinge_val_op = loss(q, args.batch_size_val)
        non_zero_val_t = tf.count_nonzero(p_hinge_val_op)
        batch_size_val_t = tf.constant(
            args.batch_size_val, dtype=non_zero_t.dtype)
        accuracy_val_op = tf.divide(
            tf.subtract(batch_size_val_t, non_zero_val_t),
            batch_size_val_t)
        p_hinge_mean_val_op = tf.reduce_mean(p_hinge_val_op)

    # Make sure batch norm moving_variance and moving_mean update ops are included
    update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
    with tf.control_dependencies(update_ops):
        # using separate learning rate for mobilenet base
        mobilenet_var_list = tf.get_collection(
            tf.GraphKeys.TRAINABLE_VARIABLES, scope='MobilenetV2')
        mobilenet_opt = tf.train.AdamOptimizer(learning_rate=0.0001)
        mobilenet_train_op = mobilenet_opt.minimize(
            L_op, var_list=mobilenet_var_list)

        # using separate learning rate for ranker
        ranker_var_list = tf.get_collection(
            tf.GraphKeys.TRAINABLE_VARIABLES, scope='ranker')
        ranker_opt = tf.train.AdamOptimizer(learning_rate=0.001)
        ranker_train_op = ranker_opt.minimize(L_op, var_list=ranker_var_list)

        train_ops = tf.group(mobilenet_train_op, ranker_train_op)

    init = tf.global_variables_initializer()
    sess.run(init)
    # Saver for regular checkpoint
    saver_ckpt = tf.train.Saver(tf.global_variables())

    if args.starting_ckpt is not None:
        logging.info("loading previous checkpoint {}".format(args.starting_ckpt))
        saver_ckpt.restore(sess, args.starting_ckpt)

    # Saver for best 10 checkpoints based on validation
    saver_best_val = tf.train.Saver(tf.global_variables())
    # for selecting snapshot to save
    best_val_loss = 1e30000  # arbitrary infinity value
    best_val_acc = 0

    # file queue for using dataset api tf.train.string_input_producer
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(sess=sess, coord=coord)

    # Tensorboard
    summary_trn_op = tf.summary.merge_all(scope="train")
    summary_val_op = tf.summary.merge_all(scope="val")

    
    train_writer = tf.summary.FileWriter(path.join(tb_log_dir, experiment_name, "train"),
                                         sess.graph)

    val_writer = tf.summary.FileWriter(path.join(tb_log_dir, experiment_name, "val"),
                                       sess.graph)

    for step in range(args.total_steps+1):
        if step % args.checkpoint_interval == 0 and step >0:
            ckpt_name = saver_ckpt.save(sess,
                            'snapshots/{}.ckpt'.format(experiment_name),
                            global_step=step)
            logging.info("saving checkpoint to {}".format(ckpt_name))
        if step > 0:    
            # Train
            t0 = time.time()
            trn_image_batch = sess.run(trn_next_batch)
            _, loss, acc, summary_train = sess.run(
                [train_ops, L_op, accuracy_op, summary_trn_op],
                feed_dict={input_ph: trn_image_batch})
            t1 = time.time()
            logging.info("Iteration {}: L={:0.4f} acc={:.4f} duration={:0.3f}".format(
                step, loss, acc, t1-t0))
            train_writer.add_summary(summary_train, step)
        # Validation
        if step % args.validation_interval == 0 and step >= 0:
            val_step = step/args.validation_interval
            val_start = time.time()
            loss_val_avg = 0.0
            acc_val_avg = 0.0
            p_hinge_mean_val_avg = 0.0
            p_hinge_zero_count = 0
            evaled = 0
            num_val_batch = args.validation_instances/args.batch_size_val
            image_sums = []
            for k in range(num_val_batch):
                val_batch_start = time.time()
                val_image_batch = sess.run(val_next_batch)
                image_sums.append(val_image_batch.sum())
                loss_val, acc_val, p_hinge_val = sess.run(
                    [L_val_op, accuracy_val_op, p_hinge_val_op],
                    feed_dict={input_ph: val_image_batch})
                val_batch_end = time.time()
                loss_val_avg += loss_val
                acc_val_avg += acc_val
                p_hinge_zero_count += len(p_hinge_val) - np.count_nonzero(p_hinge_val)
                evaled += len(p_hinge_val)
                logging.info("VAL {}: batch={}/{} L={:0.4f} acc={:.4f} overall_acc={}/{} ({:.4f}) duration={:0.3f}".format(
                    val_step, k+1, num_val_batch, loss_val, acc_val, p_hinge_zero_count, evaled, p_hinge_zero_count / float(evaled), val_batch_end - val_batch_start))
            loss_val_avg /= float(num_val_batch)
            acc_val_avg /= float(num_val_batch)
            overall_acc = p_hinge_zero_count / float(evaled)

            val_end = time.time()
            logging.info("VAL {}: AVG L={:0.4f} avg_batch_acc={:.4f} overall_acc={}/{} ({:.4f}) duration={:0.3f}".format(
                val_step, loss_val_avg, acc_val_avg, p_hinge_zero_count, evaled, overall_acc, val_end-val_start))
            # manual log
            summary_val = tf.Summary(value=[
                tf.Summary.Value(tag="val/loss_avg",
                                 simple_value=loss_val_avg),
                tf.Summary.Value(tag="val/accuracy",
                                 simple_value=acc_val_avg)
            ])
            val_writer.add_summary(summary_val, val_step)

            if loss_val_avg <= best_val_loss or acc_val_avg > best_val_acc:
                best_val_acc = acc_val_avg
                best_val_loss = loss_val_avg
                snapshot_name = "snapshots/{}_L_{:.4f}_acc_{:.4f}" \
                    .format(experiment_name, loss_val_avg, acc_val_avg) \
                    .replace(".", "_") + ".ckpt"
                ckpt_name = saver_best_val.save(sess, snapshot_name, global_step=val_step)
                logging.info("saving checkpoint to {}".format(ckpt_name))

    sess.close()
    coord.request_stop()
    coord.join(threads)