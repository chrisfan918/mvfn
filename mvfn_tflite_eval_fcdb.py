import skimage.io as io
import numpy as np
import tensorflow as tf
import skimage.transform as transform
import os
from os.path import join
import argparse
import json
import time
from interpreters import MVFNLite
import logging
import sys

logging.basicConfig(
    level=logging.INFO,
    format="[%(asctime)s] %(levelname)s %(message)s",
    datefmt="%H:%M:%S",
    stream=sys.stdout)

global_dtype = tf.float32
global_dtype_np = np.float32
batch_size = 200


def overlap_ratio(x1, y1, w1, h1, x2, y2, w2, h2):
    intersection = max(0, min(x1 + w1, x2 + w2) - max(x1, x2)) * \
        max(0, min(y1 + h1, y2 + h2) - max(y1, y2))
    union = (w1 * h1) + (w2 * h2) - intersection
    return float(intersection) / float(union)


def evaluate_sliding_window(model, img_filename, crops, dim=224):
    img = io.imread(img_filename).astype(np.float32)
    if img.ndim == 2:  # Handle B/W images
        img = np.expand_dims(img, axis=-1)
        img = np.repeat(img, 3, 2)

    img_crops = np.zeros((batch_size, dim, dim, 3))
    for i in xrange(len(crops)):
        crop = crops[i]
        try:
            img_crop = img[crop[1]:crop[1]+crop[3], crop[0]:crop[0]+crop[2]]
            shape = img_crop.shape
            if (shape[0] == 0 or shape[1] == 0):
                continue
            img_crop = transform.resize(img_crop, (dim, dim))
            img_crop = np.expand_dims(img_crop, axis=0)
            img_crop = img_crop
            img_crops[i, :, :, :] = img_crop
        except Exception as e:
            print e
	    continue

    # compute ranking scores
    scores = [model.run([crop]) for crop in img_crops]


    # find the optimal crop
    idx = np.argmax(scores[:len(crops)])
    print idx
    best_window = crops[idx]

    # return the best crop
    return (best_window[0], best_window[1], best_window[2], best_window[3])


def evaluate_FCDB(model, dim=224):
    slidling_windows_string = open('./sliding_window.json', 'r').read()
    sliding_windows = json.loads(slidling_windows_string)
    def filter_func(item):
        img_filename = join('FCDB', item['filename'])
        if os.stat(img_filename).st_size < 15000:
            # filesize < 15KB are all lost in flickr already
            return False
        else:
            return True
        
    sliding_windows = filter(filter_func, sliding_windows)

    cnt = 0
    alpha = 0.75
    alpha_cnt = 0
    accum_boundary_displacement = 0
    accum_overlap_ratio = 0
    crop_cnt = 0

    logging.info("evaluating {} images".format(len(sliding_windows)))
    for i, item in enumerate(sliding_windows):
        crops = item['crops']
        img_filename = join('FCDB', item['filename'])

        logging.info("evaluating image {} with {} crops ({}/{})".format(
            img_filename, len(crops), i, len(sliding_windows)))

        img = io.imread(img_filename)
        height = img.shape[0]
        width = img.shape[1]

        # ground truth
        x = crops[0][0]
        y = crops[0][1]
        w = crops[0][2]
        h = crops[0][3]

        best_x, best_y, best_w, best_h = evaluate_sliding_window(
            model, img_filename, crops, dim=dim)
        boundary_displacement = (abs(best_x - x) + abs(best_x + best_w - x - w))/float(
            width) + (abs(best_y - y) + abs(best_y + best_h - y - h))/float(height)
        accum_boundary_displacement += boundary_displacement
        ratio = overlap_ratio(x, y, w, h, best_x, best_y, best_w, best_h)
        if ratio >= alpha:
            alpha_cnt += 1
        accum_overlap_ratio += ratio
        cnt += 1
        crop_cnt += len(crops)

        if i % 10 == 0 and i > 0:
            print 'Average overlap ratio: {:.4f}'.format(
                accum_overlap_ratio / cnt)
            print 'Average boundary displacement: {:.4f}'.format(
                accum_boundary_displacement / (cnt * 4.0))
            print 'Alpha recall: {:.4f}'.format(100 * float(alpha_cnt) / cnt)
            print 'Total image evaluated:', cnt
            print 'Average crops per image:', float(crop_cnt) / cnt

    print 'Average overlap ratio: {:.4f}'.format(accum_overlap_ratio / cnt)
    print 'Average boundary displacement: {:.4f}'.format(
        accum_boundary_displacement / (cnt * 4.0))
    print 'Alpha recall: {:.4f}'.format(100 * float(alpha_cnt) / cnt)
    print 'Total image evaluated:', cnt
    print 'Average crops per image:', float(crop_cnt) / cnt

if __name__ == "__main__":

    tf.logging.set_verbosity(tf.logging.ERROR)
    parser = argparse.ArgumentParser()
    parser.add_argument("--tflite", help="Name of the tflite files",
                        type=str)
    parser.add_argument(
        "--dim", help="Dimension of Network input image", type=int)
    args = parser.parse_args()

    tflite_path = args.tflite
    dim = args.dim

    model = MVFNLite(tflite_path, dim=dim)

    start_time = time.time()
    evaluate_FCDB(model=model, dim=args.dim)
    logging.info("--- %s seconds ---" % (time.time() - start_time))
