import skimage.io as io
import numpy as np
import tensorflow as tf
import skimage.transform as transform
from os.path import join
import argparse
import json
import time
from interpreters import MVFN
import logging
import sys

logging.basicConfig(
    level=logging.WARNING,
    format="[%(asctime)s] %(levelname)s %(message)s",
    datefmt="%H:%M:%S",
    stream=sys.stdout)

global_dtype = tf.float32
global_dtype_np = np.float32

def load_tfrecords_batch(tfrecord_path, dim):
    
    def parser_fn(example_proto):
        # Parse the input tf.Example proto using the dictionary above.
        features = tf.parse_single_example(
            example_proto, {
                'img_raw': tf.FixedLenFeature([], tf.string),
            })
        image = tf.decode_raw(features['img_raw'], tf.uint8)

        # Convert from [0, 255] -> [0.0, 1.0] floats.
        image = tf.reshape(image, [dim, dim, 6])
        image = tf.cast(image, tf.float32)
        crop, full = tf.split(image, 2, 2)  # 3rd dimension two parts

        return crop, full

    with tf.name_scope("input"):
        dataset = tf.data.TFRecordDataset(tfrecord_path)
        dataset = dataset \
            .map(map_func=parser_fn, num_parallel_calls=4) \
            .batch(1)
        iterator = dataset.make_one_shot_iterator()
        next_elm = iterator.get_next()

    return next_elm

def count_tfrecords(path):
    cnt = 0
    for record in tf.python_io.tf_record_iterator(path):
        cnt += 1
    return cnt

def eval_val(model, dim, tfrecord_path):
    total_tfrecord = count_tfrecords(tfrecord_path)
    print "{}:{}".format(tfrecord_path, total_tfrecord)
    next_itr = load_tfrecords_batch(tfrecord_path, dim)
    count = 0
    correct = 0
    shouldTerm = False

    with tf.Session() as sess:
        while not shouldTerm:
            if count > 0 and count % 10 == 0:
                print "count={}/{} accuracy={}".format(count,total_tfrecord, correct*1.0/count if count > 0 else 0)
            try:
                crop, full = sess.run(next_itr)
                crop_score = model.run(crop)
                full_score = model.run(full)
                print crop_score, full_score, full_score > crop_score
                count += 1
                if full_score > crop_score:
                    correct += 1
            except tf.errors.OutOfRangeError as e:
                shouldTerm = True
            except Exception as e:
                raise e
                shouldTerm = True
    print "count={} correct={} accuracy={}".format(count, correct, correct*1.0/count if count > 0 else 0)


if __name__ == "__main__":

    tf.logging.set_verbosity(tf.logging.ERROR)
    parser = argparse.ArgumentParser()
    parser.add_argument("--dim", help="Dimension of Network input image", type=int)
    parser.add_argument("--tfrecord", help="Path to tfrecord", type=str)
    parser.add_argument("--ckpt", help="path to checkpoint", type=str)
    parser.add_argument("--tflite", help="Name of the tflite files", type=str)
    parser.add_argument("--pb", help="path to pb file", type=str)
    args = parser.parse_args()

    if args.ckpt is not None:
        logging.info("ckpt: {}".format(args.ckpt))
        model = MVFN(args.ckpt, dim=args.dim)
    elif args.tflite is not None:
        logging.info("tflite: {}".format(args.tflite))
        model = MVFNLite(args.tflite, dim=args.dim)
    elif args.pb is not None:
        logging.info("pb: {}".format(args.pb))
        model = MVFNPB(args.pb, dim=args.dim)
    else:
        raise Exception("model not specified [--ckpt, --tflite, --pb]")
        
    start_time = time.time()
    eval_val(model, args.dim, args.tfrecord)
    logging.info("--- %s seconds ---" % (time.time() - start_time))
