
import mvfn
import numpy as np
import tensorflow as tf
import tabulate
import scipy
import skimage.io as io
import skimage.transform as transform
import os
from os.path import join
import network as nw
import json
import time
import string

import logging

global_dtype = tf.float32
global_dtype_np = np.float32

batch_size = 1


# %%
# ALEXNet


class AlexNet(object):
    def __init__(self, snapshot):
        meta = '{}.meta'.format(snapshot)
        self.snapshot = snapshot
        self.graph = tf.Graph()
        self.sess = tf.Session(graph=self.graph)
        with self.graph.as_default():
            embedding_dim = 1000
            ranking_loss = 'svm'
            net_data = np.load('alexnet.npy', encoding="latin1").item()
            self.image_placeholder = tf.placeholder(
                dtype=global_dtype,
                shape=[batch_size, 227, 227, 3])
            var_dict = nw.get_variable_dict(net_data)
            SPP = True
            pooling = 'max'
            with tf.variable_scope("ranker") as scope:
                feature_vec = nw.build_alexconvnet(
                    self.image_placeholder, var_dict, embedding_dim, SPP=SPP, pooling=pooling)
                self.score_func = nw.score(feature_vec)

            self.sess.run(tf.global_variables_initializer())
            self.saver = tf.train.Saver(tf.global_variables())
            self.saver.restore(self.sess, snapshot)

    def run(self, images):
        scores = np.zeros(shape=(len(images),))
        avg_time = 0
        for i in range(len(images)):
            img = images[i].astype(np.float32)/255 - 0.5
            img_resize = transform.resize(img, (227, 227))
            img_resize = np.expand_dims(img_resize, axis=0)
            t1 = time.time()
            scores[i] = self.sess.run(self.score_func, feed_dict={
                self.image_placeholder: img_resize})[0]
            avg_time += time.time() - t1
        avg_time /= len(images)
        logging.debug("{} takes {:.3f} for each image on average".format(
            self.snapshot, avg_time))
        return scores

def check_images_value_range(images):
    flatten = images[0]
    min_, max_, mean_ = np.min(flatten), np.max(flatten), np.mean(flatten)
    logging.debug("images value min=%.4f max=%.4f mean=%.4f", min_, max_, mean_)
    if 0 <= min_ and min_ <= 255 and \
       0 <= max_ and max_ <= 255 and \
       1 <= mean_ and mean_ <= 255:
       return
    else:
        logging.warning("input images pixel value require range [0, 255], got min=%.4f max=%.4f mean=%.4f", min_, max_, mean_)

# %%
# MVFN

class MVFN(object):
    def __init__(self, snapshot, dim, batch_size=None):
        meta = '{}.meta'.format(snapshot)
        self.snapshot = snapshot
        self.dim = dim
        self.graph = tf.Graph()
        self.sess = tf.Session(graph=self.graph)
        with self.graph.as_default():
            self.image_placeholder = tf.placeholder(
            dtype=global_dtype,
                shape=[batch_size, dim, dim, 3],
                name="input_image")
            embedding_dim = 1000
            feature_vec = mvfn.load_mobilenet_v2(self.image_placeholder, is_training=False)
            with tf.variable_scope("ranker") as scope:
                self.score_func = mvfn.build_mvfn(feature_vec)
            self.sess.run(tf.global_variables_initializer())
            self.saver = tf.train.Saver(tf.global_variables())
            self.saver.restore(self.sess, snapshot)

    def run(self, images):
        check_images_value_range(images)
        with self.sess.as_default():
            scores = np.zeros(shape=(len(images),))
            avg_time = 0
            for i in range(len(images)):
                img = images[i].astype(np.float32) / 128.0 - 1
                img_resize = transform.resize(img, (self.dim, self.dim))
                img_resize = np.expand_dims(img_resize, axis=0)
                t1 = time.time()
                scores[i] = self.sess.run(self.score_func, feed_dict={
                    "input_image:0": img_resize})
                avg_time += time.time() - t1
            avg_time /= len(images)
            logging.debug("{} takes {:.3f} for each image on average".format(
                self.snapshot, avg_time))
            return scores

#%%
class MVFNLite(object):
    def __init__(self, tflite_path, dim):
        self.interpreter = tf.contrib.lite.Interpreter(tflite_path)
        self.interpreter.allocate_tensors()
        self.dim = dim
        self.snapshot = tflite_path

    def run(self, images):
        # check_images_value_range(images)
        self.interpreter.reset_all_variables()
        scores = np.zeros(shape=(len(images),))

        # Get input and output tensors.
        input_details = self.interpreter.get_input_details()
        output_details = self.interpreter.get_output_details()

        avg_time = 0
        for i in range(len(images)):
            img = images[i].astype(np.float32) / 128.0 - 1
            img_resize = transform.resize(img, (self.dim, self.dim))
            img_resize = np.expand_dims(img_resize, axis=0)
            img_resize = img_resize.astype(np.float32)

            t1 = time.time()
            self.interpreter.set_tensor(input_details[0]['index'], img_resize)

            self.interpreter.invoke()
            output_data = self.interpreter.get_tensor(output_details[0]['index'])

            scores[i] = output_data[0][0]
            avg_time += time.time() - t1
        avg_time /= len(images)
        logging.debug("{} takes {:.3f} for each image on average".format(
            self.snapshot, avg_time))
        return scores

class MVFNLiteBatch(object):
    def __init__(self, tflite_path, dim, batch_size):
        self.interpreter = tf.contrib.lite.Interpreter(tflite_path)
        self.interpreter.allocate_tensors()
        self.dim = dim
        self.snapshot = tflite_path
        self.batch_size = batch_size

    def run(self, images):
        check_images_value_range(images)
        self.interpreter.reset_all_variables()
        scores = np.zeros(shape=(len(images),))

        if len(images) != self.batch_size:
            raise Exception("input images {} is not equal to batch_size {}".format(len(images), self.batch_size))

        # Get input and output tensors.
        input_details = self.interpreter.get_input_details()
        output_details = self.interpreter.get_output_details()

        input_tensor = []
        for i in range(len(images)):
            img = images[i].astype(np.float32) / 128.0 - 1
            img_resize = transform.resize(img, (self.dim, self.dim))
            img_resize = np.expand_dims(img_resize, axis=0)
            img_resize = img_resize.astype(np.float32)
            input_tensor[i] = img_resize
        t1 = time.time()
        self.interpreter.set_tensor(input_details[0]['index'], input_tensor)

        self.interpreter.invoke()
        output_data = self.interpreter.get_tensor(output_details[0]['index'])
        scores = output_data[0]
        avg_time = time.time() - t1
        avg_time /= len(images)
        logging.debug("{} takes {:.3f} for each image on average".format(
            self.snapshot, avg_time))
        return scores

class MVFNPB:
    def __init__(self, pb, dim):
        self.pb = pb
        self.dim = dim
        with tf.gfile.GFile(pb, "rb") as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())

        with tf.Graph().as_default() as graph:
            tf.import_graph_def(graph_def, name='')
            self.input_image = graph.get_tensor_by_name("input_image:0")
            self.score_func = graph.get_tensor_by_name("ranker/score_func:0")
            self.sess = tf.Session(graph=graph)
    
    def run(self, images):
        check_images_value_range(images)
        with self.sess.as_default():
            scores = np.zeros(shape=(len(images),))
            avg_time = 0
            for i in range(len(images)):
                img = images[i].astype(np.float32) / 128. - 1
                img_resize = transform.resize(img, (self.dim, self.dim))
                img_resize = np.expand_dims(img_resize, axis=0)
                t1 = time.time()
                scores[i] = self.sess.run(self.score_func, feed_dict={self.input_image: img_resize})
                avg_time += time.time() - t1
            avg_time /= len(images)
            logging.debug("{} takes {:.3f} for each image on average".format(
                self.pb, avg_time))
            return scores

def infer_images(nets, images):
    """
    args:
        nets - dict<net_name, net>
        images - list of np array images sorted by filename
    returns:
        scores - dict<net_name, [scores]> where scores as in order of input images
    """

    scores = {}
    for key, net in nets.items():
        scores[key] = net.run(images)
    return scores

def rank_matrix(scores):
    """
    args:
        scores - dict<net_name, [scores]> where scores as in order of input images
    returns:
        rank_matrix - dict<net_name, [rank]> where rank as in order of input images
    """
    ranks = {}
    for (net, ss) in scores.items():
        sorted_ss = sorted(ss, reverse=True)
        ranks[net] = [sorted_ss.index(s) for s in ss]
    return ranks

def corr_matrix(ranks):
    """
    args:
        rank_matrix - dict<net_name, [rank]> where rank as in order of input images
    returns:
        corr_matrix - dict<net_name, dict<net_name, corr>>
    """
    nets = sorted(ranks.keys())
    corr = {}
    for a in nets:
        if a not in corr:
            corr[a] = {}
        for b in nets:
            footrule = 0
            ranka = ranks[a]
            rankb = ranks[b]
            for i in range(len(ranka)):
                footrule += abs(ranka[i] - rankb[i])
            corr[a][b] = footrule
    return corr