import math
import cv2
import numpy as np
import random
import time
import argparse
import os


def rotate_image(image, angle):
    """
    Rotates an OpenCV 2 / NumPy image about it's centre by the given angle
    (in degrees). The returned image will be large enough to hold the entire
    new image, with a black background
    """

    # Get the image size
    # No that's not an error - NumPy stores image matricies backwards
    image_size = (image.shape[1], image.shape[0])
    image_center = tuple(np.array(image_size) / 2)

    # Convert the OpenCV 3x2 rotation matrix to 3x3
    rot_mat = np.vstack(
        [cv2.getRotationMatrix2D(image_center, angle, 1.0), [0, 0, 1]]
    )

    rot_mat_notranslate = np.matrix(rot_mat[0:2, 0:2])

    # Shorthand for below calcs
    image_w2 = image_size[0] * 0.5
    image_h2 = image_size[1] * 0.5

    # Obtain the rotated coordinates of the image corners
    rotated_coords = [
        (np.array([-image_w2,  image_h2]) * rot_mat_notranslate).A[0],
        (np.array([image_w2,  image_h2]) * rot_mat_notranslate).A[0],
        (np.array([-image_w2, -image_h2]) * rot_mat_notranslate).A[0],
        (np.array([image_w2, -image_h2]) * rot_mat_notranslate).A[0]
    ]

    # Find the size of the new image
    x_coords = [pt[0] for pt in rotated_coords]
    x_pos = [x for x in x_coords if x > 0]
    x_neg = [x for x in x_coords if x < 0]

    y_coords = [pt[1] for pt in rotated_coords]
    y_pos = [y for y in y_coords if y > 0]
    y_neg = [y for y in y_coords if y < 0]

    right_bound = max(x_pos)
    left_bound = min(x_neg)
    top_bound = max(y_pos)
    bot_bound = min(y_neg)

    new_w = int(abs(right_bound - left_bound))
    new_h = int(abs(top_bound - bot_bound))

    # We require a translation matrix to keep the image centred
    trans_mat = np.matrix([
        [1, 0, int(new_w * 0.5 - image_w2)],
        [0, 1, int(new_h * 0.5 - image_h2)],
        [0, 0, 1]
    ])

    # Compute the tranform for the combined rotation and translation
    affine_mat = (np.matrix(trans_mat) * np.matrix(rot_mat))[0:2, :]

    # Apply the transform
    result = cv2.warpAffine(
        image,
        affine_mat,
        (new_w, new_h),
        flags=cv2.INTER_LINEAR
    )

    return result


def largest_rotated_rect(w, h, angle):
    """
    Given a rectangle of size wxh that has been rotated by 'angle' (in
    radians), computes the width and height of the largest possible
    axis-aligned rectangle within the rotated rectangle.

    Original JS code by 'Andri' and Magnus Hoff from Stack Overflow

    Converted to Python by Aaron Snoswell
    """

    quadrant = int(math.floor(angle / (math.pi / 2))) & 3
    sign_alpha = angle if ((quadrant & 1) == 0) else math.pi - angle
    alpha = (sign_alpha % math.pi + math.pi) % math.pi

    bb_w = w * math.cos(alpha) + h * math.sin(alpha)
    bb_h = w * math.sin(alpha) + h * math.cos(alpha)

    gamma = math.atan2(bb_w, bb_w) if (w < h) else math.atan2(bb_w, bb_w)

    delta = math.pi - alpha - gamma

    length = h if (w < h) else w

    d = length * math.cos(alpha)
    a = d * math.sin(alpha) / math.sin(delta)

    y = a * math.cos(gamma)
    x = y * math.tan(gamma)

    return (
        bb_w - 2 * x,
        bb_h - 2 * y
    )


def crop_around_center(image, width, height):
    """
    Given a NumPy / OpenCV 2 image, crops it to the given width and height,
    around it's centre point
    """

    image_size = (image.shape[1], image.shape[0])
    image_center = (int(image_size[0] * 0.5), int(image_size[1] * 0.5))

    if(width > image_size[0]):
        width = image_size[0]

    if(height > image_size[1]):
        height = image_size[1]

    x1 = int(image_center[0] - width * 0.5)
    x2 = int(image_center[0] + width * 0.5)
    y1 = int(image_center[1] - height * 0.5)
    y2 = int(image_center[1] + height * 0.5)

    return image[y1:y2, x1:x2]


def random_crop(image, width, height):
    h, w = image.shape[0:2]
    x_offset_range = w - width
    y_offset_range = h - height

    x = random.randint(0, x_offset_range)
    y = random.randint(0, y_offset_range)

    return image[y:int(y+height), x:int(x+width)]


def demoRotateCrop():
    """
    Demos the largest_rotated_rect function
    """

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-i", "--image", help="image to be transform", type=str)
    args = parser.parse_args()
    image = cv2.imread(args.image)

    image_height, image_width = image.shape[0:2]

    cv2.imshow("Original Image", image)

    print "Press [enter] to begin the demo"
    print "Press [q] or Escape to quit"

    key = cv2.waitKey(0)
    if key == ord("q") or key == 27:
        exit()

    for i in np.arange(0, 360, 0.5):
        image_orig = np.copy(image)
        image_rotated = rotate_image(image, i)
        image_rotated_cropped = crop_around_center(
            image_rotated,
            *largest_rotated_rect(
                image_width,
                image_height,
                math.radians(i)
            )
        )

        key = cv2.waitKey(2)
        if(key == ord("q") or key == 27):
            exit()

        cv2.imshow("Original Image", image_orig)
        cv2.imshow("Rotated Image", image_rotated)
        cv2.imshow("Cropped Image", image_rotated_cropped)

    print "Done"


def demoCrop():

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-i", "--image", help="image to be transform", type=str)
    args = parser.parse_args()
    image = cv2.imread(args.image)

    image_height, image_width = image.shape[0:2]

    key = cv2.waitKey(0)
    if key == ord("q") or key == 27:
        exit()

    for i in np.arange(0, 360, 0.5):
        cropped = random_crop(image, image_width*.5, image_height*.5)
        cv2.imshow("cropped", cropped)
        time.sleep(0.5)


def random_rotate_crop(image, max_angle=45, min_size_ratio=.8):
    angle = random.random() * max_angle*2 - max_angle
    i_h, i_w = image.shape[0:2]

    # rotate and crop center as maximum
    image_rotated = rotate_image(image, angle)
    image_rotated_cropped = crop_around_center(
        image_rotated,
        *largest_rotated_rect(
            i_w,
            i_h,
            math.radians(angle)
        )
    )

    c_h, c_w = image_rotated_cropped.shape[0:2]

    i_aspect = i_w / float(i_h)
    c_aspect = c_w / float(c_h)

    if c_aspect > i_aspect:
        n_w = int(i_aspect * c_h)
        offset = (c_w - n_w) / 2
        cropped = image_rotated_cropped[0:c_h, offset:c_w-offset]
    else:
        n_h = int(c_w / i_aspect)
        offset = (c_h - n_h)/2
        cropped = image_rotated_cropped[offset:c_h - offset, 0:c_w]

    c_h, c_w = cropped.shape[0:2]

    f = random.random() * (1-min_size_ratio) + min_size_ratio
    c_h_, c_w_ = int(c_h*f), int(c_w*f)
    cropped = random_crop(cropped, c_w_, c_h_)

    return cropped


def generate_random_crops():

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-i", "--image", help="image to be transform", type=str)
    parser.add_argument("-o", "--out_dir",
                        help="output dir of images", type=str)
    parser.add_argument("-n", "--number_of_crops",
                        help="number of crops to be generate", type=int, default=5)
    parser.add_argument(
        "--min_size_ratio", help="minimum size ratio of crop", type=float, default=.8)
    parser.add_argument(
        "--min_dim", help="minimum dimension", type=int, default=224)

    args = parser.parse_args()

    if not os.path.exists(args.out_dir):
        os.makedirs(args.out_dir)

    image = cv2.imread(args.image)
    i_h, i_w = image.shape[0:2]
    cv2.imwrite(os.path.join(
        args.out_dir, "_origin_{}x{}.jpg".format(i_w, i_h)), image)

    for i in range(args.number_of_crops):
        cropped = random_rotate_crop(
            image, max_angle=45, min_size_ratio=args.min_size_ratio)
        c_h, c_w = cropped.shape[0:2]
        filename = os.path.join(
            args.out_dir, "r{}_{}x{}.jpg".format(i, c_w, c_h))
        print "saving image " + filename
        cv2.imwrite(filename, cropped)

    for i in range(args.number_of_crops):
        f = random.random() * (1-args.min_size_ratio) + args.min_size_ratio
        cropped = random_crop(image, int(i_w*f), int(i_h*f))
        c_h, c_w = cropped.shape[0:2]
        filename = os.path.join(
            args.out_dir, "c{}_{}x{}.jpg".format(i, c_w, c_h))
        print "saving image " + filename
        cv2.imwrite(filename, cropped)


def generate_view_finding_set():

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-i", "--image", help="image to be transform", type=str)
    parser.add_argument("-o", "--out_dir",
                        help="output dir of images", type=str)

    args = parser.parse_args()

    if not os.path.exists(args.out_dir):
        os.makedirs(args.out_dir)

    image = cv2.imread(args.image)
    i_h, i_w = image.shape[0:2]
    cv2.imwrite(os.path.join(
        args.out_dir, "_origin_{}x{}.jpg".format(i_w, i_h)), image)

    f = .8
    c_h, c_w = int(i_h*f), int(i_w * f)
    offset_x = int(i_w - c_w) / 2
    offset_y = int(i_h - c_h) / 2
    print i_h, i_w
    print c_h, c_w
    print offset_x, offset_y
    # left
    cropped = image[offset_y:offset_y+c_h, 0:c_w]
    h, w = cropped.shape[0:2]
    filename = os.path.join(
        args.out_dir, "c_left_{}x{}.jpg".format(w, h))
    print "saving image " + filename
    cv2.imwrite(filename, cropped)

    # right
    cropped = image[offset_y:offset_y+c_h, offset_x:offset_x+c_w]
    h, w = cropped.shape[0:2]
    filename = os.path.join(
        args.out_dir, "c_right_{}x{}.jpg".format(w, h))
    print "saving image " + filename
    cv2.imwrite(filename, cropped)

    # up
    up = image[0:c_h, offset_x:offset_x+c_w]
    h, w = cropped.shape[0:2]
    filename = os.path.join(
        args.out_dir, "c_up_{}x{}.jpg".format(w, h))
    print "saving image " + filename
    cv2.imwrite(filename, cropped)

    # down
    down = image[offset_y:offset_y+c_h, offset_x:offset_x+c_w]
    h, w = cropped.shape[0:2]
    filename = os.path.join(
        args.out_dir, "c_down_{}x{}.jpg".format(w, h))
    print "saving image " + filename
    cv2.imwrite(filename, cropped)

    # rotate and crop center as maximum
    for angle in [-15, 15]:
        cropped = rotate_image(image, angle)
        cropped = crop_around_center(
            cropped,
            *largest_rotated_rect(
                i_w,
                i_h,
                math.radians(angle)
            )
        )
        
        c_h, c_w = cropped.shape[0:2]

        i_aspect = i_w / float(i_h)
        c_aspect = c_w / float(c_h)

        if c_aspect > i_aspect:
            n_w = int(i_aspect * c_h)
            offset = (c_w - n_w) / 2
            cropped = cropped[0:c_h, offset:c_w-offset]
        else:
            n_h = int(c_w / i_aspect)
            offset = (c_h - n_h)/2
            cropped = cropped[offset:c_h - offset, 0:c_w]
        filename = os.path.join(
            args.out_dir, "r_{}_{:02}_{}x{}.jpg".format(
                "CW" if angle >= 0 else "ACW",
                abs(angle), c_w, c_h))
        print "saving image " + filename
        cv2.imwrite(filename, cropped)


if __name__ == "__main__":
    # demoCrop()
    # demoRotateCrop()

    # generate_random_crops()
    generate_view_finding_set()
