import time
import os
import tensorflow as tf
import mvfn

trained_checkpoint_prefix = '/Users/kyfan/work/view-finding-network/models/mvfn_96_2019_01_23/snapshots/mvfn_96_2019-01-23-03-41.ckpt-15000'
export_dir = '/Users/kyfan/work/view-finding-network/models_lite/mvfn_096_sm'

loaded_graph = tf.Graph()
with tf.Session(graph=loaded_graph) as sess:
    # Restore from checkpoint
    image_placeholder = tf.placeholder(
        dtype=tf.float32,
        shape=[1, 96, 96, 3],
        name="input_image")
    embedding_dim = 1000
    feature_vec = mvfn.load_mobilenet_v2(image_placeholder)
    with tf.variable_scope("ranker") as scope:
        score_func = mvfn.build_mvfn(feature_vec, embedding_dim)
    sess.run(tf.global_variables_initializer())
    saver = tf.train.Saver(tf.global_variables())
    saver.restore(sess, trained_checkpoint_prefix)

    # Export checkpoint to SavedModel
    builder = tf.saved_model.builder.SavedModelBuilder(export_dir)
    default_serving_signature_def = tf.saved_model.build_signature_def(
        inputs={tf.saved_model.signature_constants.CLASSIFY_INPUTS: tf.saved_model.utils.build_tensor_info(image_placeholder)},
        outputs={tf.saved_model.signature_constants.CLASSIFY_OUTPUT_CLASSES: tf.saved_model.utils.build_tensor_info(score_func)},
        method_name=tf.saved_model.signature_constants.CLASSIFY_METHOD_NAME
    )
    builder.add_meta_graph_and_variables(sess,
                                         [tf.saved_model.tag_constants.TRAINING],
                                         strip_default_attrs=True,
                                         signature_def_map={
                                             tf.saved_model.signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY: default_serving_signature_def
                                        },)
    builder.add_meta_graph([
        tf.saved_model.tag_constants.SERVING], strip_default_attrs=True)
    builder.save()