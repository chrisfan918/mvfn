
# %% ALEXNET
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import mvfn
from IPython.display import display, Image
import numpy as np
import tensorflow as tf
import tabulate
import scipy
import skimage.io as io
import skimage.transform as transform
import os
from os.path import join
import network as nw
import json
import time
import string
from interpreters import *

# %% setup networks
alexnet = AlexNet('./models/model-spp-max/model-spp-max')
mvfn_224 = MVFN(
    'models/mvfn_224_2019_01_21/snapshots/mobilenet_v2_224_2019-01-21-14-07.ckpt-15000',
    dim=224)
mvfn_160 = MVFN(
    'models/mvfn_160_2019_01_22/snapshots/mvfn_160_2019-01-22-12-28.ckpt-15000',
    dim=160)
mvfn_096 = MVFN(
    'models/mvfn_96_2019_01_23/snapshots/mvfn_96_2019-01-23-03-41.ckpt-15000',
    dim=96)

mvfnlite_096 = MVFNLite("./models_lite/mvfn_096.tflite", dim=96)
mvfnlite_160 = MVFNLite("./models_lite/mvfn_160.tflite", dim=160)
mvfnlite_224 = MVFNLite("./models_lite/mvfn_224.tflite", dim=224)

networks = {
    "alexnet": alexnet,
    "mvfn_224": mvfn_224,
    "mvfn_160": mvfn_160,
    "mvfn_096": mvfn_096,
    "mvfnl_224": mvfnlite_224,
    "mvfnl_160": mvfnlite_160,
    "mvfnl_096": mvfnlite_096,
}


# %%
test_images_dir = "./test_images/dim_224"
image_dirs = filter(lambda x: os.path.isdir(
    x), [ os.path.join(test_images_dir, x) for x in os.listdir(test_images_dir)])
print image_dirs
# %%
network_names = sorted(networks.keys())

def print_corr_matrix(corr):
    table = [[""]+nets]
    for a in nets:
        row = [a]
        for b in nets:
            row.append(format(corr[a][b]))
        table.append(row)
    print tabulate.tabulate(table)

#%%
def plot_corr_matrix(corr, title="corr_matrix"):
    heat_map_array = np.zeros((len(network_names), len(network_names)))
    for ia, a in enumerate(network_names):
        for ib, b in enumerate(network_names):
            heat_map_array[ia][ib] = corr[a][b]

    fig, ax = plt.subplots()
    im = ax.imshow(heat_map_array)

    # We want to show all ticks...
    ax.set_xticks(np.arange(len(network_names)))
    ax.set_yticks(np.arange(len(network_names)))
    # ... and label them with the respective list entries
    ax.set_xticklabels(network_names)
    ax.set_yticklabels(network_names)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
            rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    for i in range(len(network_names)):
        for j in range(len(network_names)):
            text = ax.text(j, i, heat_map_array[i, j],
                        ha="center", va="center", color="w")

    ax.set_title(title)
    fig.tight_layout()
    plt.show()

acc_corr = {}
for a in network_names:
    if a not in acc_corr:
        acc_corr[a] = {}
    for b in network_names:
        acc_corr[a][b] = 0

results = {}

for image_dir in image_dirs:
    print "processing {} with {} images".format(image_dir, len(image_filenames))
    image_filenames = [join(image_dir, i) for i in os.listdir(
        image_dir) if os.path.isfile(join(image_dir, i)) and string.lower(i).endswith(('.png', '.jpg'))]
    if len(image_filenames) < 2:
        print "skipping directory {} with {} images".format(image_dir, len(image_dir))
        continue
    image_filenames = sorted(image_filenames)
    print image_filenames

    img_dict = dict([(img,  io.imread(img)) for img in image_filenames])
    scores = infer_images(networks, img_dict.values())
    rank = rank_matrix(scores)
    corr = corr_matrix(rank)
    # plot_corr_matrix(corr, title=image_dir)

    results[image_dir] = {
        'scores': scores,
        'rank': rank,
        'corr': corr
    }
    for a in network_names:
        for b in network_names:
            acc_corr[a][b] += corr[a][b]

plot_corr_matrix(acc_corr, title="accumulated corr matrix")

#%% 
minv, maxv = 9999, 0
for a in network_names:
    for b in network_names:
        if acc_corr[a][b] > 0:
            minv = min(minv, acc_corr[a][b])
        maxv = max(maxv, acc_corr[a][b])
norm_acc_corr = {}
for a in network_names:
    if a not in norm_acc_corr:
        norm_acc_corr[a] = {}
    for b in network_names:
        norm_acc_corr[a][b] = int((acc_corr[a][b] - minv) / float(maxv - minv) * 100)
plot_corr_matrix(norm_acc_corr, title="norm acc corr matrix")