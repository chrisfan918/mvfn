//
//  Misc.m
//  mvfn_iosTests
//
//  Created by Chris Fan on 12/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "../mvfn_ios/HeuristicHelper.h"
#import <math.h>
//#import <UIKit/UIKit.h>

@interface Misc : XCTestCase

@end

@implementation Misc

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void) testLargestArea {
    CGSize size = [HeuristicHelper getMaxSizeAfterRotate:CGSizeMake(1500, 500)
                                                angleInRadian:20*M_PI/180];
    NSLog(@"%.6f, %.6f", size.width, size.height);
    
}


@end
