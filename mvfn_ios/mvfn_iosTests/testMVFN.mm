//
//  mvfn.m
//  mvfn_iosTests
//
//  Created by Chris Fan on 13/2/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "../mvfn_ios/MVFNModel.h"
#import "../mvfn_ios/ViewFindingHelper.h"

@interface testMVFN : XCTestCase

@end

@implementation testMVFN

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (UIImage*) loadUIImage: (NSString*) imageName {
    NSBundle *bundle = [NSBundle bundleForClass:[testMVFN class]];
    NSString* imagePath = [bundle pathForResource:imageName.stringByDeletingPathExtension ofType:imageName.pathExtension];
    UIImage* image =  [UIImage imageWithContentsOfFile:imagePath];
    
    if (!image) {
        NSLog(@"cannot load image %@", imageName);
        XCTFail();
        return nil;
    }
    return image;
}

- (void)testInfer {
//    NSArray* imageNames = @[@"__origin.jpg", @"crop_324.jpg",@"crop_325.jpg",@"crop_326.jpg",@"crop_327.jpg"];
    
    NSArray* imageNames = @[@"test.jpg", @"test1.jpg", @"_origin.jpg"];
    NSMutableDictionary* images = [[NSMutableDictionary alloc] init];
    for (NSString * imageName in imageNames) {
        UIImage* image = [self loadUIImage:imageName];
        if (!image) {
            XCTFail("cannot load image");
            return;
        }
        [images setValue:image forKey:imageName];
    }
    
    NSMutableDictionary* scores = [[NSMutableDictionary alloc] init];
    for (NSString* imageName in images){
        [scores setObject:[MVFNModel inferWithImage:images[imageName]] forKey:imageName];
    }

    NSArray *keys = [scores allKeys];
    NSArray *sortedKeys = [keys sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        return [(NSString*) a compare: (NSString*) b];
    }];
    
    for (NSString* imageName in sortedKeys) {
        NSDictionary* score = scores[imageName];
        NSLog(@"%-10s | %@:%@ \t %@:%@ \t %@:%@", [imageName UTF8String], kMVFN096, score[kMVFN096], kMVFN160, score[kMVFN160], kMVFN224, score[kMVFN224]);
    }
}

- (void)testBatchInfer {
    //    NSArray* imageNames = @[@"__origin.jpg", @"crop_324.jpg",@"crop_325.jpg",@"crop_326.jpg",@"crop_327.jpg"];
    
    UIImage* testImage = [self loadUIImage:@"test.jpg"];
    UIImage* test2Image = [self loadUIImage:@"test1.jpg"];
    UIImage* originImage = [self loadUIImage:@"_origin.jpg"];
    NSArray* images = @[
                        testImage,test2Image,testImage,
                        testImage,test2Image,originImage,
                        testImage,test2Image,testImage,
                        ];
    
    for (MVFNModel* model in @[
                               MVFNModel.sharedMVFN096Batch,
                               MVFNModel.sharedMVFN160Batch,
                               MVFNModel.sharedMVFN224Batch,
                               ]){
        NSArray<NSNumber*>* results = [model inferWithImages:images];
        for (int i = 0; i< 9; i++) {
            NSLog(@"%d: %@", i, results[i]);
        }
    }
}

- (void) testViewFinding {
    UIImage* image = [self loadUIImage:@"test.jpg"];
    [ViewFindingHelper findViewInImage:image minScale:0.6 maxScale:0.9 scaleStep:0.15 modelKey:kMVFN096Batch09];
}
@end
