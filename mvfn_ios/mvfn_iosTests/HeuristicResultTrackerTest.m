//
//  HeuristicResultTrackerTest.m
//  mvfn_iosTests
//
//  Created by Chris Fan on 15/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "../mvfn_ios/HeuristicHelper.h"
#import "../mvfn_ios/HeuristicResultTracker.h"

@interface HeuristicResultTrackerTest : XCTestCase

@end

@implementation HeuristicResultTrackerTest

- (void)setUp {
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)testInit {
    HeuristicResultTracker* tracker = [[HeuristicResultTracker alloc] initWithMaxHistoryLength:3 preprocssingMode:HeuristicResultTrackerPreprocessingModeNone aggregationMode:HeuristicResultTrackerAggregationModeLast];
    
    NSAssert(tracker != nil, @"should create tracker");
    NSAssert(tracker.maxHistoryLength == 3, @"should init maxHistoryLength");
    NSAssert(tracker.preprocessingMode == HeuristicResultTrackerPreprocessingModeNone, @"should init preprocessing mode");
    NSAssert(tracker.aggregationMode == HeuristicResultTrackerAggregationModeLast, @"should init aggregation mode");
}

- (void) testHandle {
    NSArray* results = @[
                        [self yieldRandomArrayWithLength:HeuristicModeCount min:-1.5 max:1.5],
                        [self yieldRandomArrayWithLength:HeuristicModeCount min:-1.5 max:1.5],
                        [self yieldRandomArrayWithLength:HeuristicModeCount min:-1.5 max:1.5],
                        ];
    
    HeuristicResultTracker* tracker = [[HeuristicResultTracker alloc] initWithMaxHistoryLength:3 preprocssingMode:HeuristicResultTrackerPreprocessingModeNone aggregationMode:HeuristicResultTrackerAggregationModeLast];
    
    for (int i = 0; i < [results count]; i++) {
        [tracker handle: [results objectAtIndex:i]];
    }
    
    [self assertResultArray:results valuesEqualsTo:tracker.results];
    
    [tracker handle:[results firstObject]];
    
    // test dequeue
    NSAssert([tracker.results count] == 3, @"should maintain max history length");
    [self assertNSNumberNSArray:tracker.results[0] valuesEqualsTo:results[1]];
    [self assertNSNumberNSArray:tracker.results[1] valuesEqualsTo:results[2]];
    [self assertNSNumberNSArray:tracker.results[2] valuesEqualsTo:results[0]];
    
}


- (void) testPreprocessingDiffCenter {
    HeuristicResultTracker* tracker = [[HeuristicResultTracker alloc] initWithMaxHistoryLength:1 preprocssingMode:HeuristicResultTrackerPreprocessingModeCenterDiff aggregationMode:HeuristicResultTrackerAggregationModeLast];
    
    
    NSMutableArray<NSNumber*>* out = [[NSMutableArray alloc] init];
    out[kHeuristicModeCenter] = @1;
    out[kHeuristicModeUpper] = @2;
    out[kHeuristicModeLeft] = @3;
    out[kHeuristicModeRight] = @4;
    out[kHeuristicModeLower] = @5;
    out[kHeuristicModeUpperLeft] = @6;
    out[kHeuristicModeUpperRight] = @7;
    out[kHeuristicModeLowerLeft] = @8;
    out[kHeuristicModeLowerRight] = @9;
    out[kHeuristicModeClockWise] = @10;
    out[kHeuristicModeAntiClockWise] = @11;
    
    HeuristicResult* aggregated = [tracker handle:out];
    
    
    for (int j = 0; j < HeuristicModeCount; j++) {
        NSString* msg = [NSString stringWithFormat:@"%d index, expected: %@, got %@", j, out[j], aggregated[j]];
        NSAssert(aggregated[j].floatValue == out[j].floatValue - 1, msg);
    }
    
}

- (void) testPreprocessingNormalized {
    HeuristicResultTracker* tracker = [[HeuristicResultTracker alloc] initWithMaxHistoryLength:1 preprocssingMode:HeuristicResultTrackerPreprocessingModeCenterDiff aggregationMode:HeuristicResultTrackerAggregationModeLast];
    
    
    NSMutableArray<NSNumber*>* out = [[NSMutableArray alloc] init];
    out[kHeuristicModeCenter] = @1;
    out[kHeuristicModeUpper] = @2;
    out[kHeuristicModeLeft] = @3;
    out[kHeuristicModeRight] = @4;
    out[kHeuristicModeLower] = @5;
    out[kHeuristicModeUpperLeft] = @6;
    out[kHeuristicModeUpperRight] = @7;
    out[kHeuristicModeLowerLeft] = @8;
    out[kHeuristicModeLowerRight] = @9;
    out[kHeuristicModeClockWise] = @10;
    out[kHeuristicModeAntiClockWise] = @11;
    
    HeuristicResult* aggregated = [tracker handle:out];
    
    
    for (int j = 0; j < HeuristicModeCount; j++) {
        #warning TODO
        //        NSString* msg = [NSString stringWithFormat:@"%d index, expected: %@, got %@", j, out[j], aggregated[j]];
        //        NSAssert(aggregated[j].floatValue == out[j].floatValue - 1, msg);
    }
    
}

- (void) testAggregationSum {
    HeuristicResult* sampleResult = [self yieldRandomArrayWithLength:HeuristicModeCount min:-1.5 max:1.5];
    NSArray* results = @[sampleResult, sampleResult, sampleResult];
    
    HeuristicResultTracker* tracker = [[HeuristicResultTracker alloc] initWithMaxHistoryLength:3 preprocssingMode:HeuristicResultTrackerPreprocessingModeNone aggregationMode:HeuristicResultTrackerAggregationModeSum];
    
    HeuristicResult* result = nil;
    for (int i = 0; i < [results count]; i++) {
        result = [tracker handle: [results objectAtIndex:i]];
    }
    
    NSMutableArray* expected = [[NSMutableArray alloc] init];
    for (int i = 0; i < [sampleResult count]; i++) {
        [expected addObject:[NSNumber numberWithFloat:sampleResult[i].floatValue * 3]];
    }
    
    [self assertNSNumberNSArray:result valuesEqualsTo:expected];
}

- (void) testAggregateAverage {
    
    NSArray<HeuristicResult*>* results = @[
                         [self yieldRandomArrayWithLength:HeuristicModeCount min:-1.5 max:1.5],
                         [self yieldRandomArrayWithLength:HeuristicModeCount min:-1.5 max:1.5],
                         [self yieldRandomArrayWithLength:HeuristicModeCount min:-1.5 max:1.5],
                         ];
    
    HeuristicResultTracker* tracker = [[HeuristicResultTracker alloc]
                                       initWithMaxHistoryLength:3
                                       preprocssingMode:HeuristicResultTrackerPreprocessingModeNone
                                       aggregationMode:HeuristicResultTrackerAggregationModeAverage];
    
    HeuristicResult* result = nil;
    for (int i = 0; i < [results count]; i++) {
        result = [tracker handle: [results objectAtIndex:i]];
    }
    
    for (int i = 0; i < [result count]; i++) {
        float mean = 0;
        for (int r = 0; r < [results count]; r++) {
            mean += results[r][i].floatValue;
        }
        mean /= [results count];
        
        NSString* msg = [NSString stringWithFormat:@"index %d different, expected: %f, got: %f", i, mean, result[i].floatValue];
        NSAssert(result[i].floatValue == mean, msg);
    }
    
}

- (bool) assertNSNumberNSArray: (NSArray<NSNumber*>*) a
                valuesEqualsTo: (NSArray<NSNumber*>*) b {
    NSAssert([a count] == [b count], @"should have same length");
    for (int i = 0; i < [a count]; i++) {
        bool cond = a[i].floatValue == b[i].floatValue;
        NSString* msg = [NSString stringWithFormat:@"index %d different, array a: %@ array b: %@", i, a, b];
        NSAssert(cond, msg);
    }
    return true;
}

- (bool) assertResultArray  : (NSArray<HeuristicResult*>*) a
                valuesEqualsTo: (NSArray<HeuristicResult*>*) b {
    NSString* msg =[NSString stringWithFormat: @"should have same length, a: %lu, b: %lu", (unsigned long)[a count], (unsigned long)[b count]];
    NSAssert([a count] == [b count], msg);
    for (int i = 0; i < [a count]; i++) {
        [self assertNSNumberNSArray:a[i] valuesEqualsTo:b[i]];
    }
    return true;
}

- (NSArray<NSNumber*>*) yieldRandomArrayWithLength: (int) length
                                               min: (float) min
                                               max: (float) max {
    NSMutableArray<NSNumber*>* out = [[NSMutableArray alloc] init];
    for (int i = 0; i < length;  i++) {
        [out addObject: [NSNumber numberWithFloat:[self randomFloatBetween:min and:max]]];
    }
    return out;
}

- (float)randomFloatBetween:(float)smallNumber and:(float)bigNumber {
    float diff = bigNumber - smallNumber;
    return (((float) (arc4random() % ((unsigned)RAND_MAX + 1)) / RAND_MAX) * diff) + smallNumber;
}
@end
