//
//  ImageStore.h
//  mvfn_ios
//
//  Created by Chris Fan on 6/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <Photos/Photos.h>
#import "InferredImageData.h"

#ifndef ImageStore_h
#define ImageStore_h

@interface ImageStore : NSObject
@property NSMutableArray<InferredImageData*>* imageData;
@property NSMapTable* scoreCache;

+ (instancetype) shareInstance;

@end

#endif /* ImageStore_h */
