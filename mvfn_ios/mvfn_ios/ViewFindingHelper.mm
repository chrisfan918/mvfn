//
//  ViewFindingHelper.m
//  mvfn_ios
//
//  Created by Chris Fan on 18/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import "ViewFindingHelper.h"
#import "MVFNModel.h"


@implementation VFCandidate
@end


@implementation VFOption: NSObject

+ (instancetype) initWithDefault {
    VFOption* option = [VFOption alloc];
    option.aspectRatio = kAspectRatio_Original;
    option.minScale = .85;
    option.maxScale = .85;
    option.stepScale = .15;
    option.modelKey = kMVFN096Batch09;
    return option;
}
@end

@implementation ViewFindingHelper

+ (NSArray<NSString*>*) kAspectRatios {
 return @[
          kAspectRatio_Original,
          kAspectRatio_1_1,
          kAspectRatio_2_3,
          kAspectRatio_3_4,
          kAspectRatio_4_5,
          kAspectRatio_16_9,
          kAspectRatio_3_2,
          kAspectRatio_4_3,
          kAspectRatio_5_4,
          kAspectRatio_9_16,
        ];
}

+ (UIImage*) preprocessImage: (UIImage*) originalImage {
    // if original image larger than 1024, resize to similiar size
    UIImage* result = originalImage;
    if (originalImage.size.width *
        originalImage.size.height > 1.6e6) {
        CGSize rescaledSize = CGSizeMake(1024, originalImage.size.height / originalImage.size.width * 1024);
        
        NSLog(@"preprocessImage: rescale image from (%.2f, %.2f) to (%.2f, %.2f)", originalImage.size.width, originalImage.size.height, rescaledSize.width, rescaledSize.height);
        
        UIGraphicsBeginImageContext(rescaledSize);
        [originalImage drawInRect:CGRectMake(0, 0, rescaledSize.width, rescaledSize.height)];
        result = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    return result;
}

+ (CGSize) maxSizeForAspectRatioInImage: (UIImage*) image
                            aspectRatio: (NSString*) aspectRatio {
    
    if (aspectRatio == kAspectRatio_Original) {
        return image.size;
    }
    CGFloat widthCoef = 0;
    CGFloat heightCoef = 0;
    
    if (aspectRatio == kAspectRatio_1_1) {
        widthCoef = 1;
        heightCoef = 1;
    }
    else if (aspectRatio == kAspectRatio_2_3) {
        widthCoef = 2;
        heightCoef = 3;
    }
    else if (aspectRatio == kAspectRatio_3_4) {
        widthCoef = 3;
        heightCoef = 4;
    }
    else if (aspectRatio == kAspectRatio_4_5) {
        widthCoef = 4;
        heightCoef = 5;
    }
    else if (aspectRatio == kAspectRatio_16_9) {
        widthCoef = 16;
        heightCoef = 9;
    }
    else if (aspectRatio == kAspectRatio_3_2) {
        widthCoef = 3;
        heightCoef = 2;
    }
    else if (aspectRatio == kAspectRatio_4_3) {
        widthCoef = 4;
        heightCoef = 3;
    }
    else if (aspectRatio == kAspectRatio_5_4) {
        widthCoef = 5;
        heightCoef = 4;
    }
    else if (aspectRatio == kAspectRatio_9_16) {
        widthCoef = 9;
        heightCoef = 16;
    } else {
        NSException *exception = [NSException
                                  exceptionWithName:@"UnknownAspectRatioKeyException"
                                  reason:@"*** maxSizeForAspectRatioInImage(): cannot recognize given aspectRatio key"
                                  userInfo:nil];
        @throw exception;
    }
    
    if (image.size.width > image.size.height) { // landscape
        if (widthCoef > heightCoef) {
            return CGSizeMake(image.size.height, image.size.height / widthCoef * heightCoef);
        } else if (widthCoef < heightCoef) {
            return CGSizeMake(image.size.height / heightCoef * widthCoef, image.size.height);
        } else {
            return CGSizeMake(image.size.height, image.size.height);
        }
    } else if (image.size.width < image.size.height) { // portrait
        if (widthCoef > heightCoef) {
            return CGSizeMake(image.size.width, image.size.width / widthCoef * heightCoef);
        } else if (widthCoef < heightCoef) {
            return CGSizeMake(image.size.width / heightCoef * widthCoef, image.size.width);
        } else {
            return CGSizeMake(image.size.width, image.size.width);
        }
    } else { // square
        if (widthCoef > heightCoef) {
            return CGSizeMake(image.size.width, image.size.width / widthCoef * heightCoef);
        } else if (widthCoef < heightCoef) {
            return CGSizeMake(image.size.width / heightCoef * widthCoef, image.size.width);
        } else {
            return CGSizeMake(image.size.width, image.size.width);
        }
    }
}

+ (NSArray<VFCandidate*>*) findViewInImage:(UIImage *)originalImage
                                withOption:(nonnull VFOption *)option
                          progressCallBack:(nonnull void (^)(int, int))progressCallBack{

    
    
    CGFloat currentScale = option.maxScale;

    // sliding window
    NSMutableArray* candidateRects = [[NSMutableArray alloc] init];
    // default value
    CGFloat minWidthStep = originalImage.size.width * 0.05;
    CGFloat minHeightStep = originalImage.size.height * 0.05;
    while (currentScale >= option.minScale) {
        
        
        CGSize candidateSize = [self maxSizeForAspectRatioInImage:originalImage aspectRatio:option.aspectRatio];
        
        candidateSize.width *= currentScale;
        candidateSize.height *= currentScale;

        NSLog(@"update w:%.2f h:%.2f", candidateSize.width, candidateSize.height);
        
        CGFloat widthRoom = originalImage.size.width - candidateSize.width;
        CGFloat heightRoom = originalImage.size.height - candidateSize.height;
        
        CGFloat widthStep = widthRoom / 10;
        if (widthStep > minWidthStep) {
            widthStep = minWidthStep;
        }
        CGFloat heightStep = heightRoom / 10;
        if (heightStep > minHeightStep) {
            heightStep = minHeightStep;
        }
        
        CGFloat x = 0;
        while (x <= originalImage.size.width - candidateSize.width) {
//            NSLog(@"update w:%.2f h:%.2f x:%.2f", candidateSize.width, candidateSize.height, x);
            CGFloat y = 0;
            while (y <= originalImage.size.height - candidateSize.height) {
                
//                NSLog(@"update w:%.2f h:%.2f x:%.2f y:%.2f", candidateSize.width, candidateSize.height, x, y);
                CGRect rect = CGRectMake(x,
                                         y,
                                         candidateSize.width,
                                         candidateSize.height);
                [candidateRects addObject: [NSValue valueWithCGRect:rect]];
                
                y += heightStep;
            }
            x += widthStep;
        }
        currentScale -= option.stepScale;
    }
    
//    NSLog(@"candidates: %lu", (unsigned long)[candidateRects count]);
//    for (NSValue* value in candidateRects) {
//        CGRect rect = [value CGRectValue];
//        NSLog(@"x:%.2f, y:%.2f, w:%.2f, h:%.2f, r:%.6f", rect.origin.x, rect.origin.y, rect.size.width, rect.size.height, rect.size.width/ rect.size.height);
//    }
    
    // infer
    NSTimeInterval start = [[NSDate new] timeIntervalSince1970];
    
    NSMutableArray<VFCandidate*>* history = [[NSMutableArray alloc] init];
    
    int i = 0;
    int batch_size = 9;
    while (i + batch_size < [candidateRects count]) {
        NSTimeInterval b_start = [[NSDate new] timeIntervalSince1970];
        NSArray* rects_batch = [candidateRects subarrayWithRange:NSMakeRange(i, batch_size)];
        NSArray* image_batch = [self mapRectsToUIImages:rects_batch originalImage:originalImage];
        NSArray<NSNumber*>* results = [MVFNModel inferWithImages:image_batch withModel:option.modelKey];
        for (int r = 0; r < [rects_batch count]; r++) {
            VFCandidate* candidate = [[VFCandidate alloc] init];
            candidate.rect = [rects_batch[r] CGRectValue];
            candidate.score = results[r];
            [history addObject:candidate];
        }
        
        NSTimeInterval b_end = [[NSDate new] timeIntervalSince1970];
        
        if (progressCallBack) {
            dispatch_async(dispatch_get_main_queue(), ^{
                progressCallBack(i+batch_size, (int)[candidateRects count]);
            });
        }
        
//        NSLog(@"candidate %d to %d of %lu took %f ms", i, i+batch_size, (unsigned long)[candidateRects count], b_end - b_start);
        i += batch_size;
    }
    progressCallBack((int)[candidateRects count], (int)[candidateRects count]);
    
    NSTimeInterval end = [[NSDate new] timeIntervalSince1970];
    
    
    [history sortUsingComparator:^NSComparisonResult(id a, id b) {
        NSNumber *first = [(VFCandidate*)a score];
        NSNumber *second = [(VFCandidate*)b score];
        // desc order
        if (first.floatValue > second.floatValue) {
            return NSOrderedAscending;
        } else if (first.floatValue < second.floatValue) {
            return NSOrderedDescending;
        } else {
            return NSOrderedSame;
        }
    }];
    
//    for (VFCandidate* candidate in history) {
//        NSLog(@"x:%.2f, y:%.2f, w:%.2f, h:%.2f, score:%.6f", candidate.rect.origin.x, candidate.rect.origin.y, candidate.rect.size.width, candidate.rect.size.height, [[candidate score] floatValue]);
//    }

    NSLog(@"%@ takes %f to eval %lu candidates", option.modelKey, end - start, (unsigned long)[candidateRects count]);
    
    return history;
}

+ (UIImage*) cropImage: (UIImage *) originalImage
                  rect: (CGRect) rect {
    CGImageRef imageRef = CGImageCreateWithImageInRect(originalImage.CGImage, rect);
    UIImage *result = [UIImage imageWithCGImage:imageRef scale:1 orientation:originalImage.imageOrientation];
    CGImageRelease(imageRef);
    return result;
}

+ (NSArray*) generateCroppingCandidates: (CGSize) original
                                  scale: (CGFloat) scale {
    NSMutableArray* result = [[NSMutableArray alloc] init];
    
    CGSize candidateSize = CGSizeMake(original.width*scale, original.height* scale);
    
    CGFloat widthRoom = original.width - candidateSize.width;
    CGFloat heightRoom = original.height - candidateSize.height;
    
    CGFloat widthRoomStep =  widthRoom / 3;
    CGFloat heightRoomStep = heightRoom / 3;
    
    for (int x = 0; x < 3; x++) {
        for (int y = 0; y < 3; y++) {
            [result addObject: [NSValue valueWithCGRect:CGRectMake(widthRoomStep *x, heightRoomStep*y, candidateSize.width, candidateSize.height)]];
        }
    }
    return result;
}

+ (NSArray<NSNumber*>*) getIndicesSortedDescByValue: (NSArray<NSNumber*>*) array {
    
    NSSortDescriptor * desc = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending: false];
    NSArray<NSNumber*>* sorted = [array sortedArrayUsingDescriptors:[NSArray arrayWithObject: desc]];
    
    NSMutableArray<NSNumber*>* indices = [[NSMutableArray<NSNumber*> alloc] init];
    for (int i = 0; i < [sorted count]; i++) {
        [indices addObject:[NSNumber numberWithInteger:[array indexOfObject:indices[i]]]];
    }
    return indices;
}

+ (NSArray<UIImage*>*) mapRectsToUIImages: (NSArray*) rects
                            originalImage: (UIImage*) originalImage {
    
    NSMutableArray<UIImage*>* results = [[NSMutableArray<UIImage*> alloc] init];
    
    for (NSValue* value in rects) {
        [results addObject:[self cropImage:originalImage rect:[value CGRectValue]]];
    }
    
    return results;
    
}
@end
