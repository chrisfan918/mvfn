//
//  HeuristicCompass.m
//  mvfn_ios
//
//  Created by Chris Fan on 20/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import "HeuristicCompass.h"
#import "HeuristicResultTracker.h"
#import "HeuristicHelper.h"
#import "Config.h"
#import "mixin.h"

@implementation HeuristicCompass

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.isDrawing = false;
        self.backgroundColor = Config.colorForBackground;
        self.thresholdForRotationHeuristic = 0;
        self.thresholdForTranslationHeuristic = 0;
    }
    return self;
}

- (void) drawForHeuristicResult: (HeuristicResult*) results {
    
    self.backgroundColor = Config.colorForBackground;
    if (self.isDrawing) {
        NSLog(@"isDrawing, skip this");
        return;
    }
    self.isDrawing = true;
    self.layer.sublayers = nil;
    
    // normalize without center
    NSMutableArray<NSNumber*>* normalized = [[NSMutableArray<NSNumber*> alloc] init];
    [normalized addObject:[NSNumber numberWithFloat:0]];
    bool anyHeuristicBetter = false;
    for (int mode = 1; mode < [results count]; mode++) {
        float diffCenter = (results[mode].floatValue - results[kHeuristicModeCenter].floatValue);
        float value = (pow(M_E, 2 * diffCenter) - 1) /(pow(M_E, 2 * diffCenter) + 1);
        [normalized addObject:[NSNumber numberWithFloat:value]];
        if (value > 0){
            anyHeuristicBetter = true;
        }
    }
    
    
    CGFloat width = self.frame.size.width;
    CGFloat height = self.frame.size.height;
    
    CGFloat centerX = width / 2;
    CGFloat centerY = height / 2;
    
    
    if (!anyHeuristicBetter) {
        CAShapeLayer *circleLayer = [CAShapeLayer layer];
        CGFloat circleWidth = width / 9 * 2;
        CGFloat circleOriginX = centerX - circleWidth/2;
        CGFloat circleOriginY = centerY - circleWidth/2;
        CGRect rect = CGRectMake(circleOriginX, circleOriginY, circleWidth, circleWidth);
        [circleLayer setPath:[UIBezierPath bezierPathWithOvalInRect: rect].CGPath];
        [circleLayer setFillColor:[[[UIColor greenColor] colorWithAlphaComponent:.8] CGColor]];
        [self.layer addSublayer:circleLayer];
    }
    
    // start from 1, skip drawing center
    for (int mode = 1; mode < HeuristicModeCountWithoutRotation; mode++) {
        CGFloat radian = [HeuristicHelper radianForHeuristicMode:(HeuristicMode
                                                                   ) mode];
        
        CGFloat hypStart = width / 9;
        CGFloat xStart = hypStart * cos(radian) + centerX;
        CGFloat yStart = hypStart * sin(radian) + centerY;

        
        CGFloat hyp = width / 3;
        CGFloat x = hyp * cos(radian) + centerX;
        CGFloat y = hyp * sin(radian) + centerY;
        
        UIBezierPath *path=[UIBezierPath dqd_bezierPathWithArrowFromPoint:CGPointMake(xStart, yStart)
                                                                  toPoint:CGPointMake(x, y)
                                                                tailWidth: width / 9 / 2
                                                                headWidth: width / 9
                                                               headLength: width / 9];
    
        
        CAShapeLayer *shape = [CAShapeLayer layer];
        shape.path = path.CGPath;
        shape.cornerRadius = 10;
        // only do for improvement
        
        if (normalized[mode].floatValue > self.thresholdForTranslationHeuristic) {
            CGFloat value = .7;
            CGFloat alphaValue = .5;
            value += (1-value) * (normalized[mode].floatValue / 2);
            alphaValue = 1;
            shape.fillColor = [UIColor colorWithRed:value green:value blue:value alpha:alphaValue].CGColor;
        } else {
            CGFloat value = .4;
            shape.fillColor = [UIColor colorWithRed:value green:value blue:value alpha:.5].CGColor;
        }
        [self.layer addSublayer:shape];
        
    }
    
    
    // threshold relative to normalized value tanh(diff_center)
    // if rotation heuristic is supplied
    if ([results count] == 11) {
        // draw background
//        [self makeLineLayer:self.layer
//             lineFromPointA:CGPointMake(width/4,0)
//                   toPointB:CGPointMake(width/4*3, 0)
//                  withColor:[UIColor lightGrayColor].CGColor
//                withOpacity:.8
//              withLineWidth:5];
        // draw heuristic meter for anti-clockwise rotation if it is better than center
        CGFloat y = height;
        CGFloat headLength = 8;
        if (normalized[kHeuristicModeAntiClockWise].floatValue > self.thresholdForRotationHeuristic) {
            CGFloat from = width/4;
            CGFloat to = from - headLength - (width/4 - headLength) * normalized[kHeuristicModeAntiClockWise].floatValue;
            
            UIBezierPath *path=[UIBezierPath dqd_bezierPathWithArrowFromPoint:CGPointMake(from, y)
                                                                      toPoint:CGPointMake(to, y)
                                                                    tailWidth: 5
                                                                    headWidth: 8
                                                                   headLength: headLength];
            
            
            CAShapeLayer *shape = [CAShapeLayer layer];
            shape.path = path.CGPath;
            shape.cornerRadius = 10;
            
            shape.fillColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1].CGColor;
            [self.layer addSublayer:shape];
            
        }
        
        // draw heuristic meter for clockwise rotation if it is better than center
        if (normalized[kHeuristicModeClockWise].floatValue > self.thresholdForRotationHeuristic) {
            CGFloat from = width/4*3;
            CGFloat to = from + headLength + (width/4 - headLength) * normalized[kHeuristicModeClockWise].floatValue;
            
            UIBezierPath *path=[UIBezierPath dqd_bezierPathWithArrowFromPoint:CGPointMake(from, y)
                                                                      toPoint:CGPointMake(to, y)
                                                                    tailWidth: 5
                                                                    headWidth: 8
                                                                   headLength: headLength];
            
            
            CAShapeLayer *shape = [CAShapeLayer layer];
            shape.path = path.CGPath;
            shape.cornerRadius = 10;
        
            shape.fillColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1].CGColor;
            [self.layer addSublayer:shape];
        }
        
        // if both rotation is worse than current, draw ok indicator
        if (normalized[kHeuristicModeClockWise].floatValue <= self.thresholdForRotationHeuristic and
            normalized[kHeuristicModeAntiClockWise].floatValue <= self.thresholdForRotationHeuristic) {
            [self makeLineLayer:self.layer
                 lineFromPointA:CGPointMake(width/4, y)
                       toPointB:CGPointMake(width/4 *3, y)
                      withColor:[UIColor greenColor].CGColor
                    withOpacity:.8
                  withLineWidth:5];
        }
    }
    
    self.isDrawing = false;
}

-(void)makeLineLayer:(CALayer *)layer
      lineFromPointA:(CGPoint)pointA
            toPointB:(CGPoint)pointB
           withColor:(CGColor*)color
         withOpacity:(CGFloat) opacity
       withLineWidth:(CGFloat) lineWidth
{
    CAShapeLayer *line = [CAShapeLayer layer];
    UIBezierPath *linePath=[UIBezierPath bezierPath];
    [linePath moveToPoint: pointA];
    [linePath addLineToPoint:pointB];
    line.path=linePath.CGPath;
    line.fillColor = nil;
    line.opacity = opacity;
    line.strokeColor = color;
    line.lineWidth = lineWidth;
    line.cornerRadius = lineWidth/2;
    [layer addSublayer:line];
}


- (void) drawOutterCircle {
    CGFloat width = self.frame.size.width;
    CGFloat height = self.frame.size.height;
    
    CGFloat centerX = width / 2;
    CGFloat centerY = height / 2;
    CAShapeLayer *circleLayer = [CAShapeLayer layer];
    CGFloat circleOriginX = centerX - width/2;
    CGFloat circleOriginY = centerY - width/2;
    [circleLayer setPath:[[UIBezierPath bezierPathWithOvalInRect:CGRectMake(circleOriginX, circleOriginY, width, width)] CGPath]];
    circleLayer.lineWidth = 1.0;
    [circleLayer setStrokeColor:[[UIColor redColor] CGColor]];
    [circleLayer setFillColor:[[UIColor clearColor] CGColor]];
    
    circleLayer.lineJoin = kCALineJoinRound;
    circleLayer.lineDashPattern = [NSArray arrayWithObjects:[NSNumber numberWithInt:2],[NSNumber numberWithInt:3 ], nil];
    [self.layer addSublayer:circleLayer];
}


- (void) drawInnerCircle: (bool) shouldFill {
    CGFloat width = self.frame.size.width;
    CGFloat height = self.frame.size.height;
    
    CGFloat centerX = width / 2;
    CGFloat centerY = height / 2;
    CAShapeLayer *circleLayer = [CAShapeLayer layer];
    CGFloat circleOriginX = centerX - width/4;
    CGFloat circleOriginY = centerY - width/4;
    [circleLayer setPath:[[UIBezierPath bezierPathWithOvalInRect:CGRectMake(circleOriginX, circleOriginY, width/2, width/2)] CGPath]];
    circleLayer.lineWidth = 1.0;
    [circleLayer setStrokeColor:[[UIColor redColor] CGColor]];
    // fill inner circle if there is NOT anyHeuristicBetter
    if (shouldFill) {
        [circleLayer setFillColor:[[UIColor colorWithRed:0 green:1 blue:0 alpha:.5] CGColor]];
    } else {
        [circleLayer setFillColor:[[UIColor clearColor] CGColor]];
    }
    circleLayer.lineJoin = kCALineJoinRound;
    circleLayer.lineDashPattern = [NSArray arrayWithObjects:[NSNumber numberWithInt:2],[NSNumber numberWithInt:3 ], nil];
    [self.layer addSublayer:circleLayer];
}

- (CGPoint) getArrowHeadPointForMode: (HeuristicMode) mode
                             inRect: (CGRect) rect {
    CGFloat s = rect.size.width / 6;
    
    switch (mode) {
        case kHeuristicModeUpper:
            return CGPointMake(s * 3, s    );
        case kHeuristicModeLeft:
            return CGPointMake(s    , s * 3);
        case kHeuristicModeRight:
            return CGPointMake(s * 5, s * 3);
        case kHeuristicModeLower:
            return CGPointMake(s * 3, s * 5);
        case kHeuristicModeUpperLeft:
            return CGPointMake(s * 1, s    );
        case kHeuristicModeUpperRight:
            return CGPointMake(s * 5, s    );
        case kHeuristicModeLowerLeft:
            return CGPointMake(s * 1, s * 5);
        case kHeuristicModeLowerRight:
            return CGPointMake(s * 5, s * 5);
        default:
            return CGPointMake(0,0);
    }
}
@end
