//
//  ViewController.m
//  mvfn_ios
//
//  Created by Chris Fan on 4/2/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

// iOS Objective C libraries
#import "ViewController.h"
#import <AssertMacros.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <CoreImage/CoreImage.h>
#import <ImageIO/ImageIO.h>
#import <Photos/Photos.h>

// c++ library
#include <iostream>

// tensorflow c++ library
#import "tensorflow/lite/kernels/register.h"
#import "tensorflow/lite/model.h"

#define LOG(x) std::cerr

static NSString* model_file_name = @"mvfn_224";
static NSString* model_file_type = @"tflite";

// These dimensions need to match those the model was trained with.
static const int wanted_input_width = 224;
static const int wanted_input_height = 224;
static const int wanted_input_channels = 3;

static NSString* FilePathForResourceName(NSString* name, NSString* extension) {
    NSString* file_path = [[NSBundle mainBundle] pathForResource:name ofType:extension];
    if (file_path == NULL) {
        LOG(FATAL) << "Couldn't find '" << [name UTF8String] << "." << [extension UTF8String]
        << "' in bundle.";
    }
    return file_path;
}



// View Controller

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // register tap gesture recognizer
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap) ];
    [imageView setUserInteractionEnabled:true];
    [imageView addGestureRecognizer:singleTap];
    
    [addButton setTarget:self];
    [addButton setAction:@selector(onTap)];
    
    NSString* graph_path = FilePathForResourceName(model_file_name, model_file_type);
    model = tflite::FlatBufferModel::BuildFromFile([graph_path UTF8String]);
    if (!model) {
        LOG(FATAL) << "Failed to mmap model " << graph_path;
    }
    LOG(INFO) << "Loaded model " << graph_path;
    model->error_reporter();
    LOG(INFO) << "resolved reporter";
    
    tflite::ops::builtin::BuiltinOpResolver resolver;
    
    tflite::InterpreterBuilder(*model, resolver)(&interpreter);
    if (!interpreter) {
        LOG(FATAL) << "Failed to construct interpreter";
    }
    if (interpreter->AllocateTensors() != kTfLiteOk) {
        LOG(FATAL) << "Failed to allocate tensors!";
    }
    [self UpdatePhoto];
}

- (void) onTap {

    UIImagePickerController *pickerView = [[UIImagePickerController alloc] init];
    pickerView.allowsEditing = NO;
    pickerView.delegate = self;
    [pickerView setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [self presentViewController:pickerView animated:YES completion:nil];
}


#pragma mark - PickerDelegates

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
//    UIImage * img = [info valueForKey:UIImagePickerControllerEditedImage];
    UIImage * img = [info valueForKey:UIImagePickerControllerOriginalImage];
    
    CGSize size = CGSizeMake(224, 224);
    UIGraphicsBeginImageContext(size);
    [img drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage * croppedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
//    CGImageRef imageRef = CGImageCreateWithImageInRect([img CGImage], CGRectMake(0, 0, 224, 224));
//    UIImage *croppedImage = [UIImage imageWithCGImage:imageRef];
    
    imageView.image = croppedImage;
    
    [self inputImageToModel:[self CGImageToPixels:[croppedImage CGImage]]];
    
    [self runModel];
    
//    CGImageRelease(imageRef);
    
}

- (void) updatePhotosLibrary{
    PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
    fetchOptions.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]];
    photos = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:fetchOptions];
}

- (UIImage *) convertImageFromAsset:(PHAsset *)asset
                         targetSize:(CGSize) targetSize
                               mode:(PHImageContentMode) mode{
    PHImageManager * manager = [[PHImageManager alloc] init];
    PHImageRequestOptions * options = [[PHImageRequestOptions alloc] init];
    NSMutableArray * images = [[NSMutableArray alloc] init];
    NSMutableArray * infos = [[NSMutableArray alloc] init];
    
    options.synchronous = TRUE;
    
    [manager requestImageForAsset:asset
                       targetSize:targetSize
                      contentMode:mode
                          options:options
                    resultHandler:^(UIImage *image, NSDictionary *info){
                        [images addObject:image];
                        [infos addObject:info];
                    }
     ];
    
    UIImage *result = images[0];
    
    return result;
}


- (void)UpdatePhoto{
    PHAsset* asset;
    if (photos==nil || photos_index >= photos.count){
        [self updatePhotosLibrary];
        photos_index=0;
    }
    if (photos.count){
        asset = photos[photos_index];
        photos_index += 1;
        input_image = [self convertImageFromAsset:asset
                                       targetSize:CGSizeMake(wanted_input_width, wanted_input_height)
                                             mode:PHImageContentModeAspectFill];
    }
    
    if (input_image != nil){
        image_data image = [self CGImageToPixels:input_image.CGImage];
        [self inputImageToModel:image];
        [self runModel];
    }
}

- (image_data)CGImageToPixels:(CGImage *)image {
    image_data result;
    result.width = (int)CGImageGetWidth(image);
    result.height = (int)CGImageGetHeight(image);
    result.channels = 4;

    CGColorSpaceRef color_space = CGColorSpaceCreateDeviceRGB();
    const int bytes_per_row = (result.width * result.channels);
    const int bytes_in_image = (bytes_per_row * result.height);
    result.data = std::vector<uint8_t>(bytes_in_image);
    const int bits_per_component = 8;
    
    CGContextRef context =
    CGBitmapContextCreate(result.data.data(), result.width, result.height, bits_per_component, bytes_per_row,
                          color_space, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(color_space);
    CGContextDrawImage(context, CGRectMake(0, 0, result.width, result.height), image);
    CGContextRelease(context);
    
    return result;
}


- (void)inputImageToModel:(image_data)image{
    float* out = interpreter->typed_input_tensor<float>(0);
    
    assert(image.channels >= wanted_input_channels);
    uint8_t* in = image.data.data();
    
    for (int y = 0; y < wanted_input_height; ++y) {
        const int in_y = (y * image.height) / wanted_input_height;
        uint8_t* in_row = in + (in_y * image.width * image.channels);
        float* out_row = out + (y * wanted_input_width * wanted_input_channels);
        for (int x = 0; x < wanted_input_width; ++x) {
            const int in_x = (x * image.width) / wanted_input_width;
            uint8_t* in_pixel = in_row + (in_x * image.channels);
            float* out_pixel = out_row + (x * wanted_input_channels);
            for (int c = 0; c < wanted_input_channels; ++c) {
                out_pixel[c] = in_pixel[c];
            }
        }
    }
}

- (void)runModel {
    double startTimestamp = [[NSDate new] timeIntervalSince1970];
    if (interpreter->Invoke() != kTfLiteOk) {
        LOG(FATAL) << "Failed to invoke!";
    }
    double endTimestamp = [[NSDate new] timeIntervalSince1970];
    total_latency += (endTimestamp - startTimestamp);
    total_count += 1;
    NSLog(@"Time: %.4lf, avg: %.4lf, count: %d", endTimestamp - startTimestamp,
          total_latency / total_count,  total_count);
    
    float* output = interpreter->typed_output_tensor<float>(0);
    
    NSLog(@"score = %.2f", *output);
    [label setText:[NSString stringWithFormat:@"score = %.2f", *output ]];
}

@end
