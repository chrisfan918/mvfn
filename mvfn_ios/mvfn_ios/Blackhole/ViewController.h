//
//  ViewController.h
//  mvfn_ios
//
//  Created by Chris Fan on 4/2/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>

#import "tensorflow/lite/kernels/register.h"
#import "tensorflow/lite/model.h"


typedef struct {
    int width;
    int height;
    int channels;
    std::vector<uint8_t> data;
} image_data;


@interface ViewController : UIViewController<
UIGestureRecognizerDelegate,
UIActionSheetDelegate,
UINavigationControllerDelegate,
UIImagePickerControllerDelegate> {
    std::unique_ptr<tflite::FlatBufferModel> model;
    tflite::ops::builtin::BuiltinOpResolver resolver;
    std::unique_ptr<tflite::Interpreter> interpreter;
    
    
    double total_latency;
    int total_count;
    
    int photos_index;
    PHFetchResult *photos;
    
    UIImage* input_image;
    UIImage* display_image;
    __weak IBOutlet UIImageView *imageView;
    __weak IBOutlet UILabel *label;
    __weak IBOutlet UIBarButtonItem *addButton;
}
@end

