//
//  HeuristicResultTracker.m
//  mvfn_ios
//
//  Created by Chris Fan on 15/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//
#import "HeuristicHelper.h"
#import "HeuristicResultTracker.h"

@implementation HeuristicResultTracker

- (instancetype)initWithMaxHistoryLength:(int)historyLength
                        preprocssingMode: (HeuristicResultTrackerPreprocessingMode) preprocessingMode
                         aggregationMode: (HeuristicResultTrackerAggregationMode) aggregationMode;{
    self = [super init];
    if (self) {
        self.maxHistoryLength = historyLength;
        self.preprocessingMode = preprocessingMode;
        self.aggregationMode = aggregationMode;
        self.results = [[NSMutableArray alloc] init];
    }
    return self;
}

- (HeuristicResult *)handle:(HeuristicResult *)result
{
    [self.results enqueue:[self preprocessResult:result]];
    while ([self.results count] > self.maxHistoryLength) {
        [self.results dequeue];
    }
    return [self aggregateResult];
}

- (HeuristicResult *) preprocessResult: (HeuristicResult*) input {
    switch (self.preprocessingMode) {
        case HeuristicResultTrackerPreprocessingModeNormalize:
            return [self normalize:input];
        case HeuristicResultTrackerPreprocessingModeCenterDiff:
            return [self diffResultAgainstCenter:input];
        default:
            return input;
    }
}

- (HeuristicResult *) aggregateResult {
    switch (self.aggregationMode) {
        case HeuristicResultTrackerAggregationModeSum:
            return [self sumResults];
        case HeuristicResultTrackerAggregationModeAverage:
            return [self averageResults];
        case HeuristicResultTrackerAggregationModeLast:
            return [self.results lastObject];
        default:
            return nil;
    }
}


- (HeuristicResult*) diffResultAgainstCenter: (HeuristicResult*) input {
    NSMutableArray<NSNumber*>* output = [[NSMutableArray<NSNumber*> alloc]init];
    float centerValue = input[kHeuristicModeCenter].floatValue;
    for (int mode = 0; mode < HeuristicModeCount; mode++) {
        [output addObject:[NSNumber numberWithFloat: input[mode].floatValue - centerValue]];
    }
    return output;
}

- (HeuristicResult*) normalize: (HeuristicResult*) input {
    NSMutableArray<NSNumber*>* output = [[NSMutableArray<NSNumber*> alloc]init];
    float mean = 0;
    for (int mode = 0; mode < HeuristicModeCount; mode++) {
        mean += input[mode].floatValue;
    }
    mean /= HeuristicModeCount;
    for (int mode = 0; mode < HeuristicModeCount; mode++) {
        [output addObject:[NSNumber numberWithFloat: (input[mode].floatValue - mean) / mean]];
    }
    
    return output;
}

- (HeuristicResult*) sumResults {
    NSMutableArray<NSNumber*>* output = [[NSMutableArray<NSNumber*> alloc] init];
    for (int i = 0; i < HeuristicModeCount; i++) {
        [output addObject:[NSNumber numberWithInt:0]];
    }
    for (int i = 0; i < [self.results count]; i++) {
        HeuristicResult* result = [self.results objectAtIndex:i];
        for (int j = 0; j < HeuristicModeCount; j++) {
            output[j] = [NSNumber numberWithFloat: output[j].floatValue + result[j].floatValue];
        }
    }
    return output;
}

- (HeuristicResult*) averageResults {
    NSMutableArray<NSNumber*>* output = (NSMutableArray<NSNumber*>*) [self sumResults];
    for (int j = 0; j < HeuristicModeCount; j++) {
        output[j] = [NSNumber numberWithFloat: output[j].floatValue / [self.results count]];
    }
    return output;
}

@end
