//
//  LiveHeuristicConfigViewController.m
//  mvfn_ios
//
//  Created by Chris Fan on 23/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import "LiveHeuristicConfigViewController.h"
#import "MVFNModel.h"
#import "ViewFindingHelper.h"
#import "Config.h"
#import "mixin.h"

@interface LiveHeuristicConfigViewController () <UIPickerViewDataSource, UIPickerViewDelegate>
@property NSArray<NSString*>* availableModelKeys;

@end

@implementation LiveHeuristicConfigViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.showGridViewSwitch setOn:false];
    self.availableModelKeys = self.delegate.supportedModels;
    self.modelKeyPickerView.dataSource = self;
    self.modelKeyPickerView.delegate = self;
    [self.modelKeyPickerView reloadAllComponents];

    [self.trackerHistoryLimitTextField addDoneToolBar];
    [self.translationThresholdTextField addDoneToolBar];
    [self.rotationThresholdTextField addDoneToolBar];
    
    UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onViewTapped:)];
    [self.view addGestureRecognizer:tapGesture];
    
    self.view.backgroundColor = [[Config colorForBackground] colorWithAlphaComponent:.8];
    
    self.modalBaseView.layer.cornerRadius = 10;
    self.modalBaseView.clipsToBounds = true;
}

- (void)viewWillAppear:(BOOL)animated {
    // set value to delegate
    
    [self.showGridViewSwitch setOn:self.delegate.heuristicResultGridView.shouldShow or self.delegate.trackerHeuristicResultGridView.shouldShow];
    self.trackerHistoryLimitTextField.text = [NSString stringWithFormat:@"%d", self.delegate.tracker.maxHistoryLength];
    self.translationThresholdTextField.text = [NSString stringWithFormat:@"%.2f", self.delegate.compass.thresholdForTranslationHeuristic];
    self.rotationThresholdTextField.text = [NSString stringWithFormat:@"%.2f", self.delegate.compass.thresholdForRotationHeuristic];
    [self.modelKeyPickerView selectRow:[self.availableModelKeys indexOfObject:self.delegate.modelKey]
                           inComponent:0
                              animated:true];
}

- (void)viewDidDisappear:(BOOL)animated {
    self.delegate.shouldInfer = true;
}

- (void) onViewTapped: (UITapGestureRecognizer*) sender {
    auto loc = [sender locationInView:self.view];
    if (!CGRectContainsPoint(self.modalBaseView.frame, loc)) {
        [self dismissViewControllerAnimated:true completion:nil];
    }
}

- (IBAction)onShowGridViewSwitchValueChange:(id)sender {
    [self.delegate.heuristicResultGridView setShouldShow:self.showGridViewSwitch.on];
    [self.delegate.trackerHeuristicResultGridView setShouldShow:self.showGridViewSwitch.on];
}

- (IBAction)setButtonDidPressed:(id)sender {
    NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
    self.delegate.tracker.maxHistoryLength = MAX(0,(int)[[formatter numberFromString: [self.trackerHistoryLimitTextField text]] doubleValue]);
    
    self.delegate.compass.thresholdForTranslationHeuristic = MAX(0,(int)[[formatter numberFromString: [self.translationThresholdTextField text]] doubleValue]);
    self.delegate.compass.thresholdForRotationHeuristic = MAX(0,(int)[[formatter numberFromString: [self.rotationThresholdTextField text]] doubleValue]);
    
    self.delegate.modelKey = self.availableModelKeys[[self.modelKeyPickerView selectedRowInComponent:0]];
    
    [self.delegate.heuristicResultGridView setShouldShow:self.showGridViewSwitch.on];
    [self.delegate.trackerHeuristicResultGridView setShouldShow:self.showGridViewSwitch.on];
    [self dismissViewControllerAnimated:true completion:nil];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.availableModelKeys count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return self.availableModelKeys[row];
}
@end
