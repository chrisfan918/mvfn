//
//  LiveHeuristicConfigViewController.h
//  mvfn_ios
//
//  Created by Chris Fan on 23/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveHeuristicViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface LiveHeuristicConfigViewController : UIViewController

@property (weak, nonatomic) IBOutlet UISwitch *showGridViewSwitch;
@property (weak, nonatomic) IBOutlet UITextField *translationThresholdTextField;
@property (weak, nonatomic) IBOutlet UITextField *rotationThresholdTextField;
@property (weak, nonatomic) IBOutlet UITextField *trackerHistoryLimitTextField;
@property (weak, nonatomic) IBOutlet UIPickerView *modelKeyPickerView;
@property (weak, nonatomic) IBOutlet UIButton *setButton;
@property (weak, nonatomic) IBOutlet UIView *modalBaseView;

@property (weak) LiveHeuristicViewController* delegate;
@end

NS_ASSUME_NONNULL_END
