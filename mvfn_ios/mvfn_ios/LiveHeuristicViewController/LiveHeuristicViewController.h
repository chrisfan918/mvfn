//
//  LiveHeuristicViewController.h
//  mvfn_ios
//
//  Created by Chris Fan on 13/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import "HeuristicResultGridView.h"
#import "HeuristicHelper.h"
#import "HeuristicResultTracker.h"
#import "HeuristicCompass.h"
#import "MVFNModel.h"
#import "LiveHeuristicViewController.h"
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveHeuristicViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *previewView;
@property (weak, nonatomic) IBOutlet HeuristicResultGridView *heuristicResultGridView;
@property (weak, nonatomic) IBOutlet HeuristicResultGridView *trackerHeuristicResultGridView;
@property (weak, nonatomic) IBOutlet HeuristicCompass *compass;
@property (weak, nonatomic) IBOutlet UIButton *configButton;
@property (weak, nonatomic) IBOutlet UIButton *shutterButton;
@property (atomic) NSString* modelKey;
@property NSArray<NSString* >* supportedModels;
@property (atomic) BOOL isInfering;
@property (atomic) BOOL shouldInfer;
@property (atomic) UIDeviceOrientation deviceOrientationWhenShutterFires;
@property double labelLastUpdatedAt;

@property HeuristicResultTracker* tracker;
@property UIImage* currentFrame;
@property AVCaptureSession* session;
@property AVCaptureConnection* connection;
@property AVCaptureVideoDataOutput* videoOutput;
@property AVCaptureVideoPreviewLayer* videoPreviewLayer;
@property AVCapturePhotoOutput* photoOutput;
@end

NS_ASSUME_NONNULL_END
