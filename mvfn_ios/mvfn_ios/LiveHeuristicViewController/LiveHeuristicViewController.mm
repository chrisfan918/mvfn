//
//  LiveHeuristicViewController.m
//  mvfn_ios
//
//  Created by Chris Fan on 13/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//
#import "HeuristicHelper.h"
#import "HeuristicCompass.h"
#import "HeuristicResultTracker.h"
#import "MVFNModel.h"
#import "ImageStore.h"
#import "LiveHeuristicViewController.h"
#import "LiveHeuristicConfigViewController.h"
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "Config.h"
#import "AVHelper.h"

@interface LiveHeuristicViewController () <AVCaptureVideoDataOutputSampleBufferDelegate, AVCapturePhotoCaptureDelegate>

@end

@implementation LiveHeuristicViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Camera";
    [self.view setBackgroundColor: Config.colorForBackground];
    
    self.isInfering = false;
    self.labelLastUpdatedAt = -1;
    self.supportedModels = MVFNModel.availableModelsForBatch11;
    self.modelKey = self.supportedModels[0];
    self.tracker = [[HeuristicResultTracker alloc] initWithMaxHistoryLength: 5
                                                           preprocssingMode: HeuristicResultTrackerPreprocessingModeCenterDiff
                                                            aggregationMode: HeuristicResultTrackerAggregationModeAverage];
    
    [self.trackerHeuristicResultGridView setShouldShow:false];
    [self.heuristicResultGridView setShouldShow:false];
    
    AVCaptureDevice* captureDevice = [AVCaptureDevice defaultDeviceWithMediaType: AVMediaTypeVideo];
    AVCaptureDeviceInput* input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:nil];
    self.session = [[AVCaptureSession alloc] init];
    self.session.sessionPreset = AVCaptureSessionPresetPhoto;
    [self.session addInput:input];
    
    self.previewView.layer.cornerRadius = 10;
    self.previewView.layer.masksToBounds = true;
    
    self.videoPreviewLayer = [AVCaptureVideoPreviewLayer layerWithSession:self.session];
    self.videoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    self.videoPreviewLayer.frame = self.previewView.layer.bounds;
    [self.previewView.layer addSublayer:self.videoPreviewLayer];
    
    self.videoOutput = [[AVCaptureVideoDataOutput alloc] init];
    [self.videoOutput setSampleBufferDelegate:self queue:dispatch_queue_create("sample buffer", nil)];
    if ([self.session canAddOutput:self.videoOutput]) {
        [self.session addOutput:self.videoOutput];
    } else {
        NSLog(@"cannot add AVCaptureVideoDataOutput to AVCaptureSession");
    }
    self.connection = [self.videoOutput connectionWithMediaType:AVMediaTypeVideo];
    
    // offset for AVHelper:CorrectImage
    self.connection.videoOrientation = AVCaptureVideoOrientationLandscapeRight;
    
    
    self.photoOutput = [[AVCapturePhotoOutput alloc] init];
    [self.photoOutput setLivePhotoCaptureEnabled:false];
    [self.photoOutput setHighResolutionCaptureEnabled:false];
    if ([self.session canAddOutput:self.photoOutput]) {
        [self.session addOutput:self.photoOutput];
    } else {
        NSLog(@"cannot add AVCapturePhotoOuput to AVCaptureSession");
    }
    
    self.shouldInfer = false;
    
    auto tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onPreviewViewTapped)];
    [self.previewView setUserInteractionEnabled: true];
    [self.previewView addGestureRecognizer:tapRecognizer];
    
    
    self.deviceOrientationWhenShutterFires = [[UIDevice currentDevice] orientation];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleRotation)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    self.shouldInfer = false;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.session stopRunning];
    });
    NSLog(@"viewWillDisappear: session stopRunning");
}

- (void)viewWillAppear:(BOOL)animated{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.session != nil and ![self.session isRunning]) {
            [self.session startRunning];
            NSLog(@"viewDidAppear: session startRunning");
        }
    });
    self.shouldInfer = false;
}

- (void)viewDidAppear:(BOOL)animated {
    self.shouldInfer = true;
}

- (void) onPreviewViewTapped {
    if ([self.session isRunning]) {
        [self.session  stopRunning];
        self.title = @"paused";
        NSLog(@"onPreviewViewTapped: session stopRunning");
    } else {
        [self.session startRunning];
        self.title = @"running";
        NSLog(@"onPreviewViewTapped: session startRunning");
    }
}

- (void) handleRotation {
    
    UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
    switch (deviceOrientation) {
        case UIDeviceOrientationLandscapeLeft:
            self.heuristicResultGridView.view.transform = CGAffineTransformMakeRotation(90 * M_PI /180);
            self.trackerHeuristicResultGridView.view.transform = CGAffineTransformMakeRotation(90 * M_PI /180);
            self.compass.transform = CGAffineTransformMakeRotation(90 * M_PI /180);
            self.configButton.transform = CGAffineTransformMakeRotation(90 * M_PI /180);
            break;
        case UIDeviceOrientationLandscapeRight:
            self.heuristicResultGridView.view.transform = CGAffineTransformMakeRotation(-90 * M_PI /180);
            self.trackerHeuristicResultGridView.view.transform = CGAffineTransformMakeRotation(-90 * M_PI /180);
            self.compass.transform = CGAffineTransformMakeRotation(-90 * M_PI /180);
            self.configButton.transform = CGAffineTransformMakeRotation(-90 * M_PI /180);
            break;
        case UIDeviceOrientationPortrait:
            self.heuristicResultGridView.view.transform = CGAffineTransformMakeRotation(0 * M_PI /180);
            self.trackerHeuristicResultGridView.view.transform = CGAffineTransformMakeRotation(0 * M_PI /180);
            self.compass.transform = CGAffineTransformMakeRotation(0 * M_PI /180);
            self.configButton.transform = CGAffineTransformMakeRotation(0 * M_PI /180);
        default:
            break;
    }
}

- (void)captureOutput:(AVCaptureOutput *)output didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection {
    if (self.shouldInfer == false) {
        return;
    }
    if (self.isInfering) {
        // skip frame if isInferring
        return;
    }
    
    auto uiImage = [AVHelper imageFromSampleBuffer:sampleBuffer];
    
    self.isInfering = true;
    
    dispatch_async(dispatch_get_main_queue(), ^(void){
        
        NSLog(@"--------------------------------------");
        auto startAt = [[NSDate new] timeIntervalSince1970];
        NSArray<UIImage*>* heuristicImages = [HeuristicHelper generateHeuristicImagesWithImage:uiImage];
        auto heuristicImageDoneAt = [[NSDate new] timeIntervalSince1970];
        NSArray<NSNumber*>* scores = [HeuristicHelper inferImages:heuristicImages
                                                     withModelKey:self.modelKey];
        
        
        auto inferEndAt = [[NSDate new] timeIntervalSince1970];
        auto inferDuration = inferEndAt - startAt;
        auto inferPS = 1 / inferDuration;
        
        auto trackerResults = [self.tracker handle:scores];
        auto trackerUpdateDoneAt = [[NSDate new] timeIntervalSince1970];
        [[self trackerHeuristicResultGridView] setResults: trackerResults
                                               withRemark:@"tracker"];
        
        auto updateTrackerHRGVDoneAt = [[NSDate new] timeIntervalSince1970];
        [[self heuristicResultGridView] setResults:scores
                                        withRemark: [NSString stringWithFormat:@"fps: %4f", inferPS]];
        
        auto updateHRGVDoneAt = [[NSDate new] timeIntervalSince1970];
        [[self compass] drawForHeuristicResult: trackerResults];
        auto updateCompassDoneAt = [[NSDate new] timeIntervalSince1970];
        
        NSTimeInterval total = inferEndAt - startAt;
        NSLog(@"time-  heuristic image generation takes:   %f,  %.2f%%", heuristicImageDoneAt - startAt, (heuristicImageDoneAt - startAt)/total * 100);
        NSLog(@"time-  inferring takes:                    %f,  %.2f%%", inferEndAt - heuristicImageDoneAt, (inferEndAt - heuristicImageDoneAt)/total *100);
        NSLog(@"time-  total takes:                        %f", total);
        NSLog(@"time-  fps:                                %f", 1/total);
        NSLog(@"--------------------------------------");
        
        double fps = 0;
        if (self.labelLastUpdatedAt < 0) {
            self.labelLastUpdatedAt = [[NSDate new] timeIntervalSince1970];
        } else {
            auto now = [[NSDate new] timeIntervalSince1970];
            fps = 1/ (now - self.labelLastUpdatedAt);
            self.labelLastUpdatedAt = now;
        }
        
        self.isInfering = false;
    });
}


-(void) addFrameWithRect: (CGRect) rect
                 forMode: (HeuristicMode) mode {
    CAShapeLayer* rectLayer = [CAShapeLayer layer];
    [rectLayer setPath:[[UIBezierPath bezierPathWithRect:rect] CGPath]];
    rectLayer.lineWidth = 1.0;
    [rectLayer setFillColor:[UIColor clearColor].CGColor];
    [rectLayer setStrokeColor: [HeuristicHelper colorForHeuristicMode:(HeuristicMode) mode].CGColor];
    rectLayer.lineDashPattern =[NSArray arrayWithObjects:[NSNumber numberWithInt:2],[NSNumber numberWithInt:3 ], nil];
    [self.previewView.layer addSublayer:rectLayer];
}

- (void) animateFlashEffect {
    UIView* flashView = [[UIView alloc] initWithFrame: [UIScreen mainScreen].bounds];
    flashView.backgroundColor = [UIColor whiteColor];
    flashView.alpha = .8;
    [self.view addSubview:flashView];
    [self.view bringSubviewToFront:flashView];
    NSLog(@"flash");
    [UIView animateWithDuration:.5f animations:^{
        flashView.alpha = 0;
    } completion:^(BOOL animated){
        [flashView removeFromSuperview];
    }];
}

- (IBAction)onShuttorButtonTouched:(id)sender {
    [self animateFlashEffect];
    
    self.deviceOrientationWhenShutterFires = [[UIDevice currentDevice] orientation];
    
    [[self.photoOutput connectionWithMediaType:AVMediaTypeVideo] setVideoOrientation: [self.connection videoOrientation]];
    AVCapturePhotoSettings* setting = [AVCapturePhotoSettings photoSettings];
    [self.photoOutput capturePhotoWithSettings:setting delegate:self];
}


- (void)captureOutput:(AVCapturePhotoOutput *)output didFinishProcessingPhoto:(AVCapturePhoto *)photo error:(NSError *)error {
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
    }
    
    dispatch_async(dispatch_get_main_queue(), ^(void){
        auto cgImage = [photo CGImageRepresentation];
        UIImage* uiImage;
        // adjust for device orientation
        UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
        uiImage = [AVHelper correctImage:cgImage forDeviceOrientation:deviceOrientation];
        InferredImageData* data = [InferredImageData initWithImage:uiImage];
        [ImageStore.shareInstance.imageData addObject:data];
    });
}
- (IBAction)onConfigButtonPressed:(id)sender {
    LiveHeuristicConfigViewController* vc = [[LiveHeuristicConfigViewController alloc] initWithNibName:@"LiveHeuristicConfigViewController" bundle:nil];
    vc.delegate = self;
    
    self.shouldInfer = false;
    
    [self setDefinesPresentationContext:true];
    self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [nav setToolbarHidden:true];
    [nav setNavigationBarHidden:true];
    nav.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    nav.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:nav animated:true completion:nil];

}
@end
