//
//  ImageStore.m
//  mvfn_ios
//
//  Created by Chris Fan on 6/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>
#import "ImageStore.h"
#import "MVFNModel.h"

@implementation ImageStore
// Singleton of ImageStore instance
+ (instancetype) shareInstance {
    static ImageStore *shareInstance = nil;
    static dispatch_once_t onceToken; // onceToken = 0
    dispatch_once(&onceToken, ^{
        shareInstance = [[ImageStore alloc] init];
        shareInstance.imageData = [[NSMutableArray alloc] init];
        shareInstance.scoreCache = [NSMapTable weakToStrongObjectsMapTable];
    });
    return shareInstance;
}

+ (ModelScoreDict*) inferScoreForImage: (UIImage*) image {
    ModelScoreDict* dict = [ImageStore.shareInstance.scoreCache objectForKey:image];
    if (!dict) {
        // miss cache and infer now
        dict = [MVFNModel inferWithImage:image];
        [ImageStore.shareInstance.scoreCache setObject:dict forKey:image];
    }
    return dict;
}
@end
