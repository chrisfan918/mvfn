//
//  InferredImageData.h
//  mvfn_ios
//
//  Created by Chris Fan on 11/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//
#import <UIKit/UIKit.h>

#ifndef InferredImageData_h
#define InferredImageData_h

typedef NSDictionary<NSString *, NSNumber *> ModelScoreDict;

@interface InferredImageData: NSObject {
@public UIImage* image;
};

+ (instancetype) initWithImage: (UIImage*) _image;

@end
#endif /* InferredImageData_h */
