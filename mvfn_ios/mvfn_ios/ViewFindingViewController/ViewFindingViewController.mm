//
//  ViewFindingViewController.m
//  mvfn_ios
//
//  Created by Chris Fan on 18/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import "ViewFindingViewController.h"
#import "ViewFindingHelper.h"
#import "MVFNModel.h"
#import "ViewFindingConfigViewController.h"
#import "Config.h"



@interface ViewFindingCandidateTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *thrumbNailImage;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabelView;
@property (weak, nonatomic) IBOutlet UILabel *originXLabelView;
@property (weak, nonatomic) IBOutlet UILabel *originYLabelView;
@property (weak, nonatomic) IBOutlet UILabel *widthLabelView;
@property (weak, nonatomic) IBOutlet UILabel *heightLabelView;

@end
@implementation ViewFindingCandidateTableViewCell
- (void)layoutSubviews {
    [super layoutSubviews];
    
}
@end

@interface ViewFindingViewController (table) <UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, ViewFindingConfigViewControllerDelegate>

@end

@implementation ViewFindingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.imageView setContentMode:UIViewContentModeScaleAspectFit];
// this does not work well for paranoram
//    self.originalImage = [ViewFindingHelper preprocessImage:self.originalImage];
    self.imageView.image = self.originalImage;
    [self.imageView setUserInteractionEnabled:true];
    UILongPressGestureRecognizer* longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    [longPress setDelegate:self];
    [self.imageView addGestureRecognizer:longPress];
    
    UITapGestureRecognizer* imageViewTappedRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleImageViewTap:)];
    [self.imageView addGestureRecognizer:imageViewTappedRecognizer];
    
    self.title = @"ViewFinding";
    self.vfoption = [VFOption initWithDefault];
    
    UIBarButtonItem* rightBarButton = [[UIBarButtonItem alloc]
                                       initWithTitle:@"save"
                                       style:UIBarButtonItemStylePlain
                                       target:self
                                       action:@selector(onSaveButtonTouched)];
    
    [self.navigationItem setRightBarButtonItem:rightBarButton];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    self.findButton.backgroundColor = [Config colorForBackground];
    [self.findButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.progressView setProgress:0];
    
    [self.tableView setBackgroundColor: [Config colorForBackground]];
    [self.tableView setSeparatorColor:[UIColor clearColor]];
    
    self.distributionView = [[UIView alloc] initWithFrame:CGRectMake(0, self.imageView.frame.size.height - 40, self.imageView.frame.size.width, 40)];
    [self.view addSubview:self.distributionView];
    [self.distributionView setHidden:true];
    [self clearDistributionView];
    [self.distributionView setUserInteractionEnabled:true];
    UITapGestureRecognizer* tapDistributionViewRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDistributionViewTapped:)];
    [self.distributionView addGestureRecognizer:tapDistributionViewRecognizer];
    
    self.view.backgroundColor = [Config colorForBackground];
}

- (BOOL)shouldAutorotate {
    return true;
}
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void) handleImageViewTap: (UITapGestureRecognizer*) sender {
    self.distributionView.hidden = !self.distributionView.hidden;
}

-  (void)handleLongPress:(UILongPressGestureRecognizer*)sender {
    if (sender.state == UIGestureRecognizerStateBegan) {
        self.imageView.image = self.originalImage;
        self.distributionView.hidden = true;
    }
    else if (sender.state == UIGestureRecognizerStateEnded){
        self.distributionView.hidden = false;
        auto indexPath = [self.tableView indexPathForSelectedRow];
        if (indexPath) {
            VFCandidate* candidate = self.candidates[indexPath.row];
            self.imageView.image = [ViewFindingHelper cropImage:self.originalImage rect:candidate.rect];
        }
    }
}


- (void) onSaveButtonTouched {
    UIImageWriteToSavedPhotosAlbum(self.imageView.image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    auto alertVC = [UIAlertController alertControllerWithTitle:@"Saving Image"
                    
                                                       message:@"current image has been saved to album"
                                                preferredStyle: UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSLog(@"OK action");
                               }];
    
    [alertVC addAction:okAction];
    [self presentViewController:alertVC animated:true completion:nil];
}


- (void) resetImageView {
    auto selectedRow = [self.tableView indexPathForSelectedRow];
    if (selectedRow) {
        [self.tableView deselectRowAtIndexPath:selectedRow animated:false];
    }
    
    self.imageView.image = self.originalImage;
}

- (IBAction)onFindButtonTouched:(id)sender {
    [self triggerConfigViewController];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VFCandidate* candidate = self.candidates[indexPath.row];
    ViewFindingCandidateTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"ViewFindingCandidateTableViewCell"];
    cell.thrumbNailImage.contentMode = UIViewContentModeScaleAspectFit;
    cell.thrumbNailImage.image = [ViewFindingHelper cropImage:self.originalImage rect:candidate.rect];
    cell.originXLabelView.text = [NSString stringWithFormat:@"x: % 5.2f", candidate.rect.origin.x];
    cell.originYLabelView.text = [NSString stringWithFormat:@"y: % 5.2f", candidate.rect.origin.y];
    cell.widthLabelView.text = [NSString stringWithFormat:@"w: % 5.2f", candidate.rect.size.width];
    cell.heightLabelView.text = [NSString stringWithFormat:@"h: % 5.2f", candidate.rect.size.height];
    cell.scoreLabelView.text =[NSString stringWithFormat:@"%.6f", candidate.score.floatValue];
    cell.backgroundColor = [Config colorForBackground];
    
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIColor darkGrayColor]];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.candidates count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    VFCandidate* candidate = self.candidates[indexPath.row];
    self.imageView.image = [ViewFindingHelper cropImage:self.originalImage rect:candidate.rect];
    [self drawDistributionView];
}

- (void) clearDistributionView {
    self.distributionView.layer.sublayers = nil;
    self.distributionView.backgroundColor = [UIColor clearColor];
}

- (void) drawDistributionView {
    self.distributionView.layer.sublayers = nil;
    if ([self.candidates count] == 0) {
        return;
    }
    self.distributionView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.6];
    float scoresMin = [self.candidates firstObject].score.floatValue;
    float scoresMax = [self.candidates lastObject].score.floatValue;
    float scoreRange = scoresMax - scoresMin;
    
    NSIndexPath* indexPath = self.tableView.indexPathForSelectedRow;
    int selectedIndex = (int)indexPath.row;
    
    float width = self.distributionView.frame.size.width;
    float height = self.distributionView.frame.size.height;
    for (int i = 0; i < [self.candidates count]; i++){
        VFCandidate* candidate = self.candidates[i];
        float score = candidate.score.floatValue;
        
        CGFloat x = ((score - scoresMin) / scoreRange) * width;
        CAShapeLayer *line = [CAShapeLayer layer];
        UIBezierPath *linePath=[UIBezierPath bezierPath];
        [linePath moveToPoint: CGPointMake(x, 0)];
        [linePath addLineToPoint: CGPointMake(x, height)];
        line.path=linePath.CGPath;
        line.fillColor = nil;
        line.opacity = .8;
        line.cornerRadius = 1/2;
    
        if (selectedIndex == i) {
            line.strokeColor = [UIColor redColor].CGColor;
            line.lineWidth = 3;
        } else {
            line.lineWidth = 1;
            line.strokeColor = [UIColor grayColor].CGColor;
        }
        
        [self.distributionView.layer addSublayer:line];
    }
}

-(void) onDistributionViewTapped: (UITapGestureRecognizer*) sender {
    float scoresMin = [self.candidates firstObject].score.floatValue;
    float scoresMax = [self.candidates lastObject].score.floatValue;
    float scoreRange = scoresMax - scoresMin;
    
    float width = self.distributionView.frame.size.width;
    
    CGPoint loc = [sender locationInView:self.distributionView];
    float aroundScore = loc.x / width * scoreRange + scoresMin;
    
    // binary search
    
    int indexStart = 0;
    int indexEnd = (int)[self.candidates count] - 1;
    int index = (indexEnd + indexStart) / 2;
    
    while (true) {
        if (self.candidates[index].score.floatValue > aroundScore) {
            indexStart = index;
        } else {
            indexEnd = index;
        }
        if (indexEnd <= indexStart) {
            break;
        }
        if (abs(indexEnd - indexStart) <= 3) {
            break;
        }
        index = (indexEnd + indexStart) / 2;
    }

    
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    VFCandidate* candidate = self.candidates[indexPath.row];
    self.imageView.image = [ViewFindingHelper cropImage:self.originalImage rect:candidate.rect];
    [self.tableView selectRowAtIndexPath:indexPath animated:true scrollPosition:UITableViewScrollPositionMiddle];
    [self drawDistributionView];
    
}

- (void) triggerConfigViewController {
    ViewFindingConfigViewController* vc = [[ViewFindingConfigViewController alloc] initWithNibName:@"ViewFindingConfigViewController" bundle:nil];
    vc.delegate = self;
    vc.option = self.vfoption;
    
    [self setDefinesPresentationContext:true];
    self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:vc];
    nav.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    nav.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [self presentViewController:nav animated:true completion:nil];
}

- (void)didSetOption:(VFOption *)option {
    self.vfoption = option;
    [self.findButton setEnabled:false];
    [self.progressView setProgress:0];
    [self clearDistributionView];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        self.candidates = [ViewFindingHelper findViewInImage:self.originalImage
                                                  withOption:self.vfoption
                                            progressCallBack:^(int curr, int count){
                                                NSLog(@"%d/%d progress: %.2f", curr, count, (float)curr / (float) count);
                                                
                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                    [self.progressView setProgress:(float)curr / (float) count];
                                                });
                                            }];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.progressView setProgress:1];
            [self.findButton setEnabled:true];
            [self.tableView reloadData];
            NSIndexPath* indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            [self.tableView selectRowAtIndexPath:indexPath animated:true scrollPosition:UITableViewScrollPositionTop];
            VFCandidate* candidate = self.candidates[indexPath.row];
            self.imageView.image = [ViewFindingHelper cropImage:self.originalImage rect:candidate.rect];
            [self drawDistributionView];
        });
    });
}

@end

