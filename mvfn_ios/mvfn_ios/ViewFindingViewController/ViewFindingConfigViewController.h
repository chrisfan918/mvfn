//
//  ViewFindingConfigViewController.h
//  mvfn_ios
//
//  Created by Chris Fan on 18/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewFindingHelper.h"

NS_ASSUME_NONNULL_BEGIN



@protocol ViewFindingConfigViewControllerDelegate <NSObject>

- (void) didSetOption: (VFOption*) option;

@end

@interface ViewFindingConfigViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *minScaleLabel;
@property (weak, nonatomic) IBOutlet UILabel *maxScaleLabel;
@property (weak, nonatomic) IBOutlet UILabel *scaleStepLabel;
@property (weak, nonatomic) IBOutlet UISlider *minScaleSlider;
@property (weak, nonatomic) IBOutlet UISlider *maxScaleSlider;
@property (weak, nonatomic) IBOutlet UISlider *scaleStepSlider;

@property (weak, nonatomic) IBOutlet UIPickerView *modelKeyPickerView;
@property (weak, nonatomic) IBOutlet UIPickerView *aspectRatioPickerView;
@property (weak, nonatomic) IBOutlet UIView *modalBaseView;

@property VFOption* option;

@property (weak) id<ViewFindingConfigViewControllerDelegate> delegate;
@end
NS_ASSUME_NONNULL_END
