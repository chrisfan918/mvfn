//
//  ViewFindingViewController.h
//  mvfn_ios
//
//  Created by Chris Fan on 18/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewFindingHelper.h"
#import "ImageStore.h"

NS_ASSUME_NONNULL_BEGIN

@interface ViewFindingViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UIButton *findButton;

@property UIView* distributionView;
@property UIImage* originalImage;
@property NSArray<VFCandidate*>* candidates;
@property VFOption* vfoption;
@end

NS_ASSUME_NONNULL_END
