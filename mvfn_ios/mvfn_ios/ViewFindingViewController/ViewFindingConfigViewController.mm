//
//  ViewFindingConfigViewController.m
//  mvfn_ios
//
//  Created by Chris Fan on 18/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "ViewFindingConfigViewController.h"
#import "MVFNModel.h"
#import "ViewFindingHelper.h"
#import "Config.h"
#import "mixin.h"

@interface ViewFindingConfigViewController () <UIPickerViewDataSource, UIPickerViewDelegate>

@property NSArray<NSString*>* availableModelKeys;
@property NSArray<NSString*>* aspectRatioKeys;
@end

@implementation ViewFindingConfigViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.availableModelKeys = [MVFNModel availableModelsForBatch09];
    self.aspectRatioKeys = [ViewFindingHelper kAspectRatios];
    
    self.modelKeyPickerView.dataSource = self;
    self.modelKeyPickerView.delegate = self;
    [self.modelKeyPickerView reloadAllComponents];
    
    self.aspectRatioPickerView.dataSource = self;
    self.aspectRatioPickerView.delegate = self;
    [self.aspectRatioPickerView reloadAllComponents];
    
    UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onViewTapped:)];
    [self.view addGestureRecognizer:tapGesture];
    
    self.view.backgroundColor = [[Config colorForBackground] colorWithAlphaComponent:.8];
    
    self.modalBaseView.layer.cornerRadius = 10;
    self.modalBaseView.clipsToBounds = true;
}

-(void)viewWillAppear:(BOOL)animated {
    self.minScaleLabel.text = [NSString stringWithFormat:@"%.2f", self.option.minScale];
    [self.minScaleSlider setValue:self.option.minScale];
    self.maxScaleLabel.text = [NSString stringWithFormat:@"%.2f", self.option.maxScale];
    [self.maxScaleSlider setValue:self.option.maxScale];
    self.scaleStepLabel.text = [NSString stringWithFormat:@"%.2f", self.option.stepScale];
    [self.scaleStepSlider setValue:self.option.stepScale];
    [self.modelKeyPickerView selectRow:[self.availableModelKeys indexOfObject:self.option.modelKey] inComponent:0 animated:true];
}

- (void) onViewTapped: (UITapGestureRecognizer*) sender {
    auto loc = [sender locationInView:self.view];
    if (!CGRectContainsPoint(self.modalBaseView.frame, loc)) {
        [self dismissViewControllerAnimated:true completion:nil];
    }
}

- (float) roundToNearest0_05: (float) value {
    float two_decimal = round(value * 100);
    return (5 * floor(two_decimal/5 + 0.5)) / 100;
}

- (IBAction)onMinScaleSliderValueChange:(id)sender {
    float rounded = [self roundToNearest0_05: self.minScaleSlider.value];
    rounded = MIN(rounded, self.maxScaleSlider.value);
    [self.minScaleSlider setValue:rounded];
    self.minScaleLabel.text = [NSString stringWithFormat:@"%.2f", rounded];
}
- (IBAction)onMaxScaleSliderValueChange:(id)sender {
    float rounded = [self roundToNearest0_05: self.maxScaleSlider.value];
    rounded = MAX(rounded, self.minScaleSlider.value);
    [self.maxScaleSlider setValue:rounded];
    self.maxScaleLabel.text = [NSString stringWithFormat:@"%.2f", rounded];
}
- (IBAction)onScaleStepSliderValueChange:(id)sender {
    float rounded = [self roundToNearest0_05: self.scaleStepSlider.value];
    [self.scaleStepSlider setValue:rounded];
    self.scaleStepLabel.text = [NSString stringWithFormat:@"%.2f", rounded];
}

- (IBAction)didPressConfirmButton:(id)sender {
    VFOption* option = [VFOption alloc];
    
    option.minScale = self.minScaleSlider.value;
    option.maxScale = self.maxScaleSlider.value;
    option.stepScale = self.scaleStepSlider.value;
    
    option.modelKey = self.availableModelKeys[[self.modelKeyPickerView selectedRowInComponent:0]];
    
    option.aspectRatio = self.aspectRatioKeys[[self.aspectRatioPickerView selectedRowInComponent:   0]];
    
    [[self delegate] didSetOption:option];
    [self dismissViewControllerAnimated:true completion:nil];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView.tag == self.modelKeyPickerView.tag){
            return [self.availableModelKeys count];
    } else if (pickerView.tag == self.aspectRatioPickerView.tag){
            return [self.aspectRatioKeys count];
    } else {
        return 0;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (pickerView.tag == self.modelKeyPickerView.tag){
        return self.availableModelKeys[row];
    } else if (pickerView.tag == self.aspectRatioPickerView.tag){
        return self.aspectRatioKeys[row];
    } else {
        return @"";
    }
}
@end
