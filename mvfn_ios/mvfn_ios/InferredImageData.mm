//
//  InferredImageData.m
//  mvfn_ios
//
//  Created by Chris Fan on 11/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InferredImageData.h"
#import "MVFNModel.h"
#import "ImageStore.h"

@implementation InferredImageData

+ (instancetype) initWithImage:(UIImage *)_image {
    InferredImageData* data = [[InferredImageData alloc] init];
    data->image = _image;
    return data;
}

@end
