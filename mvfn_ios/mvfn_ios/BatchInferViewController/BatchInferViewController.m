//
//  BatchInferViewController.m
//  mvfn_ios
//
//  Created by Chris Fan on 11/2/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import "InferredCollectionViewCell.h"
#import "BatchInferViewController.h"
#import "BatchInferViewController+pickerView.h"
#import "BatchInferViewController+collectionView.h"

#import <UIKit/UIKit.h>

#import "HeuristicHelper.h"
#import "HeuristicResultGridView.h"
#import "Config.h"

@interface BatchInferViewController ()

@end

@implementation BatchInferViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    inferredImages = [[NSMutableArray alloc] init];
    
    [addButton setTarget: self];
    [addButton setAction:@selector(invokeImagePickerViewWithLibrary)];
    
    [cameraButton setTarget:self];
    [cameraButton setAction:@selector(invokeImagePickerViewWithCamera)];
    
    [collectionView setDelegate:self];
    [collectionView setDataSource:self];
    UINib* collectionViewCell = [UINib nibWithNibName:@"InferredCollectionViewCell" bundle:nil];

    [collectionView registerNib:collectionViewCell forCellWithReuseIdentifier:@"InferredCollectionViewCell"];
    
    [ImageStore.shareInstance.imageData addObject:[InferredImageData initWithImage:[UIImage imageNamed:@"test"]]];
    [ImageStore.shareInstance.imageData addObject:[InferredImageData initWithImage:[UIImage imageNamed:@"test1"]]];
    [ImageStore.shareInstance.imageData addObject:[InferredImageData initWithImage:[UIImage imageNamed:@"test2"]]];
    
    self.view.backgroundColor = [Config colorForBackground];
    collectionView.backgroundColor = [Config colorForBackground];
}

- (void)viewWillAppear:(BOOL)animated {
    [collectionView reloadData];
}


@end
