//
//  BatchInferViewController+pickerView.h
//  mvfn_ios
//
//  Created by Chris Fan on 11/2/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import "BatchInferViewController.h"
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BatchInferViewController (pickerView) <
    UIImagePickerControllerDelegate,
    UINavigationControllerDelegate>


- (void) invokeImagePickerViewWithLibrary;
- (void) invokeImagePickerViewWithCamera;
@end

NS_ASSUME_NONNULL_END
