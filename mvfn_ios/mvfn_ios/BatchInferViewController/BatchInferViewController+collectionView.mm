//
//  BatchInferViewController+collectionView.m
//  mvfn_ios
//
//  Created by Chris Fan on 11/2/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import "BatchInferViewController+collectionView.h"
#import "InferredCollectionViewCell.h"
#import "InspectImageViewController.h"

@implementation BatchInferViewController (collectionView)

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    InferredCollectionViewCell* cellView = [collectionView dequeueReusableCellWithReuseIdentifier:@"InferredCollectionViewCell"
                                                                                     forIndexPath:  indexPath];
    
    InferredImageData* data = ImageStore.shareInstance.imageData[indexPath.row];
    [cellView setImage: data->image];
    return cellView;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [ImageStore.shareInstance.imageData count];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return InferredCollectionViewCell.size;
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    InspectImageViewController* vc = [[self storyboard] instantiateViewControllerWithIdentifier:@"InspectImageViewController"];
    vc.imageIndex = [indexPath row];
    [[self navigationController] pushViewController:vc animated:true];
}

@end
