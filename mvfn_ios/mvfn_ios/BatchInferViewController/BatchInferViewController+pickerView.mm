//
//  BatchInferViewController+pickerView.m
//  mvfn_ios
//
//  Created by Chris Fan on 11/2/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import "BatchInferViewController+pickerView.h"
#import "BatchInferViewController+toCropVC.h"
#import "MVFNModel.h"

#import <AssertMacros.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <CoreImage/CoreImage.h>
#import <ImageIO/ImageIO.h>
#import <Photos/Photos.h>

@implementation BatchInferViewController (pickerView)

- (void) invokeImagePickerViewWithCamera {
    UIImagePickerController *pickerView = [[UIImagePickerController alloc] init];
    pickerView.allowsEditing = NO;
    pickerView.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [pickerView setSourceType:UIImagePickerControllerSourceTypeCamera];
    } else {
        [pickerView setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }
    [self presentViewController:pickerView animated:YES completion:nil];
}

- (void) invokeImagePickerViewWithLibrary {
    UIImagePickerController *pickerView = [[UIImagePickerController alloc] init];
    pickerView.allowsEditing = NO;
    pickerView.delegate = self;
    [pickerView setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [self presentViewController:pickerView animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    UIImage * img = nil;
    if (picker.allowsEditing){
        img = [info valueForKey:UIImagePickerControllerEditedImage];
    } else {
        img = [info valueForKey:UIImagePickerControllerOriginalImage];
    }
    
    [self presentCropView:img];
}

@end
