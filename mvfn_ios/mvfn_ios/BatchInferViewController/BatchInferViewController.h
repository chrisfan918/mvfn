//
//  BatchInferViewController.h
//  mvfn_ios
//
//  Created by Chris Fan on 11/2/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageStore.h"

NS_ASSUME_NONNULL_BEGIN

@interface BatchInferViewController : UIViewController
{
    NSMutableArray<InferredImageData *>* inferredImages;
    
    __weak IBOutlet UIBarButtonItem *cameraButton;
    __weak IBOutlet UIBarButtonItem *addButton;
    __weak IBOutlet UICollectionView *collectionView;
}
@end
NS_ASSUME_NONNULL_END
