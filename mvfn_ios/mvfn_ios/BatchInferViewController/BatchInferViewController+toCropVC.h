//
//  BatchInferViewController+toCropVC.h
//  mvfn_ios
//
//  Created by Chris Fan on 6/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import "BatchInferViewController.h"
#import "PhotoTweaksViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BatchInferViewController (toCropVC) <PhotoTweaksViewControllerDelegate>

- (void) presentCropView: (UIImage*) img;
- (void) inferAndUpdateCollectionView: (UIImage*) img;

@end

NS_ASSUME_NONNULL_END
