//
//  BatchInferViewController+toCropVC.m
//  mvfn_ios
//
//  Created by Chris Fan on 6/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import "BatchInferViewController+toCropVC.h"
#import "MVFNModel.h"

@implementation BatchInferViewController (toCropVC) 

- (void) presentCropView: (UIImage*) img {
    PhotoTweaksViewController *photoTweaksViewController = [[PhotoTweaksViewController alloc] initWithImage:img];
    photoTweaksViewController.delegate = self;
    photoTweaksViewController.autoSaveToLibray = false;
    photoTweaksViewController.maxRotationAngle = M_PI_4;
    [self presentViewController:photoTweaksViewController animated:YES completion:nil];
}

- (void)photoTweaksController:(PhotoTweaksViewController *)controller didFinishWithCroppedImage:(UIImage *)croppedImage {
    [self inferAndUpdateCollectionView: croppedImage];
    [self dismissViewControllerAnimated:true completion:nil];
}

- (void)photoTweaksControllerDidCancel:(PhotoTweaksViewController *)controller {
    [self dismissViewControllerAnimated:true completion:nil];
}


- (void) inferAndUpdateCollectionView: (UIImage*) img {
//    UIImage* resizedImg = [self resizeImage:img withSize:CGSizeMake(224, 224)];
    InferredImageData* data = [InferredImageData initWithImage:img];
    [ImageStore.shareInstance.imageData addObject:data];
    [collectionView reloadData];
}

- (UIImage*) resizeImage: (UIImage*) original
                withSize: (CGSize) size
{
    UIGraphicsBeginImageContext(size);
    [original drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage * resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resizedImage;
}
@end
