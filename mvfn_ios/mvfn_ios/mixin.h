//
//  mixin.h
//  mvfn_ios
//
//  Created by Chris Fan on 15/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#ifndef mixin_h
#define mixin_h

@interface NSMutableArray (QueueAdditions)
- (id) dequeue;
- (void) enqueue:(id)obj;
@end

@interface UITextField (DoneButton)
- (void) addDoneToolBar;

@end


@interface UIBezierPath (dqd_arrowhead)

+ (UIBezierPath *)dqd_bezierPathWithArrowFromPoint:(CGPoint)startPoint
                                           toPoint:(CGPoint)endPoint
                                         tailWidth:(CGFloat)tailWidth
                                         headWidth:(CGFloat)headWidth
                                        headLength:(CGFloat)headLength;

@end
#endif /* mixin_h */
