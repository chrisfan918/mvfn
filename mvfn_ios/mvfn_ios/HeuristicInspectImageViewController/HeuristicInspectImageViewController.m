//
//  HeuristicInspectImageViewController.m
//  mvfn_ios
//
//  Created by Chris Fan on 8/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//
#import "HeuristicHelper.h"
#import "MVFNModel.h"
#import "HeuristicInspectImageViewController.h"
#import "ImageInfoViewController.h"
#import "Config.h"

@interface HeuristicInspectImageViewController ()

@end

@implementation HeuristicInspectImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view setBackgroundColor:[Config colorForBackground]];
    
    [self addImageInfoViewControllerVCToView:self.originalView withData:self.originImageData];
    
    // loop through all HeuristicMode
    for (int mode = 0; mode < HeuristicModeCount; mode++) {
        UIView* view = [self getViewWithMode: mode];
        
        if (!view) continue;
        NSTimeInterval start = [[NSDate new] timeIntervalSince1970];
        UIImage* heuristicImage = [self getHeuristicImageWithOriginalImage:self.originImageData->image withHeuristicMode:mode];
        
        NSTimeInterval endEditImage = [[NSDate new] timeIntervalSince1970];
        InferredImageData* data = [InferredImageData initWithImage:heuristicImage];
        
        NSTimeInterval endInfer = [[NSDate new] timeIntervalSince1970];
        
        NSLog(@"mode %d editDuration: %.6f inferAllDuration: %.6f total: %.6f", mode, endEditImage-start, endInfer-endEditImage, endInfer-start);
        [self addImageInfoViewControllerVCToView:view withData:data];
        
    }
}

- (void) addImageInfoViewControllerVCToView: (UIView *) view
                                   withData: (InferredImageData*) data {
    
    ImageInfoViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ImageInfoViewController"];
    
    [vc setData:data];
    
    [vc.view setFrame:view.bounds];
    [view addSubview:vc.view];
    [self addChildViewController:vc];
    [self didMoveToParentViewController:vc];

}

- (UIImage*) getHeuristicImageWithOriginalImage: (UIImage*) originImage
                              withHeuristicMode: (HeuristicMode) mode {
    return [HeuristicHelper crop:originImage withHeuristicMode:mode];
}

- (UIView*) getViewWithMode: (HeuristicMode) mode {
    switch (mode) {
        case kHeuristicModeCenter:
            return self.centerView;
        case kHeuristicModeUpper:
            return self.upperView;
        case kHeuristicModeLeft:
            return self.leftView;
        case kHeuristicModeRight:
            return self.rightView;
        case kHeuristicModeLower:
            return self.lowerView;
        case kHeuristicModeUpperLeft:
            return self.upperLeftView;
        case kHeuristicModeUpperRight:
            return self.upperRightView;
        case kHeuristicModeLowerLeft:
            return self.lowerLeftView;
        case kHeuristicModeLowerRight:
            return self.lowerRightView;
        case kHeuristicModeClockWise:
            return self.cwView;
        case kHeuristicModeAntiClockWise:
            return self.acwView;
        default:
            return nil;
    }
}

@end
