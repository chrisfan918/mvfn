//
//  HeuristicInspectImageViewController.h
//  mvfn_ios
//
//  Created by Chris Fan on 8/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageStore.h"

NS_ASSUME_NONNULL_BEGIN

@interface HeuristicInspectImageViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *originalView;

@property (weak, nonatomic) IBOutlet UIView *upperView;
@property (weak, nonatomic) IBOutlet UIView *upperLeftView;
@property (weak, nonatomic) IBOutlet UIView *upperRightView;
@property (weak, nonatomic) IBOutlet UIView *centerView;
@property (weak, nonatomic) IBOutlet UIView *lowerView;
@property (weak, nonatomic) IBOutlet UIView *leftView;
@property (weak, nonatomic) IBOutlet UIView *rightView;
@property (weak, nonatomic) IBOutlet UIView *lowerLeftView;
@property (weak, nonatomic) IBOutlet UIView *lowerRightView;
@property (weak, nonatomic) IBOutlet UIView *acwView;
@property (weak, nonatomic) IBOutlet UIView *cwView;

@property NSInteger imageIndex;
@property InferredImageData* originImageData;
@property NSMutableDictionary* heuristicImageData;

@end


NS_ASSUME_NONNULL_END
