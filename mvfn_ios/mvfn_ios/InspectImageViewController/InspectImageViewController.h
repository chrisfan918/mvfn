//
//  InspectImageViewController.h
//  mvfn_ios
//
//  Created by Chris Fan on 7/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "ImageStore.h"

NS_ASSUME_NONNULL_BEGIN

@interface InspectImageViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *viewFindingButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property NSMutableArray* infoContents;
@property NSInteger imageIndex;
@property InferredImageData* imageData;
@end

NS_ASSUME_NONNULL_END
