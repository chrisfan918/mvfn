//
//  InspectImageViewController.m
//  mvfn_ios
//
//  Created by Chris Fan on 7/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import "InspectImageViewController.h"
#import "InfoTableViewCell.h"
#import "ImageStore.h"
#import "MVFNModel.h"
#import "Config.h"
#import "PhotoTweaksViewController.h"
#import "HeuristicInspectImageViewController.h"
#import "ViewFindingViewController.h"

@interface InspectImageViewController () <PhotoTweaksViewControllerDelegate>

@end

@implementation InspectImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [Config colorForBackground];
    auto tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(triggerEditImage)];
    [self.imageView setUserInteractionEnabled:true];
    [self.imageView addGestureRecognizer:tapRecognizer];
    
    // Do any additional setup after loading the view.
    self.infoContents = [[NSMutableArray alloc] init];
    
    [self.imageView setContentMode:UIViewContentModeScaleAspectFit];
    
    UIBarButtonItem* editBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(triggerEditImage)];
    [self.navigationItem setRightBarButtonItem:editBtn];
    
    [self.saveButton setBackgroundColor:[Config colorForBackground]];
    [self.saveButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.viewFindingButton setBackgroundColor:[Config colorForBackground]];
    [self.viewFindingButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self reload];
}

- (BOOL)shouldAutorotate {
    return true;
}
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
- (IBAction)onViewFindingButtonTouched:(id)sender {
    [self triggerViewFindingViewController];
}

- (IBAction)onSaveButtonTouched:(id)sender {
    UIImageWriteToSavedPhotosAlbum(self.imageData->image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
}
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    auto alertVC = [UIAlertController alertControllerWithTitle:@"Saving Image"
                    
                                                       message:@"current image has been saved to album"
                                                preferredStyle: UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSLog(@"OK action");
                               }];

    [alertVC addAction:okAction];
    [self presentViewController:alertVC animated:true completion:nil];
}


-(void) reload {
    
    [self.infoContents removeAllObjects];
    
    // load data
    self.imageData = [ImageStore.shareInstance.imageData objectAtIndex:(NSUInteger) self.imageIndex];
    
    CGSize size = [self.imageData->image size];
    NSInteger height = (NSInteger) size.height;
    NSInteger width = (NSInteger) size.width;
    [self.infoContents addObject:@{
                                   @"key":@"width",
                                   @"value":[NSString stringWithFormat:@"%ld", (long)width]
                                   }];
    [self.infoContents addObject:@{
                                   @"key":@"height",
                                   @"value":[NSString stringWithFormat:@"%ld", (long)height]
                                   }];
    
    self.imageView.image = self.imageData->image;
}

- (void) triggerEditImage {
    PhotoTweaksViewController *photoTweaksViewController = [[PhotoTweaksViewController alloc] initWithImage:self.imageData->image];
    photoTweaksViewController.view.backgroundColor = [Config colorForBackground];
    photoTweaksViewController.delegate = self;
    photoTweaksViewController.autoSaveToLibray = false;
    photoTweaksViewController.maxRotationAngle = M_PI_4;
    [self presentViewController:photoTweaksViewController animated:YES completion:nil];
}

- (void)photoTweaksController:(PhotoTweaksViewController *)controller didFinishWithCroppedImage:(UIImage *)croppedImage {
    auto data = [InferredImageData initWithImage:croppedImage];
    [ImageStore.shareInstance.imageData replaceObjectAtIndex:self.imageIndex withObject:data];
    [self reload];
    [self dismissViewControllerAnimated:true completion:nil];
}

- (void)photoTweaksControllerDidCancel:(PhotoTweaksViewController *)controller {
    [self dismissViewControllerAnimated:true completion:nil];
}

- (void) triggerHeuristicInspectViewController {
    HeuristicInspectImageViewController* vc = [[self storyboard] instantiateViewControllerWithIdentifier:@"HeuristicInspectImageViewController"];
    [vc setOriginImageData:self.imageData];
    [[self navigationController] pushViewController:vc animated:true];
}


- (void) triggerViewFindingViewController {
    ViewFindingViewController* vc = [[self storyboard] instantiateViewControllerWithIdentifier:@"ViewFindingViewController"];
    vc.originalImage = self.imageData->image;
    [[self navigationController] pushViewController:vc animated:true];
}
@end
