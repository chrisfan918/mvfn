//
//  MVFNModel.m
//  mvfn_ios
//
//  Created by Chris Fan on 12/2/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <Photos/Photos.h>
// c++ library
#include <iostream>

// tensorflow c++ library
#import "tensorflow/lite/kernels/register.h"
#import "tensorflow/lite/model.h"

#define LOG(x) std::cerr

#import "MVFNModel.h"

@interface MVFNModel() {
    std::unique_ptr<tflite::FlatBufferModel> model;
    tflite::ops::builtin::BuiltinOpResolver resolver;
    std::unique_ptr<tflite::Interpreter> interpreter;
    CGFloat inputDim;
    int batchSize;
    NSString* name;
}

@end


@implementation MVFNModel

NSString * const kMVFN096 = @"mvfn_096";
NSString * const kMVFN160 = @"mvfn_160";
NSString * const kMVFN224 = @"mvfn_224";

NSString * const kMVFN096Batch09 = @"mvfn_096_BATCH_09";
NSString * const kMVFN160Batch09 = @"mvfn_160_BATCH_09";
NSString * const kMVFN224Batch09 = @"mvfn_224_BATCH_09";

NSString * const kMVFN096Batch11 = @"mvfn_096_BATCH_11";
NSString * const kMVFN160Batch11 = @"mvfn_160_BATCH_11";
NSString * const kMVFN224Batch11 = @"mvfn_224_BATCH_11";

NSString * const kMVFN096Batch09Quant = @"mvfn_096_BATCH_09_QUANT";
NSString * const kMVFN160Batch09Quant = @"mvfn_160_BATCH_09_QUANT";
NSString * const kMVFN224Batch09Quant = @"mvfn_224_BATCH_09_QUANT";

NSString * const kMVFN096Batch11Quant = @"mvfn_096_BATCH_11_QUANT";
NSString * const kMVFN160Batch11Quant = @"mvfn_160_BATCH_11_QUANT";
NSString * const kMVFN224Batch11Quant = @"mvfn_224_BATCH_11_QUANT";

+ (NSArray<NSString*>*) availableModelsForSingleInfer {
    return @[kMVFN096, kMVFN160, kMVFN224];
};
+ (NSArray<NSString*>*) availableModelsForBatch09 {
    return @[kMVFN096Batch09, kMVFN096Batch09Quant,
             kMVFN160Batch09, kMVFN160Batch09Quant,
             kMVFN224Batch09, kMVFN224Batch09Quant];
};
+ (NSArray<NSString*>*) availableModelsForBatch11 {
    return @[kMVFN096Batch11, kMVFN096Batch11Quant,
             kMVFN160Batch11, kMVFN160Batch11Quant,
             kMVFN224Batch11, kMVFN224Batch11Quant];
};

static NSString* FilePathForResourceName(NSString* name, NSString* extension) {
    NSString* file_path = [[NSBundle mainBundle] pathForResource:name ofType:extension];
    if (file_path == NULL) {
        NSLog(@"Couldn't find %@.%@ in bundle", name, extension);
    }
    return file_path;
}

#pragma mark - Class Methods
// Batch 1
+ (instancetype) sharedMVFN096 {
    static MVFNModel *sharedMVFN96 = nil;
    static dispatch_once_t onceToken96; // onceToken = 0
    dispatch_once(&onceToken96, ^{
        sharedMVFN96 = [[MVFNModel alloc] initWithModelName:kMVFN096
                                                   inputDim: 96
                                                  batchSize:1];
    });
    return sharedMVFN96;
}

+ (instancetype) sharedMVFN160 {
    static MVFNModel *sharedMVFN160 = nil;
    static dispatch_once_t onceToken160; // onceToken = 0
    dispatch_once(&onceToken160, ^{
        sharedMVFN160 = [[MVFNModel alloc] initWithModelName:kMVFN160
                                                    inputDim: 160
                                                   batchSize:1];
    });
    return sharedMVFN160;
}

+ (instancetype) sharedMVFN224 {
    static MVFNModel *sharedMVFN224 = nil;
    static dispatch_once_t onceToken224; // onceToken = 0
    dispatch_once(&onceToken224, ^{
        sharedMVFN224 = [[MVFNModel alloc] initWithModelName:kMVFN224
                                                    inputDim: 224
                                                   batchSize:1];
    });
    return sharedMVFN224;
}

// Batch 9
+ (instancetype) sharedMVFN096Batch09 {
    static MVFNModel *sharedMVFN096Batch = nil;
    static dispatch_once_t onceTokensharedMVFN096Batch; // onceToken = 0
    dispatch_once(&onceTokensharedMVFN096Batch, ^{
        sharedMVFN096Batch = [[MVFNModel alloc] initWithModelName:kMVFN096Batch09
                                                         inputDim: 96
                                                        batchSize:9];
    });
    return sharedMVFN096Batch;
    
}

+ (instancetype) sharedMVFN160Batch09 {
    static MVFNModel *sharedMVFN160Batch = nil;
    static dispatch_once_t onceTokensharedMVFN160Batch; // onceToken = 0
    dispatch_once(&onceTokensharedMVFN160Batch, ^{
        sharedMVFN160Batch = [[MVFNModel alloc] initWithModelName:kMVFN160Batch09
                                                         inputDim: 160
                                                        batchSize:9];
    });
    return sharedMVFN160Batch;
    
}

+ (instancetype) sharedMVFN224Batch09 {
    static MVFNModel *sharedMVFN224Batch = nil;
    static dispatch_once_t onceTokensharedMVFN224Batch; // onceToken = 0
    dispatch_once(&onceTokensharedMVFN224Batch, ^{
        sharedMVFN224Batch = [[MVFNModel alloc] initWithModelName:kMVFN224Batch09
                                                         inputDim: 224
                                                        batchSize:9];
    });
    return sharedMVFN224Batch;
    
}

// Batch 11
+ (instancetype) sharedMVFN096Batch11 {
    static MVFNModel *sharedMVFN096Batch11 = nil;
    static dispatch_once_t onceTokensharedMVFN096Batch11; // onceToken = 0
    dispatch_once(&onceTokensharedMVFN096Batch11, ^{
        sharedMVFN096Batch11 = [[MVFNModel alloc] initWithModelName:kMVFN096Batch11
                                                           inputDim: 96
                                                          batchSize:11];
    });
    return sharedMVFN096Batch11;
}

+ (instancetype) sharedMVFN160Batch11 {
    static MVFNModel *sharedMVFN160Batch11 = nil;
    static dispatch_once_t onceTokensharedMVFN160Batch11; // onceToken = 0
    dispatch_once(&onceTokensharedMVFN160Batch11, ^{
        sharedMVFN160Batch11 = [[MVFNModel alloc] initWithModelName:kMVFN160Batch11
                                                           inputDim: 160
                                                          batchSize:11];
    });
    return sharedMVFN160Batch11;
}

+ (instancetype) sharedMVFN224Batch11 {
    static MVFNModel *sharedMVFN224Batch11 = nil;
    static dispatch_once_t onceTokensharedMVFN224Batch11; // onceToken = 0
    dispatch_once(&onceTokensharedMVFN224Batch11, ^{
        sharedMVFN224Batch11 = [[MVFNModel alloc] initWithModelName:kMVFN224Batch11
                                                           inputDim: 224
                                                          batchSize:11];
    });
    return sharedMVFN224Batch11;
}


+ (instancetype) sharedMVFN096Batch11Quant {
    static MVFNModel *sharedMVFN096Batch11Quant = nil;
    static dispatch_once_t onceTokensharedMVFN096Batch11Quant; // onceToken = 0
    dispatch_once(&onceTokensharedMVFN096Batch11Quant, ^{
        sharedMVFN096Batch11Quant = [[MVFNModel alloc] initWithModelName:kMVFN096Batch11Quant
                                                           inputDim: 96
                                                          batchSize:11];
    });
    return sharedMVFN096Batch11Quant;
}


+ (instancetype) sharedMVFN160Batch11Quant {
    static MVFNModel *sharedMVFN160Batch11Quant = nil;
    static dispatch_once_t onceTokensharedMVFN160Batch11Quant; // onceToken = 0
    dispatch_once(&onceTokensharedMVFN160Batch11Quant, ^{
        sharedMVFN160Batch11Quant = [[MVFNModel alloc] initWithModelName:kMVFN160Batch11Quant
                                                                inputDim: 160
                                                               batchSize: 11];
    });
    return sharedMVFN160Batch11Quant;
}

+ (instancetype) sharedMVFN224Batch11Quant {
    static MVFNModel *sharedMVFN224Batch11Quant = nil;
    static dispatch_once_t onceTokensharedMVFN224Batch11Quant; // onceToken = 0
    dispatch_once(&onceTokensharedMVFN224Batch11Quant, ^{
        sharedMVFN224Batch11Quant = [[MVFNModel alloc] initWithModelName:kMVFN224Batch11Quant
                                                                inputDim: 224
                                                               batchSize: 11];
    });
    return sharedMVFN224Batch11Quant;
}

+ (instancetype) sharedMVFN096Batch09Quant {
    static MVFNModel *sharedMVFN096Batch09Quant = nil;
    static dispatch_once_t onceTokensharedMVFN096Batch09Quant; // onceToken = 0
    dispatch_once(&onceTokensharedMVFN096Batch09Quant, ^{
        sharedMVFN096Batch09Quant = [[MVFNModel alloc] initWithModelName:kMVFN096Batch09Quant
                                                                inputDim: 96
                                                               batchSize: 9];
    });
    return sharedMVFN096Batch09Quant;
}

+ (instancetype) sharedMVFN160Batch09Quant {
    static MVFNModel *sharedMVFN160Batch09Quant = nil;
    static dispatch_once_t onceTokensharedMVFN160Batch09Quant; // onceToken = 0
    dispatch_once(&onceTokensharedMVFN160Batch09Quant, ^{
        sharedMVFN160Batch09Quant = [[MVFNModel alloc] initWithModelName:kMVFN160Batch09Quant
                                                                inputDim: 160
                                                               batchSize: 9];
    });
    return sharedMVFN160Batch09Quant;
}

+ (instancetype) sharedMVFN224Batch09Quant {
    static MVFNModel *sharedMVFN224Batch09Quant = nil;
    static dispatch_once_t onceTokensharedMVFN224Batch09Quant; // onceToken = 0
    dispatch_once(&onceTokensharedMVFN224Batch09Quant, ^{
        sharedMVFN224Batch09Quant = [[MVFNModel alloc] initWithModelName:kMVFN224Batch09Quant
                                                                inputDim: 224
                                                               batchSize: 9];
    });
    return sharedMVFN224Batch09Quant;
}


+ (NSDictionary<NSString *, NSNumber *>*) inferWithImage: (UIImage*) image {
    auto start = [[NSDate new] timeIntervalSince1970];
    auto dict = @{
             kMVFN096: [NSNumber numberWithFloat: [MVFNModel.sharedMVFN096 inferWithImage:image]],
             kMVFN160: [NSNumber numberWithFloat: [MVFNModel.sharedMVFN160 inferWithImage:image]],
             kMVFN224: [NSNumber numberWithFloat: [MVFNModel.sharedMVFN224 inferWithImage:image]],
             };
    auto end = [[NSDate new] timeIntervalSince1970];
    NSLog(@"time for evaluating image with all models: %f", end-start);
    return dict;
}

+ (NSNumber *)inferWithImage:(UIImage *)image withModel:(NSString *)modelKey {
    if (modelKey == kMVFN096) {
        return [NSNumber numberWithFloat:[MVFNModel.sharedMVFN096 inferWithImage:image]];
    } else if (modelKey == kMVFN160) {
        return [NSNumber numberWithFloat:[MVFNModel.sharedMVFN160 inferWithImage:image]];
    } else if (modelKey == kMVFN224) {
        return  [NSNumber numberWithFloat:[MVFNModel.sharedMVFN224 inferWithImage:image]];
    } else {
        NSException *exception = [NSException
            exceptionWithName:@"UnknownModelKeyException"
            reason:@"*** inferWithImage(): cannot recognize given model key"
            userInfo:nil];
        
        // Throw the exception.
        @throw exception;
    }
}

+ (NSArray<NSNumber*>*) inferWithImages: (NSArray<UIImage*>*) images
                              withModel: (NSString*) modelKey {
    if (modelKey == kMVFN096) {
        return [MVFNModel.sharedMVFN096 inferWithImages:images];
    }
    if (modelKey == kMVFN160){
        return [MVFNModel.sharedMVFN160 inferWithImages:images];
    }
    if (modelKey == kMVFN224) {
        return [MVFNModel.sharedMVFN224 inferWithImages:images];
    }
    
    // batch size = 9
    if (modelKey == kMVFN096Batch09) {
        return [MVFNModel.sharedMVFN096Batch09 inferWithImages:images];
    }
    if (modelKey == kMVFN160Batch09){
        return [MVFNModel.sharedMVFN160Batch09 inferWithImages:images];
    }
    if (modelKey == kMVFN224Batch09) {
        return [MVFNModel.sharedMVFN224Batch09 inferWithImages:images];
    }
    
    // batch size = 11
    if (modelKey == kMVFN096Batch11) {
        return [MVFNModel.sharedMVFN096Batch11 inferWithImages:images];
    }
    if (modelKey == kMVFN160Batch11){
        return [MVFNModel.sharedMVFN160Batch11 inferWithImages:images];
    }
    if (modelKey == kMVFN224Batch11) {
        return [MVFNModel.sharedMVFN224Batch11 inferWithImages:images];
    }
    
    // batch size = 09 quantized
    if (modelKey == kMVFN096Batch09Quant) {
        return [MVFNModel.sharedMVFN096Batch09Quant inferWithImages:images];
    }
    
    if (modelKey == kMVFN160Batch09Quant) {
        return [MVFNModel.sharedMVFN160Batch09Quant inferWithImages:images];
    }
    
    if (modelKey == kMVFN224Batch09Quant) {
        return [MVFNModel.sharedMVFN224Batch09Quant inferWithImages:images];
    }
    
    // batch size = 11 quantized
    if (modelKey == kMVFN096Batch11Quant) {
        return [MVFNModel.sharedMVFN096Batch11Quant inferWithImages:images];
    }
    
    if (modelKey == kMVFN160Batch11Quant) {
        return [MVFNModel.sharedMVFN160Batch11Quant inferWithImages:images];
    }
    
    if (modelKey == kMVFN224Batch11Quant) {
        return [MVFNModel.sharedMVFN224Batch11Quant inferWithImages:images];
    }
    
    NSException *exception = [NSException
                              exceptionWithName:@"UnknownModelKeyException"
                              reason:@"*** [inferWithImages: withModel]: cannot recognize given model key"
                              userInfo:nil];
    
    // Throw the exception.
    @throw exception;
    
}

#pragma mark - Instance Methods

- (instancetype) initWithModelName: (NSString*) _name
                          inputDim:(CGFloat) _inputDim
                         batchSize:(int) _batchSize {
    self = [super init];
    name = _name;
    inputDim = _inputDim;
    batchSize = _batchSize;
    
    NSString* graph_path = FilePathForResourceName(name, @"tflite");
    model = tflite::FlatBufferModel::BuildFromFile([graph_path UTF8String]);
    if (!model) {
        NSLog(@"Failed to map model %@", graph_path);
    }
    
    NSLog(@"Loaded model %@", graph_path);
    model->error_reporter();
    NSLog(@"resolved reporter");
    
    tflite::ops::builtin::BuiltinOpResolver resolver;
    
    tflite::InterpreterBuilder(*model, resolver)(&interpreter);
    if (!interpreter) {
        NSLog(@"Failed to construct interpreter");
    }
    
    if (interpreter->AllocateTensors() != kTfLiteOk) {
        NSLog(@"Failed to allocate tensors");
    }
    
    return self;
}

- (float) inferWithImage: (UIImage*) image {
    
    // UIImage to pixel
    int imageWidth = (int) CGImageGetWidth(image.CGImage);
    int imageHeight = (int) CGImageGetHeight(image.CGImage);
    int imageChannels = 4;
    
    CGColorSpaceRef color_space = CGColorSpaceCreateDeviceRGB();
    // bytes_per_row = width * channel = 224 * 3
    const int bytes_per_row = (imageWidth * 4);
    // bytes_in_image = width * channel * height = 224 * 3 * 224
    const int bytes_in_image = (bytes_per_row * imageHeight);
    std::vector<uint8_t> data = std::vector<uint8_t>(bytes_in_image);
    const int bits_per_component = 8;
    
    CGContextRef context =
    CGBitmapContextCreate(data.data(), imageWidth, imageHeight, bits_per_component, bytes_per_row,
                          color_space, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(color_space);
    CGRect size = CGRectMake(0, 0, imageWidth, imageHeight);
    CGContextDrawImage(context, size, image.CGImage);
    CGContextRelease(context);
    
    // set input tensor
    float* out = interpreter->typed_input_tensor<float>(0);
    
    uint8_t* in = data.data();
    
    int wanted_input_width = inputDim;
    int wanted_input_height = inputDim;
    int wanted_input_channels = 3;
    
    
    for (int y = 0; y < wanted_input_height; y++) {
        const int in_y = (y * imageHeight) / wanted_input_height;
        uint8_t* in_row = in + (in_y * imageWidth * imageChannels);
        float* out_row = out + (y * wanted_input_width * wanted_input_channels);
        for (int x = 0; x < wanted_input_width; x++) {
            const int in_x = (x * imageWidth) / wanted_input_width;
            uint8_t* in_pixel = in_row + (in_x * imageChannels);
            float* out_pixel = out_row + (x * wanted_input_channels);
            for (int c = 0; c < wanted_input_channels; ++c) {
                out_pixel[c] = in_pixel[c] / 128.0 - 1;
            }
        }
    }
    
    double startTimestamp = [[NSDate new] timeIntervalSince1970];
    if (interpreter->Invoke() != kTfLiteOk) {
        NSLog(@"Failed to allocate invoke");
    }
    double endTimestamp = [[NSDate new] timeIntervalSince1970];
    NSLog(@"%@ Time: %.4lf", name, endTimestamp - startTimestamp);
    
    float* output = interpreter->typed_output_tensor<float>(0);
    
    return *output;
}


- (NSArray<NSNumber*>*) inferWithImages: (NSArray<UIImage*>*) images {

    if ([images count] < batchSize) {
        NSException *exception = [NSException
                                  exceptionWithName:@"InputBatchSizeMismatch"
                                  reason:@"*** (NSArray<NSNumber*>*) inferWithImages: (NSArray<UIImage*>*) images has received incorrect input batch size"
                                  userInfo:nil];
        
        // Throw the exception.
        @throw exception;
    }
#warning prepareTensorStart
    NSTimeInterval prepareTensorStart = [[NSDate new] timeIntervalSince1970];
    
    float* out = interpreter->typed_input_tensor<float>(0);
    
    NSTimeInterval prepareTensorForImagesSum = 0;
    for (int i = 0; i < batchSize; i++) {
    #warning writeTensorStart
        NSTimeInterval writeTensorStart = [[NSDate new] timeIntervalSince1970];
        
        UIImage* image = [images objectAtIndex:i];
        // UIImage to pixel
        int imageWidth = (int) CGImageGetWidth(image.CGImage);
        int imageHeight = (int) CGImageGetHeight(image.CGImage);
        int imageChannels = 4;
        
        CFDataRef rawData = CGDataProviderCopyData(CGImageGetDataProvider(image.CGImage));
        uint8_t* in = (uint8_t*) CFDataGetBytePtr(rawData);

        int wanted_input_width = inputDim;
        int wanted_input_height = inputDim;
        int wanted_input_channels = 3;
        int bytes_in_wanted_image = wanted_input_width * wanted_input_height * wanted_input_channels;
        const int image_offset = bytes_in_wanted_image * i;


        for (int y = 0; y < wanted_input_height; y++) {
            const int in_y = (y * imageHeight) / wanted_input_height;
            uint8_t* in_row = in + (in_y * imageWidth * imageChannels);
            float* out_row = image_offset + out + (y * wanted_input_width * wanted_input_channels);
            for (int x = 0; x < wanted_input_width; x++) {
                const int in_x = (x * imageWidth) / wanted_input_width;
                uint8_t* in_pixel = in_row + (in_x * imageChannels);
                float* out_pixel = out_row + (x * wanted_input_channels);
                for (int c = 0; c < wanted_input_channels; ++c) {
                    out_pixel[c] = in_pixel[c] / 128.0 - 1;
                }
            }
        }
        
        CFRelease(rawData);
        
    #warning writeTensorEnd
        NSTimeInterval writeTensorEnd = [[NSDate new] timeIntervalSince1970];
        
        prepareTensorForImagesSum += writeTensorEnd - writeTensorStart;
    }

#warning inferInvokeStart
    NSTimeInterval inferInvokeStart = [[NSDate new] timeIntervalSince1970];
    if (interpreter->Invoke() != kTfLiteOk) {
        NSLog(@"Failed to allocate invoke");
    }
#warning inferInvokeStart
    NSTimeInterval inferInvokeEnd = [[NSDate new] timeIntervalSince1970];

    float* output = interpreter->typed_output_tensor<float>(0);
    
    NSMutableArray* result = [[NSMutableArray alloc] init];
    for (int i = 0; i < batchSize; i++) {
        [result addObject:[NSNumber numberWithFloat:output[i]]];
    }
#warning copyResultEnd
    NSTimeInterval copyResultEnd = [[NSDate new] timeIntervalSince1970];

    
    NSTimeInterval total = copyResultEnd - prepareTensorStart;
    NSLog(@"%@ total takes:     %f", name, total);
    NSLog(@"    prepare tensor takes:           %f, %.2f%%", inferInvokeStart - prepareTensorStart, (inferInvokeStart - prepareTensorStart) / total * 100);
    NSLog(@"        avg prepare takes:          %f", prepareTensorForImagesSum / (float) batchSize);
    NSLog(@"    invoke takes:                   %f, %.2f%%", inferInvokeEnd - inferInvokeStart, (inferInvokeEnd - inferInvokeStart) / total * 100);
    
    return result;
}
@end
