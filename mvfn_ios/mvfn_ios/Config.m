//
//  Config.m
//  mvfn_ios
//
//  Created by Chris Fan on 21/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import "Config.h"

@implementation Config

+ (UIColor *)colorForBackground {
    return [[UIColor alloc] initWithRed:37/255. green:37/255. blue:42/255. alpha:1];
}
@end
