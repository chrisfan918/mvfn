//
//  AutoSelectFrameCamViewController.m
//  mvfn_ios
//
//  Created by Chris Fan on 26/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import "AutoSelectFrameCamViewController.h"
#import "AutoSelectFrameCamConfigViewController.h"
#import "ReviewAutoSelectFrameResultViewController.h"
#import "AVHelper.h"
#import "Config.h"
#import "FrameResult.h"
#import "ImageStore.h"

@interface AutoSelectFrameCamViewController () <AVCaptureVideoDataOutputSampleBufferDelegate, AVCapturePhotoCaptureDelegate>

@end

@implementation AutoSelectFrameCamViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.supportedModels = MVFNModel.availableModelsForBatch09;
    self.modelKey = self.supportedModels[0];
    
    [self configAVStuffs];

    self.deviceOrientationWhenShutterFires = [[UIDevice currentDevice] orientation];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleRotation)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
    
    [self.progressView setProgress:0];
    [self.progressView setBackgroundColor: Config.colorForBackground];
    [self.progressView setTrackTintColor: Config.colorForBackground];
    
    [self.view setBackgroundColor: Config.colorForBackground];
    
    self.shutterBtnLongPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleShutterBtnLongPress:)];
    [self.shutterButton setUserInteractionEnabled:true];
    [self.shutterButton addGestureRecognizer:self.shutterBtnLongPressRecognizer];
    
    self.autoSelectFrameCamSession = nil;
    [self.progressView setHidden:true];
    
    [self.historyImageView setHidden:true];
    [self.historyImageView.layer setCornerRadius:10];
    [self.historyImageView setClipsToBounds: true];
    [self.historyImageView setUserInteractionEnabled:true];
    UITapGestureRecognizer* tapHistoryImageView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(triggerReviewAutoSelectFrameViewController)];
    [self.historyImageView addGestureRecognizer:tapHistoryImageView];
    
    
    self.fps = 3;
    self.topN = 1;
}

- (void)viewWillDisappear:(BOOL)animated {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.session stopRunning];
    });
    NSLog(@"viewWillDisappear: session stopRunning");
}

- (void)viewWillAppear:(BOOL)animated{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.session != nil and ![self.session isRunning]) {
            [self.session startRunning];
            NSLog(@"viewDidAppear: session startRunning");
        }
    });
}

- (void) handleRotation {
//    UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
//    switch (deviceOrientation) {
//        case UIDeviceOrientationLandscapeLeft:
//            self.heuristicResultGridView.view.transform = CGAffineTransformMakeRotation(90 * M_PI /180);
//            break;
//        case UIDeviceOrientationLandscapeRight:
//            self.heuristicResultGridView.view.transform = CGAffineTransformMakeRotation(-90 * M_PI /180);
//            break;
//        case UIDeviceOrientationPortrait:
//            self.heuristicResultGridView.view.transform = CGAffineTransformMakeRotation(0 * M_PI /180);
//        default:
//            break;
//    }
}

- (void)captureOutput:(AVCaptureOutput *)output didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection {
    
    if (self.autoSelectFrameCamSession == nil) {
        return;
    }
    
    if (self.autoSelectFrameCamSession.finished) {
        return;
    }
    
    UIImage* uiImage = [AVHelper imageFromSampleBuffer:sampleBuffer];
    
    if (![self.autoSelectFrameCamSession canAcceptNewFrame]){
        return;
    }
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [self animateFlashEffect];
    });
    [self.autoSelectFrameCamSession handleFrame:uiImage];
    NSLog(@"autoSelectFrameCamSession.frames = %lu", (unsigned long)[self.autoSelectFrameCamSession.frames count]);
}

- (void) handleFinishedSession {
    NSMutableArray<UIImage*>* candidates = self.autoSelectFrameCamSession.frames;
    NSMutableArray<NSNumber*>* scores = [[NSMutableArray<NSNumber*> alloc] init];
    NSLog(@"handleFinishedSession with %lu images", (unsigned long)[candidates count]);
    int batchSize = 9;
    NSTimeInterval start = [[NSDate new] timeIntervalSince1970];
    while ([scores count] < [candidates count]){
        int i = (int)[scores count];
        NSMutableArray<UIImage*>* imageBatch = [[NSMutableArray alloc] init];
        if (i + batchSize <= [candidates count]){
            [imageBatch addObjectsFromArray: [candidates subarrayWithRange:NSMakeRange(i, i+batchSize)]];
            NSLog(@"handleFinishedSession infer candidates[%d:%d]", i, i+batchSize);

        } else {
            int j = i;
            while ([imageBatch count] < batchSize){
                NSLog(@"handleFinishedSession infer candidates[%d]", j);
                [imageBatch addObject:candidates[j]];
                if (j < [candidates count]-1) {
                    j++;
                }
            }
        }
        auto batch_scores = [MVFNModel inferWithImages: imageBatch                                        withModel:self.modelKey];
        for (int j = 0; j < [batch_scores count]; j++) {
            [scores addObject:batch_scores[j]];
        }
    }
    while ([candidates count] < [scores count]) {
        [scores removeLastObject];
    }
    self.autoSelectFrameCamSession.scores = scores;
    
    auto results =self.autoSelectFrameCamSession.sortedResults;
    for (int i = 0; i < MIN(self.topN, [results count]); i++) {

        [[ImageStore.shareInstance imageData] addObject:[InferredImageData initWithImage:results[i].image]];
    }
    
    NSTimeInterval end = [[NSDate new] timeIntervalSince1970];
    NSLog(@"AutoSelectFrameCam takes %f to evaluate %lu frames", end - start, (unsigned long)[results count]);
}

- (void) handleShutterBtnLongPress: (UILongPressGestureRecognizer*) sender {
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:{
            NSLog(@"handleShutterBtnLongPress: UIGestureRecognizerStateBegan");
            self.autoSelectFrameCamSession = [[AutoSelectFrameCamSession alloc] initWithFps: self.fps];
            UIImpactFeedbackGenerator* feedbackGenerator = [[UIImpactFeedbackGenerator alloc] initWithStyle:UIImpactFeedbackStyleLight];
            [feedbackGenerator prepare];
            [feedbackGenerator impactOccurred];
            feedbackGenerator = nil;
            break;
        }
        case UIGestureRecognizerStateEnded:{
            NSLog(@"handleShutterBtnLongPress: UIGestureRecognizerStateEnded");
            self.autoSelectFrameCamSession.finished = true;
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [self handleFinishedSession];
            });
            
            UIImpactFeedbackGenerator* feedbackGenerator = [[UIImpactFeedbackGenerator alloc] initWithStyle:UIImpactFeedbackStyleLight];
            [feedbackGenerator prepare];
            [feedbackGenerator impactOccurred];
            feedbackGenerator = nil;
            [self.historyImageView setHidden:false];
            [self.historyImageView setImage:self.autoSelectFrameCamSession.frames.firstObject];
            break;
        }
        default:
            return;
    }
}

- (void) animateFlashEffect {
    UIView* flashView = [[UIView alloc] initWithFrame: [UIScreen mainScreen].bounds];
    flashView.backgroundColor = [UIColor whiteColor];
    flashView.alpha = .6;
    [self.view addSubview:flashView];
    [self.view bringSubviewToFront:flashView];
    NSLog(@"flash");
    [UIView animateWithDuration:.3f animations:^{
        flashView.alpha = 0;
    } completion:^(BOOL animated){
        [flashView removeFromSuperview];
    }];
}


- (void) configAVStuffs {
    AVCaptureDevice* captureDevice = [AVCaptureDevice defaultDeviceWithMediaType: AVMediaTypeVideo];
    AVCaptureDeviceInput* input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:nil];
    self.session = [[AVCaptureSession alloc] init];
    self.session.sessionPreset = AVCaptureSessionPresetPhoto;
    [self.session addInput:input];
    
    self.previewView.layer.cornerRadius = 10;
    self.previewView.layer.masksToBounds = true;
    
    self.videoPreviewLayer = [AVCaptureVideoPreviewLayer layerWithSession:self.session];
    self.videoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    self.videoPreviewLayer.frame = self.previewView.layer.bounds;
    [self.previewView.layer addSublayer:self.videoPreviewLayer];
    
    self.videoOutput = [[AVCaptureVideoDataOutput alloc] init];
    [self.videoOutput setSampleBufferDelegate:self queue:dispatch_queue_create("sample buffer", nil)];
    if ([self.session canAddOutput:self.videoOutput]) {
        [self.session addOutput:self.videoOutput];
    } else {
        NSLog(@"cannot add AVCaptureVideoDataOutput to AVCaptureSession");
    }
    self.connection = [self.videoOutput connectionWithMediaType:AVMediaTypeVideo];
    // offset for AVHelper:CorrectImage
    self.connection.videoOrientation = AVCaptureVideoOrientationLandscapeRight;
    
    
    self.photoOutput = [[AVCapturePhotoOutput alloc] init];
    [self.photoOutput setLivePhotoCaptureEnabled:false];
    [self.photoOutput setHighResolutionCaptureEnabled:false];
    if ([self.session canAddOutput:self.photoOutput]) {
        [self.session addOutput:self.photoOutput];
    } else {
        NSLog(@"cannot add AVCapturePhotoOuput to AVCaptureSession");
    }
    
}

- (void) triggerReviewAutoSelectFrameViewController {
    ReviewAutoSelectFrameResultViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ReviewAutoSelectFrameResultViewController"];
    vc.results = [self.autoSelectFrameCamSession sortedResults];
    [self.navigationController pushViewController:vc animated:true];
}

- (IBAction)onConfigButtonTouched:(id)sender {
    
    AutoSelectFrameCamConfigViewController* vc = [[AutoSelectFrameCamConfigViewController alloc] initWithNibName:@"AutoSelectFrameCamConfigViewController" bundle:nil];
    vc.delegate = self;
    
    [self setDefinesPresentationContext:true];
    self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [nav setToolbarHidden:true];
    [nav setNavigationBarHidden:true];
    nav.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    nav.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:nav animated:true completion:nil];
}

@end

#import "MVFNModel.h"

@implementation AutoSelectFrameCamSession

- (instancetype)initWithFps:(int)fps {
    self = [super init];
    self.fps = fps;
    self.finished = false;
    self.frames = [[NSMutableArray alloc] init];
    return self;
}

- (bool) canAcceptNewFrame {
    if (self.finished) {
        return false;
    }
    if (self.lastFrameTime == nil) {
        return true;
    }
    return [[NSDate new] timeIntervalSinceDate:self.lastFrameTime] >= 1.0/self.fps;
}

- (void)handleFrame:(UIImage *)image {
    [self.frames addObject:image];
    self.lastFrameTime = [NSDate new];
}

- (NSArray<FrameResult *> *)sortedResults {
    if (!self.finished or !self.scores) {
        return nil;
    }
    
    NSMutableArray<FrameResult*>* results = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [self.frames count]; i++) {
        [results addObject:[[FrameResult alloc] initWithImage:self.frames[i] score:self.scores[i]]];
    }
    
    [results sortUsingComparator:^(id a, id b) {
        auto _a = [(FrameResult*) a score].floatValue;
        auto _b = [(FrameResult*) b score].floatValue;
        if (_a > _b) {
            return NSOrderedAscending;
        } else if (_a < _b) {
            return NSOrderedDescending;
        } else {
            return NSOrderedSame;
        }
    }];
    
    return results;
}
@end
