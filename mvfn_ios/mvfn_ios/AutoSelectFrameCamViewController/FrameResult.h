//
//  FrameResult.h
//  mvfn_ios
//
//  Created by Chris Fan on 26/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface FrameResult: NSObject
@property UIImage* image;
@property NSNumber* score;

- (instancetype) initWithImage: (UIImage*) image
                         score: (NSNumber*) score;

@end

NS_ASSUME_NONNULL_END
