//
//  ReviewAutoSelectFrameResultViewController.m
//  mvfn_ios
//
//  Created by Chris Fan on 26/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import "ReviewAutoSelectFrameResultViewController.h"
#import "AutoSelectFrameResultCollectionViewCell.h"
#import "Config.h"
#import "ImageStore.h"

@interface ReviewAutoSelectFrameResultViewController () <UICollectionViewDelegate, UICollectionViewDataSource>

@end

@implementation ReviewAutoSelectFrameResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    [self.collectionView  registerNib:[UINib nibWithNibName:@"AutoSelectFrameResultCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.imageView.image = self.results[0].image;
    
    self.collectionView.backgroundColor = Config.colorForBackground;
    
    UIBarButtonItem* rightBarButton = [[UIBarButtonItem alloc]
                                       initWithTitle:@"save"
                                       style:UIBarButtonItemStylePlain
                                       target:self
                                       action:@selector(onSaveButtonTouched)];
    
    [self.navigationItem setRightBarButtonItem:rightBarButton];
}

- (void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:false];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:true];
}


- (void) onSaveButtonTouched {
    [[ImageStore.shareInstance imageData] addObject:[InferredImageData initWithImage:self.imageView.image]];
    auto alertVC = [UIAlertController alertControllerWithTitle:@"Saving Image"
                    
                                                       message:@"current image has been saved to library tab"
                                                preferredStyle: UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSLog(@"OK action");
                               }];
    
    [alertVC addAction:okAction];
    [self presentViewController:alertVC animated:true completion:nil];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.results count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    AutoSelectFrameResultCollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.imageView.image = self.results[indexPath.row].image;
    cell.label.text = [NSString stringWithFormat:@"score: % 7.4f", self.results[indexPath.row].score.floatValue];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    self.imageView.image = self.results[indexPath.row].image;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return AutoSelectFrameResultCollectionViewCell.size;
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0;
}


@end
