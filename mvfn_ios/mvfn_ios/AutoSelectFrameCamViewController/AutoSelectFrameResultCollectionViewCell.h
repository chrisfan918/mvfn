//
//  AutoSelectFrameResultCollectionViewCell.h
//  mvfn_ios
//
//  Created by Chris Fan on 26/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AutoSelectFrameResultCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *label;

+ (CGSize) size;
@end

NS_ASSUME_NONNULL_END
