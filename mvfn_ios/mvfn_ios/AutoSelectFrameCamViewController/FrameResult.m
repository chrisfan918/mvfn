//
//  FrameResult.m
//  mvfn_ios
//
//  Created by Chris Fan on 26/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import "FrameResult.h"


@implementation FrameResult

- (instancetype)initWithImage:(UIImage *)image score:(NSNumber *)score {
    self = [super init];
    self.image = image;
    self.score = score;
    return self;
}

@end
