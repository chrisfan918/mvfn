//
//  AutoSelectFrameCamViewController.h
//  mvfn_ios
//
//  Created by Chris Fan on 26/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MVFNModel.h"
#import "LiveHeuristicViewController.h"
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "FrameResult.h"

NS_ASSUME_NONNULL_BEGIN

@interface AutoSelectFrameCamSession: NSObject
@property int fps;

@property (atomic) NSDate* lastFrameTime;
@property NSMutableArray<UIImage*>* frames;
@property NSMutableArray<NSNumber*>* scores;
@property (atomic) bool finished;

- (instancetype) initWithFps: (int) fps;
- (bool) canAcceptNewFrame;
- (void) handleFrame: (UIImage*) image;
- (NSArray<FrameResult*>*) sortedResults;
@end


@interface AutoSelectFrameCamViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *previewView;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UIButton *shutterButton;
@property (weak, nonatomic) IBOutlet UIImageView *historyImageView;

@property (atomic) NSString* modelKey;
@property NSArray<NSString* >* supportedModels;
@property (atomic) UIDeviceOrientation deviceOrientationWhenShutterFires;

@property int fps;
@property int topN;

@property UIImage* currentFrame;
@property AVCaptureSession* session;
@property AVCaptureConnection* connection;
@property AVCaptureVideoDataOutput* videoOutput;
@property AVCaptureVideoPreviewLayer* videoPreviewLayer;
@property AVCapturePhotoOutput* photoOutput;
@property UILongPressGestureRecognizer* shutterBtnLongPressRecognizer;
@property AutoSelectFrameCamSession* autoSelectFrameCamSession;
@end


NS_ASSUME_NONNULL_END
