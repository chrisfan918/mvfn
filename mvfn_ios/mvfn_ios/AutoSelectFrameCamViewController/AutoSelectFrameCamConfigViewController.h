//
//  AutoSelectFrameCamConfigViewController.h
//  mvfn_ios
//
//  Created by Chris Fan on 27/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AutoSelectFrameCamViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface AutoSelectFrameCamConfigViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *fpsTextField;
@property (weak, nonatomic) IBOutlet UITextField *topNTextField;
@property (weak, nonatomic) IBOutlet UIPickerView *modelKeyPickerView;
@property (weak, nonatomic) IBOutlet UIButton *setButton;
@property (weak, nonatomic) IBOutlet UIView *baseModalView;

@property (weak) AutoSelectFrameCamViewController* delegate;
@end

NS_ASSUME_NONNULL_END
