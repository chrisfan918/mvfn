//
//  AutoSelectFrameResultCollectionViewCell.m
//  mvfn_ios
//
//  Created by Chris Fan on 26/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import "AutoSelectFrameResultCollectionViewCell.h"

@implementation AutoSelectFrameResultCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    [self.layer setCornerRadius:10];
//    self.clipsToBounds = true;
//    self.backgroundColor = [UIColor lightGrayColor];
    
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.imageView setContentMode:UIViewContentModeScaleAspectFit];
    self.imageView.layer.cornerRadius = 10;
    [self.imageView setClipsToBounds:true];
    
    self.label.textColor = [UIColor lightGrayColor];
}


+ (CGSize) size {
    CGFloat width = ([[UIScreen mainScreen] bounds].size.width) / 2;
    return CGSizeMake(width, width * 1.25);
}
@end
