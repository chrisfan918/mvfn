//
//  AutoSelectFrameCamConfigViewController.m
//  mvfn_ios
//
//  Created by Chris Fan on 27/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import "AutoSelectFrameCamConfigViewController.h"
#import "MVFNModel.h"
#import "ViewFindingHelper.h"
#import "Config.h"
#import "mixin.h"

@interface AutoSelectFrameCamConfigViewController () <UIPickerViewDataSource, UIPickerViewDelegate>
@property NSArray<NSString*>* availableModelKeys;

@end

@implementation AutoSelectFrameCamConfigViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.availableModelKeys = self.delegate.supportedModels;
    self.modelKeyPickerView.dataSource = self;
    self.modelKeyPickerView.delegate = self;
    [self.modelKeyPickerView reloadAllComponents];
    
    [self.fpsTextField addDoneToolBar];
    [self.topNTextField addDoneToolBar];
    
    UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onViewTapped:)];
    [self.view addGestureRecognizer:tapGesture];
    
    self.baseModalView.layer.cornerRadius = 10;
    self.baseModalView.clipsToBounds = true;

}

- (void)viewWillAppear:(BOOL)animated {
    // set value to delegate
    self.fpsTextField.text = [NSString stringWithFormat:@"%d", self.delegate.fps];
    self.topNTextField.text = [NSString stringWithFormat:@"%d", self.delegate.topN];
    [self.modelKeyPickerView selectRow:[self.availableModelKeys indexOfObject:self.delegate.modelKey]
                           inComponent:0
                              animated:true];
}


- (void) onViewTapped: (UITapGestureRecognizer*) sender {
    auto loc = [sender locationInView:self.view];
    if (!CGRectContainsPoint(self.baseModalView.frame, loc)) {
        [self dismissViewControllerAnimated:true completion:nil];
    }
}

- (IBAction)onSetButtonTouched:(id)sender {
    NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
    
    self.delegate.topN = MIN(10, MAX(1,(int)[[formatter numberFromString: [self.topNTextField text]] doubleValue]));
    self.delegate.fps = MIN(20, MAX(1,(int)[[formatter numberFromString: [self.fpsTextField text]] doubleValue]));
    self.delegate.modelKey = self.availableModelKeys[[self.modelKeyPickerView selectedRowInComponent:0]];
    [self dismissViewControllerAnimated:true completion:nil];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.availableModelKeys count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return self.availableModelKeys[row];
}
@end
