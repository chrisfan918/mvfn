//
//  ReviewAutoSelectFrameResultViewController.h
//  mvfn_ios
//
//  Created by Chris Fan on 26/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FrameResult.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReviewAutoSelectFrameResultViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property NSArray<FrameResult*>* results;
@end

NS_ASSUME_NONNULL_END
