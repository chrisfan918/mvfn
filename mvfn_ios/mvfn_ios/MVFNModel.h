//
//  MVFNModel.h
//  mvfn_ios
//
//  Created by Chris Fan on 13/2/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MVFNModel : NSObject

extern NSString * const kMVFN096Batch11Quant;
extern NSString * const kMVFN160Batch11Quant;
extern NSString * const kMVFN224Batch11Quant;

extern NSString * const kMVFN096Batch09Quant;
extern NSString * const kMVFN160Batch09Quant;
extern NSString * const kMVFN224Batch09Quant;

extern NSString * const kMVFN096Batch11;
extern NSString * const kMVFN160Batch11;
extern NSString * const kMVFN224Batch11;

extern NSString * const kMVFN096Batch09;
extern NSString * const kMVFN160Batch09;
extern NSString * const kMVFN224Batch09;

extern NSString * const kMVFN096;
extern NSString * const kMVFN160;
extern NSString * const kMVFN224;

+ (NSArray<NSString*>*) availableModelsForSingleInfer;
+ (NSArray<NSString*>*) availableModelsForBatch09;
+ (NSArray<NSString*>*) availableModelsForBatch11;

+ (NSDictionary<NSString *, NSNumber *>*) inferWithImage: (UIImage*) image;
+ (NSNumber*) inferWithImage: (UIImage*) image
                   withModel: (NSString*) modelKey;
+ (NSArray<NSNumber*>*) inferWithImages: (NSArray<UIImage*>*) images
                              withModel: (NSString*) modelKey;

+ (instancetype) sharedMVFN096;
+ (instancetype) sharedMVFN160;
+ (instancetype) sharedMVFN224;

+ (instancetype) sharedMVFN096Batch09;
+ (instancetype) sharedMVFN160Batch09;
+ (instancetype) sharedMVFN224Batch09;

+ (instancetype) sharedMVFN096Batch11;
+ (instancetype) sharedMVFN160Batch11;
+ (instancetype) sharedMVFN224Batch11;

+ (instancetype) sharedMVFN096Batch09Quant;
+ (instancetype) sharedMVFN160Batch09Quant;
+ (instancetype) sharedMVFN224Batch09Quant;

+ (instancetype) sharedMVFN096Batch11Quant;
+ (instancetype) sharedMVFN160Batch11Quant;
+ (instancetype) sharedMVFN224Batch11Quant;


- (instancetype) initWithModelName: (NSString*) name
                          inputDim: (CGFloat) inputDim
                         batchSize: (int) batchSize;
- (NSArray<NSNumber*>*) inferWithImages: (NSArray<UIImage*>*) images;
@end
