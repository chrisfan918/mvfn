//
//  HeuristicCompass.h
//  mvfn_ios
//
//  Created by Chris Fan on 20/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeuristicResultTracker.h"

NS_ASSUME_NONNULL_BEGIN

@interface HeuristicCompass : UIView
@property float thresholdForRotationHeuristic;
@property float thresholdForTranslationHeuristic;

- (void) drawForHeuristicResult: (HeuristicResult*) results;
@property (atomic) bool isDrawing;
@end

NS_ASSUME_NONNULL_END
