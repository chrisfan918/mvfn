//
//  HeuristicResultTracker.h
//  mvfn_ios
//
//  Created by Chris Fan on 15/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "mixin.h"

NS_ASSUME_NONNULL_BEGIN

typedef NSArray<NSNumber*> HeuristicResult;

typedef NS_ENUM(NSUInteger, HeuristicResultTrackerPreprocessingMode) {
    HeuristicResultTrackerPreprocessingModeNone,
    HeuristicResultTrackerPreprocessingModeCenterDiff,
    HeuristicResultTrackerPreprocessingModeNormalize,
};

typedef NS_ENUM(NSUInteger, HeuristicResultTrackerAggregationMode) {
    HeuristicResultTrackerAggregationModeLast,
    HeuristicResultTrackerAggregationModeAverage,
    HeuristicResultTrackerAggregationModeSum,
};

@interface HeuristicResultTracker : NSObject

@property int maxHistoryLength;
@property HeuristicResultTrackerPreprocessingMode preprocessingMode;
@property HeuristicResultTrackerAggregationMode aggregationMode;
@property NSMutableArray<HeuristicResult*>* results;

- (instancetype) initWithMaxHistoryLength: (int) historyLength
                         preprocssingMode: (HeuristicResultTrackerPreprocessingMode) preprocessingMode
                          aggregationMode: (HeuristicResultTrackerAggregationMode) aggregationMode;

- (HeuristicResult*) handle: (HeuristicResult*) result;

@end

NS_ASSUME_NONNULL_END
