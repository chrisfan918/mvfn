//
//  AVHelper.m
//  mvfn_ios
//
//  Created by Chris Fan on 26/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import "AVHelper.h"
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@implementation AVHelper



+ (UIImage*) imageFromSampleBuffer: (CMSampleBufferRef) sampleBuffer {
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    return [self imageFromCVPixelBuffer:imageBuffer];
}

+ (UIImage*) imageFromCVPixelBuffer: (CVPixelBufferRef) cvPixelBuffer {
    
    CIContext* context = [CIContext contextWithOptions:nil];
    CIImage* ciImage = [CIImage imageWithCVPixelBuffer:cvPixelBuffer];
    CGImageRef cgImage = [context createCGImage:ciImage fromRect: [ciImage extent]];
    
    UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
    UIImage* uiImage = [AVHelper correctImage:cgImage forDeviceOrientation:deviceOrientation];
    CGImageRelease(cgImage);
    return uiImage;
    
}


+ (UIImage*) correctImage: (CGImageRef) cgImage
     forDeviceOrientation: (UIDeviceOrientation) deviceOrientation {
    
    CGFloat originalWidth = CGImageGetWidth(cgImage);
    CGFloat originalHeight = CGImageGetHeight(cgImage);
    CGRect originalRect = CGRectMake(0, 0, originalWidth, originalHeight);
    
    switch (deviceOrientation) {
        case UIDeviceOrientationPortrait: { // Device oriented vertically, home button on the bottom
            CGSize size = CGSizeMake(originalHeight, originalWidth);
            
            UIGraphicsBeginImageContext(size);
            CGContextRef context = UIGraphicsGetCurrentContext();
            CGContextScaleCTM(context, -1, 1);
            CGContextRotateCTM(context, M_PI_2);
            CGContextDrawImage(context, originalRect, cgImage);
            CGImageRef correctedCGImage = CGBitmapContextCreateImage(context);
            UIGraphicsEndImageContext();
            UIImage* uiImage = [UIImage imageWithCGImage:correctedCGImage];
            CGImageRelease(correctedCGImage);
            return uiImage;
        }
            
        case UIDeviceOrientationLandscapeLeft: {      // Device oriented horizontally, home button on the right
            CGSize size = CGSizeMake(originalWidth, originalHeight);
            UIGraphicsBeginImageContext(size);
            CGContextRef context = UIGraphicsGetCurrentContext();
            CGContextTranslateCTM(context, originalWidth/2, originalHeight/2);
            CGContextRotateCTM(context, M_PI);
            CGContextScaleCTM(context, -1, 1);
            CGContextTranslateCTM(context, -originalWidth/2, -originalHeight/2);
            CGContextDrawImage(context, originalRect, cgImage);
            CGImageRef correctedCGImage = CGBitmapContextCreateImage(context);
            UIImage* uiImage = [UIImage imageWithCGImage:correctedCGImage];
            CGImageRelease(correctedCGImage);
            return uiImage;
        }
            
        case UIDeviceOrientationLandscapeRight: {     // Device oriented horizontally, home button on the left
            CGSize size = CGSizeMake(originalWidth, originalHeight);
            UIGraphicsBeginImageContext(size);
            CGContextRef context = UIGraphicsGetCurrentContext();
            CGContextTranslateCTM(context, originalWidth/2, originalHeight/2);
            CGContextRotateCTM(context, -90 / 180 * M_PI);
            CGContextScaleCTM(context, -1, 1);
            CGContextTranslateCTM(context, -originalWidth/2, -originalHeight/2);
            CGContextDrawImage(context, originalRect, cgImage);
            CGImageRef correctedCGImage = CGBitmapContextCreateImage(context);
            UIImage* uiImage = [UIImage imageWithCGImage:correctedCGImage];
            CGImageRelease(correctedCGImage);
            return uiImage;
        }
            
        default:
            return [UIImage imageWithCGImage:cgImage];
    }
}

@end
