//
//  AVHelper.h
//  mvfn_ios
//
//  Created by Chris Fan on 26/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AVHelper : NSObject

+ (UIImage*) imageFromSampleBuffer: (CMSampleBufferRef) sampleBuffer;
+ (UIImage*) imageFromCVPixelBuffer: (CVPixelBufferRef) cvPixelBuffer;
+ (UIImage*) correctImage: (CGImageRef) cgImage
     forDeviceOrientation: (UIDeviceOrientation) deviceOrientation;
@end

NS_ASSUME_NONNULL_END
