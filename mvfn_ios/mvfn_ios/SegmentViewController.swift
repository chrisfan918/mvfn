//
//  SegmentViewController.swift
//  mvfn_ios
//
//  Created by Chris Fan on 21/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

import UIKit
import Parchment

class SegmentViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = Config.colorForBackground()
        
        let batchInferNavigationController = UINavigationController()
        configNavigationController(navVC: batchInferNavigationController)
        
        let batchInferViewController = storyboard?.instantiateViewController(withIdentifier: "BatchInferViewController") as! BatchInferViewController
        batchInferNavigationController.pushViewController(batchInferViewController, animated: true)
        
        let liveHeuristicViewController = storyboard?.instantiateViewController(withIdentifier: "LiveHeuristicViewController") as! LiveHeuristicViewController
        let liveHeuristicNavigationController = UINavigationController()
        liveHeuristicNavigationController.setNavigationBarHidden(true, animated: false)
        liveHeuristicNavigationController.setToolbarHidden(true, animated: false)
        liveHeuristicNavigationController.pushViewController(liveHeuristicViewController, animated: true)
        configNavigationController(navVC: liveHeuristicNavigationController)
        
        let autoSelectFrameCamViewController = storyboard?.instantiateViewController(withIdentifier: "AutoSelectFrameCamViewController") as! AutoSelectFrameCamViewController
        let autoSelectFrameCamNavViewController = UINavigationController()
        autoSelectFrameCamNavViewController.setNavigationBarHidden(true, animated: false)
        autoSelectFrameCamNavViewController.setToolbarHidden(true, animated: false)
        autoSelectFrameCamNavViewController.pushViewController(autoSelectFrameCamViewController, animated:true)
        configNavigationController(navVC: autoSelectFrameCamNavViewController)
        
        
        
        
        // Do any additional setup after loading the view.
        let pagingViewController = FixedPagingViewController(viewControllers: [
            liveHeuristicNavigationController,
            batchInferNavigationController,
            autoSelectFrameCamNavViewController
            ])
        
        addChild(pagingViewController)
        view.addSubview(pagingViewController.view)
        pagingViewController.didMove(toParent: self)
        pagingViewController.view.translatesAutoresizingMaskIntoConstraints = false
        pagingViewController.select(index: 1);
        
        pagingViewController.backgroundColor = Config.colorForBackground()
        pagingViewController.menuBackgroundColor = Config.colorForBackground()
        pagingViewController.borderColor = Config.colorForBackground()
        pagingViewController.selectedBackgroundColor = Config.colorForBackground()
        pagingViewController.selectedTextColor = UIColor.white
        pagingViewController.textColor = UIColor.white
        
        NSLayoutConstraint.activate([
            pagingViewController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            pagingViewController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            pagingViewController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            pagingViewController.view.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor)
        ])
    }
    
    func configNavigationController(navVC: UINavigationController) {
        
        navVC.view.backgroundColor = Config.colorForBackground()
        navVC.navigationBar.barTintColor = Config.colorForBackground()
        navVC.navigationBar.tintColor = UIColor.white
        navVC.navigationBar.isTranslucent = false
        navVC.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white];
    }
    
    override var prefersHomeIndicatorAutoHidden: Bool {
        return true;
    }

}
