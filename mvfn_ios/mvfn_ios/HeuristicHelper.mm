//
//  HeuristicHelper.m
//  mvfn_ios
//
//  Created by Chris Fan on 11/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import "HeuristicHelper.h"
#import "InferredImageData.h"
#import "MVFNModel.h"
#import <UIKit/UIKit.h>
#import <math.h>


extern NSUInteger const HeuristicModeCountWithoutRotation = HeuristicModeCount - 2;

@implementation HeuristicHelper

+ (UIImage*) crop: (UIImage*) originImage
withHeuristicMode: (HeuristicMode) mode {
    
    CGFloat scale = .85;
    CGFloat rotateAngle = 5;
    
    CGFloat outputWidth = originImage.size.width * scale;
    CGFloat outputHeight = originImage.size.height * scale;
    CGFloat dx = (originImage.size.width - outputWidth) / 2;
    CGFloat dy = (originImage.size.height - outputHeight) / 2;
    
    switch (mode) {
        case kHeuristicModeCenter: {
            CGRect rect = CGRectMake(dx,
                                     dy,
                                     outputWidth,
                                     outputHeight);
            CGImageRef imageRef = CGImageCreateWithImageInRect(originImage.CGImage, rect);
            UIImage *result = [UIImage imageWithCGImage:imageRef scale:scale orientation:originImage.imageOrientation];
            CGImageRelease(imageRef);
            return result;
        }
        case kHeuristicModeUpper:{
            CGRect rect = CGRectMake(dx,
                                     0,
                                     outputWidth,
                                     outputHeight);
            CGImageRef imageRef = CGImageCreateWithImageInRect(originImage.CGImage, rect);
            UIImage *result = [UIImage imageWithCGImage:imageRef scale:scale orientation:originImage.imageOrientation];
            CGImageRelease(imageRef);
            return result;
        }
        case kHeuristicModeLeft:{
            CGRect rect = CGRectMake(0,
                                     dy,
                                     outputWidth,
                                     outputHeight);
            CGImageRef imageRef = CGImageCreateWithImageInRect(originImage.CGImage, rect);
            UIImage *result = [UIImage imageWithCGImage:imageRef scale:scale orientation:originImage.imageOrientation];
            CGImageRelease(imageRef);
            return result;
        }
        case kHeuristicModeRight:{
            CGRect rect = CGRectMake(originImage.size.width - outputWidth,
                                     dy,
                                     outputWidth,
                                     outputHeight);
            CGImageRef imageRef = CGImageCreateWithImageInRect(originImage.CGImage, rect);
            UIImage *result = [UIImage imageWithCGImage:imageRef scale:scale orientation:originImage.imageOrientation];
            CGImageRelease(imageRef);
            return result;
        }
        case kHeuristicModeLower:{
            CGRect rect = CGRectMake(dx,
                                     originImage.size.height - outputHeight,
                                     outputWidth,
                                     outputHeight);
            CGImageRef imageRef = CGImageCreateWithImageInRect(originImage.CGImage, rect);
            UIImage *result = [UIImage imageWithCGImage:imageRef scale:scale orientation:originImage.imageOrientation];
            CGImageRelease(imageRef);
            return result;
        }
        case kHeuristicModeUpperLeft:{
            CGRect rect = CGRectMake(0,
                                     0,
                                     outputWidth,
                                     outputHeight);
            CGImageRef imageRef = CGImageCreateWithImageInRect(originImage.CGImage, rect);
            UIImage *result = [UIImage imageWithCGImage:imageRef scale:scale orientation:originImage.imageOrientation];
            CGImageRelease(imageRef);
            return result;
        }
        case kHeuristicModeUpperRight:{
            CGRect rect = CGRectMake(originImage.size.width - outputWidth,
                                     0,
                                     outputWidth,
                                     outputHeight);
            CGImageRef imageRef = CGImageCreateWithImageInRect(originImage.CGImage, rect);
            UIImage *result = [UIImage imageWithCGImage:imageRef scale:scale orientation:originImage.imageOrientation];
            CGImageRelease(imageRef);
            return result;
        }
        case kHeuristicModeLowerLeft:{
            CGRect rect = CGRectMake(0,
                                     originImage.size.height - outputHeight,
                                     outputWidth,
                                     outputHeight);
            CGImageRef imageRef = CGImageCreateWithImageInRect(originImage.CGImage, rect);
            UIImage *result = [UIImage imageWithCGImage:imageRef scale:scale orientation:originImage.imageOrientation];
            CGImageRelease(imageRef);
            return result;
        }
        case kHeuristicModeLowerRight:{
            CGRect rect = CGRectMake(originImage.size.width - outputWidth,
                                     originImage.size.height - outputHeight,
                                     outputWidth,
                                     outputHeight);
            CGImageRef imageRef = CGImageCreateWithImageInRect(originImage.CGImage, rect);
            UIImage *result = [UIImage imageWithCGImage:imageRef scale:scale orientation:originImage.imageOrientation];
            CGImageRelease(imageRef);
            return result;
        }
            
        case kHeuristicModeAntiClockWise:{
            return [HeuristicHelper rotateImage:originImage angleInDegree:-rotateAngle];
        }
        case kHeuristicModeClockWise: {
            return [HeuristicHelper rotateImage:originImage angleInDegree:rotateAngle];
        }
        default:
            return nil;
            
    }
    return nil;
}

+ (UIImage*) rotateImage: (UIImage*) originImage
           angleInDegree: (CGFloat) degree {
    CGFloat radian = [HeuristicHelper getRadianWithDegree:degree];
    CGSize maxArea = [HeuristicHelper getMaxSizeAfterRotate:originImage.size angleInRadian: radian];
    
    UIGraphicsBeginImageContext(originImage.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // move origin to center
    CGContextTranslateCTM(context, originImage.size.width/2, originImage.size.height/2);
    
    // rotate
    CGContextRotateCTM(context, radian);
    
    // move back origin
    CGContextTranslateCTM(context, -originImage.size.width/2, -originImage.size.height/2);
    
    
    [originImage drawAtPoint:CGPointZero];
    
    UIImage* imageToCrop = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // crop center
    CGFloat dw = originImage.size.width - maxArea.width;
    CGFloat dh = originImage.size.height - maxArea.height;
    CGRect rect = CGRectMake(dw/2, dh/2, maxArea.width, maxArea.height);
    CGImageRef imageRef = CGImageCreateWithImageInRect([imageToCrop CGImage], rect);
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return cropped;
}

+ (CGFloat) getRadianWithDegree: (CGFloat) degree {
    return degree * M_PI / 180;
}


// https://stackoverflow.com/questions/16702966/rotate-image-and-crop-out-black-borders
+ (CGSize) getMaxSizeAfterRotate: (CGSize) frame angleInRadian: (CGFloat) angle {
    if (frame.width <= 0 or frame.height <= 0) {
        return CGSizeZero;
    }
    float side_long = 0;
    float side_short = 0;
    
    if (frame.width >= frame.height) {
        side_long = frame.width;
        side_short = frame.height;
    } else {
        side_long = frame.height;
        side_short = frame.width;
    }
    
    float sin_a = abs(sin(angle));
    float cos_a = abs(cos(angle));
    CGSize result;
    
    if (side_short <= 2 * sin_a*cos_a*side_long or abs(sin_a-cos_a) < 1e-10) {
        float x = 0.5*side_short;
        if (frame.width >= frame.height) {
            result = CGSizeMake(x/sin_a, x/cos_a);
        } else {
            result = CGSizeMake(x/cos_a, x/sin_a);
        }
    } else {
        float cos_2a = cos_a*cos_a - sin_a*sin_a;
        result = CGSizeMake((frame.width * cos_a - frame.height * sin_a)/cos_2a,
                            (frame.height * cos_a- frame.width*sin_a)/cos_2a);
    }
    
    return result;
}

+ (NSArray<UIImage*>*) generateHeuristicImagesWithImage: (UIImage*) image {
    NSMutableArray* results = [[NSMutableArray alloc] init];
    for (int mode = 0; mode < HeuristicModeCount; mode++) {
        [results addObject:[HeuristicHelper crop:image withHeuristicMode: (HeuristicMode) mode]];
    }
    return results;
}

+ (NSArray<NSNumber*>*) inferImages: (NSArray<UIImage*>*) images
                       withModelKey: (NSString*) modelKey {
    return [MVFNModel inferWithImages:images withModel:modelKey];
}

+ (NSString *)getNameWithMode:(HeuristicMode)mode {
    switch (mode) {

        case kHeuristicModeCenter:
            return @"kHeuristicModeCenter";
        case kHeuristicModeUpper:
            return @"kHeuristicModeUpper";
        case kHeuristicModeLeft:
            return @"kHeuristicModeLeft";
        case kHeuristicModeRight:
            return @"kHeuristicModeRight";
        case kHeuristicModeLower:
            return @"kHeuristicModeLower";
        case kHeuristicModeUpperLeft:
            return @"kHeuristicModeUpperLeft";
        case kHeuristicModeUpperRight:
            return @"kHeuristicModeUpperRight";
        case kHeuristicModeLowerLeft:
            return @"kHeuristicModeLowerLeft";
        case kHeuristicModeLowerRight:
            return @"kHeuristicModeLowerRight";
        case HeuristicModeCount:
            return @"HeuristicModeCount";
        case kHeuristicModeClockWise:
            return @"kHeuristicModeClockWise";
        case kHeuristicModeAntiClockWise:
            return @"kHeuristicModeAntiClockWise";
        default:
            return @"unknown";
            
    }
}


+ (UIColor*) colorForHeuristicMode: (HeuristicMode) mode {
    switch (mode) {
        case kHeuristicModeUpper:
            return [UIColor blueColor];
        case kHeuristicModeLeft:
            return [UIColor yellowColor];
        case kHeuristicModeRight:
            return [UIColor redColor];
        case kHeuristicModeLower:
            return [UIColor greenColor];
            
        case kHeuristicModeUpperLeft:
            return [UIColor blueColor];
        case kHeuristicModeUpperRight:
            return [UIColor redColor];
        case kHeuristicModeLowerLeft:
            return [UIColor yellowColor];
        case kHeuristicModeLowerRight:
            return [UIColor greenColor];
            
        default:
            return [UIColor whiteColor];
    }
}

+ (CGFloat) radianForHeuristicMode: (HeuristicMode) mode {
    switch (mode) {
        case kHeuristicModeUpper:
            return 270 * M_PI / 180;
        case kHeuristicModeLeft:
            return 180 * M_PI / 180;
        case kHeuristicModeRight:
            return 0 * M_PI / 180;
        case kHeuristicModeLower:
            return 90 * M_PI / 180;
        case kHeuristicModeUpperLeft:
            return 225 * M_PI / 180;
        case kHeuristicModeUpperRight:
            return 315 * M_PI / 180;
        case kHeuristicModeLowerLeft:
            return 135 * M_PI / 180;
        case kHeuristicModeLowerRight:
            return 45 * M_PI / 180;
        default:
            return 0;
    }
}

@end
