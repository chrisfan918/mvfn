//
//  HeuristicResultGridView.m
//  mvfn_ios
//
//  Created by Chris Fan on 13/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import "HeuristicResultGridView.h"
#import "HeuristicHelper.h"
#import "Config.h"
#import <UIKit/UIKit.h>

@implementation HeuristicResultGridView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setShouldShow:(bool)shouldShow {
    _shouldShow = shouldShow;
    if (!self.shouldShow) {
        
        for (int mode = 0; mode < HeuristicModeCount; mode++) {
            UILabel* label = [self getLabelWithMode:(HeuristicMode) mode];
            [label setText:@""];
        }
        
        [self.miscLabel setText:@""];
    }
}

- (void) setup {
    UINib* nib = [UINib nibWithNibName:@"HeuristicResultGridView" bundle:nil];
    UIView* view = (UIView*) [nib instantiateWithOwner:self options:nil][0];
    [self addSubview: view];
    view.frame = [self bounds];
    [self addSubview:view];
    self.view = view;
    
    [self.view setBackgroundColor: Config.colorForBackground];
    self.miscLabel.textColor = [UIColor whiteColor];
    self.clockWiseLabel.textColor = [UIColor whiteColor];
    self.antiClockWiseLabel.textColor = [UIColor whiteColor];
};

- (void)setResults:(NSArray<NSNumber *> *)results withRemark:(NSString *)remark {
    if (!self.shouldShow) {
        return;
    }
    NSSortDescriptor * ascending = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending: true];
    NSArray<NSNumber*>* sortedResults = [results sortedArrayUsingDescriptors:[NSArray arrayWithObject:ascending]];
    
    for (int mode = 0; mode < [results count]; mode++) {
        UILabel* label = [self getLabelWithMode:(HeuristicMode) mode];
        NSNumber* score = results[mode];
        [label setText:[NSString stringWithFormat:@"% 8.4f", score.floatValue]];
        NSUInteger index = [sortedResults indexOfObject:score];
        [label setTextColor: [self getColorBaseOnIndex:index count:[results count]]];
    }
    
    [self.miscLabel setText:remark];
}

- (UIColor*) getColorBaseOnIndex: (NSUInteger) index
                          count: (NSUInteger) count {
    CGFloat baseValue = 0.4;
    CGFloat value = baseValue + (baseValue) * (index) / count;
    return [[UIColor alloc] initWithRed:value green:value blue:value alpha:1.0];
}

- (UILabel*) getLabelWithMode: (HeuristicMode) mode {
    switch (mode) {
        
        case kHeuristicModeCenter: {
            return self.centerLabel;
        }
        case kHeuristicModeUpper: {
            return self.upperLabel;
        }
        case kHeuristicModeLeft: {
            return self.leftLabel;
        }
        case kHeuristicModeRight: {
            return self.rightLabel;
        }
        case kHeuristicModeLower: {
            return self.lowerLabel;
        }
        case kHeuristicModeUpperLeft: {
            return self.upperLeftLabel;
        }
        case kHeuristicModeUpperRight: {
            return self.upperRightLabel;
        }
        case kHeuristicModeLowerLeft: {
            return self.lowerLeftLabel;
        }
        case kHeuristicModeLowerRight: {
            return self.lowerRightLabel;
        }
        case kHeuristicModeClockWise: {
            return self.clockWiseLabel;
        }
        case kHeuristicModeAntiClockWise: {
            return self.antiClockWiseLabel;
        }
        default:
            return nil;
    }
}

@end
