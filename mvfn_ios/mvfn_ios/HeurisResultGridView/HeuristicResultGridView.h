//
//  HeuristicResultGridView.h
//  mvfn_ios
//
//  Created by Chris Fan on 13/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
IB_DESIGNABLE
@interface HeuristicResultGridView : UIView
@property UIView* view;
@property (weak, nonatomic) IBOutlet UILabel *upperLeftLabel;
@property (weak, nonatomic) IBOutlet UILabel *upperLabel;
@property (weak, nonatomic) IBOutlet UILabel *upperRightLabel;
@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UILabel *centerLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;
@property (weak, nonatomic) IBOutlet UILabel *lowerLeftLabel;
@property (weak, nonatomic) IBOutlet UILabel *lowerLabel;
@property (weak, nonatomic) IBOutlet UILabel *lowerRightLabel;
@property (weak, nonatomic) IBOutlet UILabel *antiClockWiseLabel;
@property (weak, nonatomic) IBOutlet UILabel *clockWiseLabel;
@property (weak, nonatomic) IBOutlet UILabel *miscLabel;
@property (nonatomic) bool shouldShow;


- (void) setResults: (NSArray<NSNumber*>*) results
         withRemark: (NSString*) remark;
@end

NS_ASSUME_NONNULL_END
