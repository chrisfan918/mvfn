//
//  HeuristicHelper.h
//  mvfn_ios
//
//  Created by Chris Fan on 11/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, HeuristicMode) {
    kHeuristicModeCenter,
    kHeuristicModeUpper,
    kHeuristicModeLeft,
    kHeuristicModeRight,
    kHeuristicModeLower,
    kHeuristicModeUpperLeft,
    kHeuristicModeUpperRight,
    kHeuristicModeLowerLeft,
    kHeuristicModeLowerRight,
    
    kHeuristicModeClockWise,
    kHeuristicModeAntiClockWise,
    
    HeuristicModeCount
};

extern NSUInteger const HeuristicModeCountWithoutRotation;

@interface HeuristicHelper : NSObject

+ (UIImage*) crop: (UIImage*) originImage
withHeuristicMode: (HeuristicMode) mode;
+ (CGSize) getMaxSizeAfterRotate: (CGSize) frame angleInRadian: (CGFloat) angle;
+ (NSArray<UIImage*>*) generateHeuristicImagesWithImage: (UIImage*) image;
+ (NSArray<NSNumber*>*) inferImages: (NSArray<UIImage*>*) images
                       withModelKey: (NSString*) modelKey;

+ (NSString*) getNameWithMode: (HeuristicMode) mode;

+ (UIColor*) colorForHeuristicMode: (HeuristicMode) mode;
+ (CGFloat) radianForHeuristicMode: (HeuristicMode) mode;
@end

NS_ASSUME_NONNULL_END
