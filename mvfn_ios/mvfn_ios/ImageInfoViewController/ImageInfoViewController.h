//
//  ImageInfo.h
//  mvfn_ios
//
//  Created by Chris Fan on 8/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageStore.h"

NS_ASSUME_NONNULL_BEGIN

@interface ImageInfoViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;


@property NSMutableArray* infoContents;
@property InferredImageData* imageData;

- (void) setData:(InferredImageData *) imageData;
@end

NS_ASSUME_NONNULL_END
