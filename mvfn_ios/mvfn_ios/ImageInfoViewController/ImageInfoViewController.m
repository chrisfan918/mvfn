//
//  ImageInfo.m
//  mvfn_ios
//
//  Created by Chris Fan on 8/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import "ImageInfoViewController.h"
#import "InfoTableViewCell.h"

@interface ImageInfoViewController () <UITableViewDelegate, UITableViewDataSource>

@end

@implementation ImageInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.infoContents = [[NSMutableArray alloc] init];
    
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
    UINib* InfoTableViewCellNib = [UINib nibWithNibName:@"InfoTableViewCell" bundle:nil];
    [self.tableView registerNib:InfoTableViewCellNib forCellReuseIdentifier:@"InfoTableViewCell"];
    
    [self.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [self reload];
}

- (void) setData:(InferredImageData *) imageData {
    [self setImageData:imageData];
    [self reload];
}

-(void) reload {
    
    [self.infoContents removeAllObjects];
    
    // load data
    CGSize size = [self.imageData->image size];
    NSInteger height = (NSInteger) size.height;
    NSInteger width = (NSInteger) size.width;
    [self.infoContents addObject:@{
                                   @"key":@"width",
                                   @"value":[NSString stringWithFormat:@"%ld", (long)width]
                                   }];
    [self.infoContents addObject:@{
                                   @"key":@"height",
                                   @"value":[NSString stringWithFormat:@"%ld", (long)height]
                                   }];
    [self.tableView reloadData];
    
    self.imageView.image = self.imageData->image;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.infoContents count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    InfoTableViewCell* cell = (InfoTableViewCell*) [tableView dequeueReusableCellWithIdentifier:@"InfoTableViewCell" forIndexPath:indexPath];
    NSDictionary* dict = [self.infoContents objectAtIndex:indexPath.row];
    [cell setContentWithKey:(NSString*) [dict objectForKey:@"key"]
                      value:(NSString*) [dict objectForKey:@"value"]];
    return cell;
}

@end
