//
//  InferredCollectionViewCell.h
//  mvfn_ios
//
//  Created by Chris Fan on 12/2/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface InferredCollectionViewCell : UICollectionViewCell
{
    
    __weak IBOutlet UIImageView *imageView;
}

- (void) setImage: (UIImage*) image;

+ (CGSize) size;
@end

NS_ASSUME_NONNULL_END
