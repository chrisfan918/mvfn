//
//  InferredCollectionViewCell.m
//  mvfn_ios
//
//  Created by Chris Fan on 12/2/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import "InferredCollectionViewCell.h"
#import "MVFNModel.h"

@implementation InferredCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void) setImage: (UIImage*) image {
    [imageView setContentMode:UIViewContentModeScaleAspectFit];
    imageView.image = image;
    imageView.layer.cornerRadius = 10;
    [imageView setClipsToBounds:true];
}

+ (CGSize) size {
    CGFloat width = ([[UIScreen mainScreen] bounds].size.width) / 2;
    return CGSizeMake(width, width);
}
@end
