//
//  LiveCamViewController.h
//  mvfn_ios
//
//  Created by Chris Fan on 6/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveCamViewController : UIViewController
@property UIImage* currentFrame;
@property AVCaptureSession* session;
@property AVCaptureConnection* connection;
@property AVCaptureVideoDataOutput* videoOutput;
@property AVCaptureVideoPreviewLayer* videoPreviewLayer;
@property AVCapturePhotoOutput* photoOutput;
@property double labelLastUpdatedAt;
@property (atomic) NSString* modelKey;
@property NSArray<NSString* >* supportedModels;
@property (atomic) BOOL isInfering;
@property (atomic) BOOL shouldInfer;

@property (weak, nonatomic) IBOutlet UIView *previewView;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UILabel *labelView;
@property (weak, nonatomic) IBOutlet UIButton *shutterButton;

@end

NS_ASSUME_NONNULL_END
