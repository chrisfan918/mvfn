//
//  LiveCamViewController.m
//  mvfn_ios
//
//  Created by Chris Fan on 6/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import "LiveCamViewController.h"
#import "LiveCamViewController+pickerView.h"
#import "MVFNModel.h"
#import "ImageStore.h"
#import <AVFoundation/AVFoundation.h>

@interface LiveCamViewController () <AVCaptureVideoDataOutputSampleBufferDelegate, AVCapturePhotoCaptureDelegate>

@end

@implementation LiveCamViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"LiveCam";
    
    self.isInfering = false;
    self.labelLastUpdatedAt = -1;
    self.modelKey = kMVFN096;
    self.supportedModels = @[kMVFN096, kMVFN160, kMVFN224];
    
    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;
    [self.pickerView reloadAllComponents];
    
    AVCaptureDevice* captureDevice = [AVCaptureDevice defaultDeviceWithMediaType: AVMediaTypeVideo];
    AVCaptureDeviceInput* input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:nil];
    self.session = [[AVCaptureSession alloc] init];
    self.session.sessionPreset = AVCaptureSessionPresetPhoto;
    [self.session addInput:input];

    self.videoPreviewLayer = [AVCaptureVideoPreviewLayer layerWithSession:self.session];
    self.videoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    self.videoPreviewLayer.frame = self.previewView.layer.bounds;
    [self.previewView.layer addSublayer:self.videoPreviewLayer];
    
    self.videoOutput = [[AVCaptureVideoDataOutput alloc] init];
    [self.videoOutput setSampleBufferDelegate:self queue:dispatch_queue_create("sample buffer", nil)];
    if ([self.session canAddOutput:self.videoOutput]) {
        [self.session addOutput:self.videoOutput];
    } else {
        NSLog(@"cannot add AVCaptureVideoDataOutput to AVCaptureSession");
    }
    self.connection = [self.videoOutput connectionWithMediaType:AVMediaTypeVideo];
    self.connection.videoOrientation = AVCaptureVideoOrientationPortrait;
    
    self.photoOutput = [[AVCapturePhotoOutput alloc] init];
    [self.photoOutput setLivePhotoCaptureEnabled:false];
    [self.photoOutput setHighResolutionCaptureEnabled:false];
    if ([self.session canAddOutput:self.photoOutput]) {
        [self.session addOutput:self.photoOutput];
    } else {
        NSLog(@"cannot add AVCapturePhotoOuput to AVCaptureSession");
    }
    self.shouldInfer = false;
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.session stopRunning];
    self.shouldInfer = false;
    NSLog(@"viewWillDisappear: session stopRunning");
}

- (void)viewWillAppear:(BOOL)animated {
    if (self.session and ![self.session isRunning]) {
        [self.session startRunning];
        NSLog(@"viewWillAppear: session startRunning");
    }
    self.shouldInfer = false;
}
- (void)viewDidAppear:(BOOL)animated {
    self.shouldInfer = true;
}

- (UIImage*) imageFromSampleBuffer: (CMSampleBufferRef) sampleBuffer {
    auto imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    return [self imageFromCVPixelBuffer:imageBuffer];
}

- (UIImage*) imageFromCVPixelBuffer: (CVPixelBufferRef) cvPixelBuffer {

    auto context = [CIContext contextWithOptions:nil];
    auto ciImage = [CIImage imageWithCVPixelBuffer:cvPixelBuffer];
    auto cgImage = [context createCGImage:ciImage fromRect: [ciImage extent]];
    auto uiImage = [UIImage imageWithCGImage:cgImage];
    CGImageRelease(cgImage);
    return uiImage;
}

- (void)captureOutput:(AVCaptureOutput *)output didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection {
    if (connection )
    if (self.isInfering) {
        // skip frame if isInferring
        NSLog(@"Inferring in progress, skipping new frame");
        return;
    }
    auto uiImage = [self imageFromSampleBuffer:sampleBuffer];
    
    self.isInfering = true;
    
    dispatch_async(dispatch_get_main_queue(), ^(void){
        auto inferStartAt = [[NSDate new] timeIntervalSince1970];
        auto score = [MVFNModel inferWithImage:uiImage withModel:self.modelKey];
        auto inferEndAt = [[NSDate new] timeIntervalSince1970];
        auto inferDuration = inferEndAt - inferStartAt;
        auto inferPS = 1 / inferDuration;
        
        double fps = 0;
        if (self.labelLastUpdatedAt < 0) {
            self.labelLastUpdatedAt = [[NSDate new] timeIntervalSince1970];
        } else {
            auto now = [[NSDate new] timeIntervalSince1970];
            fps = 1/ (now - self.labelLastUpdatedAt);
            self.labelLastUpdatedAt = now;
        }
        [self.labelView setText:[NSString stringWithFormat:@"%@ score: %@, fps: %.2f, IPS: %.2f", self.modelKey, score, fps, inferPS]];
        self.isInfering = false;
    });
}

- (IBAction)onShutterButtonTap:(id)sender {
    [[self.photoOutput connectionWithMediaType:AVMediaTypeVideo] setVideoOrientation: [self.connection videoOrientation]];
    AVCapturePhotoSettings* setting = [AVCapturePhotoSettings photoSettings];
    [self.photoOutput capturePhotoWithSettings:setting delegate:self];
}


- (void)captureOutput:(AVCapturePhotoOutput *)output didFinishProcessingPhoto:(AVCapturePhoto *)photo error:(NSError *)error {
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
    }
    
    dispatch_async(dispatch_get_main_queue(), ^(void){
        NSNumber* orientation = [photo.metadata valueForKey: (NSString*)kCGImagePropertyOrientation];
        NSInteger orientatinInt = [orientation integerValue];
        UIImageOrientation uiImageOrientation = UIImageOrientationUp;
        
        // correct orientation from sensor to videoOutput orientation
        NSLog(@"UIImageOrientation: %ld", uiImageOrientation);
        switch (orientatinInt) {
            case kCGImagePropertyOrientationUp:
                NSLog(@"kCGImagePropertyOrientationUp");
                uiImageOrientation = UIImageOrientationUp;
                break;
            case kCGImagePropertyOrientationDown:
                NSLog(@"kCGImagePropertyOrientationDown");
                uiImageOrientation = UIImageOrientationDown;
                break;
            case kCGImagePropertyOrientationLeft:
                NSLog(@"kCGImagePropertyOrientationLeft");
                uiImageOrientation = UIImageOrientationLeft;
                break;
            case kCGImagePropertyOrientationRight:
                NSLog(@"kCGImagePropertyOrientationRight");
                uiImageOrientation = UIImageOrientationRight;
                break;
            case kCGImagePropertyOrientationUpMirrored:
                NSLog(@"kCGImagePropertyOrientationUpMirrored");
                uiImageOrientation = UIImageOrientationUpMirrored;
                break;
            case kCGImagePropertyOrientationDownMirrored:
                NSLog(@"kCGImagePropertyOrientationDownMirrored");
                uiImageOrientation = UIImageOrientationDownMirrored;
                break;
            case kCGImagePropertyOrientationLeftMirrored:
                NSLog(@"kCGImagePropertyOrientationLeftMirrored");
                uiImageOrientation = UIImageOrientationLeftMirrored;
                break;
            case kCGImagePropertyOrientationRightMirrored:
                NSLog(@"kCGImagePropertyOrientationRightMirrored");
                uiImageOrientation = UIImageOrientationRightMirrored;
                break;
        }
        auto uiImage = [UIImage imageWithCGImage:[photo CGImageRepresentation] scale:1.0 orientation:uiImageOrientation];
        
        // adjust for device orientation
        UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
        NSLog(@"UIDeviceOrientation: %ld", deviceOrientation);
        switch (deviceOrientation) {
            case UIDeviceOrientationLandscapeLeft:
                NSLog(@"UIDeviceOrientationLandscapeLeft");
                uiImage = [UIImage imageWithCGImage:[uiImage CGImage] scale:1.0 orientation:UIImageOrientationLeft];
                break;
            case UIDeviceOrientationLandscapeRight:
                NSLog(@"UIDeviceOrientationLandscapeRight");
                uiImage = [UIImage imageWithCGImage:[uiImage CGImage] scale:1.0 orientation:UIImageOrientationRight];
                break;
            default:
                break;
        }
        InferredImageData* data = [InferredImageData initWithImage:uiImage];
        [ImageStore.shareInstance.imageData addObject:data];
    });
}
@end
