//
//  Config.h
//  mvfn_ios
//
//  Created by Chris Fan on 21/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Config : NSObject

+(UIColor *) colorForBackground;

@end

NS_ASSUME_NONNULL_END
