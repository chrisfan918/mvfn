//
//  InfoTableRowTableViewCell.m
//  mvfn_ios
//
//  Created by Chris Fan on 7/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import "InfoTableViewCell.h"
#import "Config.h"

@implementation InfoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor = [Config colorForBackground];
}

- (void) setContentWithKey: (NSString*) key
                     value: (NSString*) value {
    [self.keyLabelView setText:key];
    [self.keyLabelView setTextColor: [UIColor whiteColor]];
    [self.valueLabelView setText:value];
    [self.valueLabelView setTextColor: [UIColor whiteColor]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
