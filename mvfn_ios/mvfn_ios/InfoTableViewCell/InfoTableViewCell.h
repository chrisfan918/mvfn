//
//  InfoTableRowTableViewCell.h
//  mvfn_ios
//
//  Created by Chris Fan on 7/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface InfoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *keyLabelView;
@property (weak, nonatomic) IBOutlet UILabel *valueLabelView;

- (void) setContentWithKey: (NSString*) key
                     value: (NSString*) value;
@end

NS_ASSUME_NONNULL_END
