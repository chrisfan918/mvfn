//
//  ViewFindingHelper.h
//  mvfn_ios
//
//  Created by Chris Fan on 18/3/2019.
//  Copyright © 2019 Chris Fan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

// kAspectRatio_{w}_{h}
NSString* const kAspectRatio_Original = @"original";
NSString* const kAspectRatio_1_1 = @"1:1";
NSString* const kAspectRatio_2_3 = @"2:3";
NSString* const kAspectRatio_3_4 = @"3:4";
NSString* const kAspectRatio_4_5 = @"4:5";
NSString* const kAspectRatio_16_9 = @"16:9";
NSString* const kAspectRatio_3_2 = @"3:2";
NSString* const kAspectRatio_4_3 = @"4:3";
NSString* const kAspectRatio_5_4 = @"5:4";
NSString* const kAspectRatio_9_16 = @"9:16";

@interface VFCandidate : NSObject
@property CGRect rect;
@property NSNumber* score;
@end

@interface VFOption: NSObject
@property NSString* aspectRatio;
@property CGFloat minScale;
@property CGFloat maxScale;
@property  CGFloat stepScale;
@property NSString* modelKey;

+ (instancetype) initWithDefault;
@end

@interface ViewFindingHelper : NSObject

+ (NSArray<NSString*>*) kAspectRatios;

+ (NSArray<VFCandidate*>*) findViewInImage: (UIImage*) originalImage
                                withOption: (VFOption*) option
              progressCallBack: (void (^) (int, int)) progressCallBack;


+ (UIImage*) cropImage: (UIImage *) originalImage
                  rect: (CGRect) rect;

+ (UIImage*) preprocessImage: (UIImage*) originalImage;
@end

NS_ASSUME_NONNULL_END
