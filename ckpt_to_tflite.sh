# main
# CKPT_PATH=/Users/kyfan/work/view-finding-network/models/mvfn_224_revamp/snapshots/mvfn_224_201903041844_revamp_L_0_1478_acc_0_9102.ckpt-30
# INPUT_IMAGE_SIZE=224

# CKPT_PATH=/Users/kyfan/work/view-finding-network/models/mvfn_160_revamp/snapshots/mvfn_160_201903050825_revamp_L_0_1278_acc_0_9410.ckpt-29
# INPUT_IMAGE_SIZE=160

CKPT_PATH=/Users/kyfan/work/view-finding-network/models/mvfn_096_revamp/snapshots/mvfn_96_201903041609_revamp_L_0_2222_acc_0_8572.ckpt-24
INPUT_IMAGE_SIZE=96

INPUT_BATCH=11

OUTPUT_PATH="${CKPT_PATH}_BATCH_${INPUT_BATCH}"
FROZEN_PB_PATH="$OUTPUT_PATH.pb"
TFLITE_PATH="$OUTPUT_PATH.tflite"
CKPT_BASE_PATH=`dirname $CKPT_PATH`
TB_PATH="${CKPT_BASE_PATH}/`basename $CKPT_PATH`_tb_log"
TFLITE_VIS_HTML_PATH="$TFLITE_PATH.html"
TF_PATH=/Users/kyfan/work/tensorflow_1.13
PWD=`pwd`

# # # ckpt to tflite
python ckpt_to_tflite.py \
    --ckpt $CKPT_PATH \
    --input_node "input_image" \
    --input_batch $INPUT_BATCH \
    --input_dim ${INPUT_IMAGE_SIZE} \
    --output_node "ranker/score_func" \
    --output_path $OUTPUT_PATH

# diff tflite & pb
$TF_PATH/bazel-bin/tensorflow/lite/testing/tflite_diff_example_test \
    --tensorflow_model=${FROZEN_PB_PATH} \
    --tflite_model=${TFLITE_PATH} \
    --input_layer="input_image" \
    --input_layer_type=float \
    --input_layer_shape=${INPUT_BATCH},${INPUT_IMAGE_SIZE},${INPUT_IMAGE_SIZE},3 \
    --output_layer="ranker/score_func"

mkdir -p $TB_PATH
# pb to tensorboard
python ${TF_PATH}/tensorflow/python/tools/import_pb_to_tensorboard.py \
    --model_dir=${FROZEN_PB_PATH} \
    --log_dir=$TB_PATH

# visualize tflite
$TF_PATH/bazel-bin/tensorflow/lite/tools/visualize ${TFLITE_PATH} ${TFLITE_VIS_HTML_PATH}

# trash
# bazel run tensorflow/lite/tools:visualize -- ${TFLITE_PATH} ${TFLITE_VIS_HTML_PATH}

# bazel run tensorflow/lite/testing:tflite_diff_example_test -- \
#     --tensorflow_model=${FROZEN_PB_PATH} \
#     --tflite_model=${TFLITE_PATH} \
#     --input_layer="input_image" \
#     --input_layer_type=float \
#     --input_layer_shape=1,${INPUT_IMAGE_SIZE},${INPUT_IMAGE_SIZE},3 \
#     --output_layer="ranker/score_func"

# freeze graph
# python ${TF_PATH}/tensorflow/python/tools/freeze_graph.py \
#     --input_binary=true \
#     --input_meta_graph=${CKPT_META_PATH} \
#     --input_checkpoint=${CKPT_WEIGHT_PATH} \
#     --output_graph=${FROZEN_PB_PATH} \
#     --output_node_names="ranker/score_func"

# convert pb to tflite
# bazel run tensorflow/lite/toco:toco -- \
#     --input_file=${FROZEN_PB_PATH} \
#     --input_format=TENSORFLOW_GRAPHDEF \
#     --output_format=TFLITE \
#     --output_file=${TFLITE_PATH} \
#     --inference_type=FLOAT \
#     --inference_input_type=FLOAT \
#     --input_arrays=input_image \
#     --output_arrays="ranker/score_func" \
#     --input_shapes=1,${INPUT_IMAGE_SIZE},${INPUT_IMAGE_SIZE},3 \
#     --allow_custom_ops

# tflite_convert \
#     --graph_def_file=/Users/kyfan/work/view-finding-network/models_lite/mvfn_224_static_opt.pb \
#     --output_file=/Users/kyfan/work/view-finding-network/models_lite/mvfn_224_static_opt_tflite_convert.pb \
#     --input_arrays=input_image \
#     --output_arrays=ranker/score_func 

# tflite_convert \
#     --output_file=/Users/kyfan/work/view-finding-network/models_lite/mvfn_096_sm.tflite \
#     --saved_model_dir=/Users/kyfan/work/view-finding-network/models_lite/mvfn_096_sm \
#     --input_arrays=input_image \
#     --output_arrays=ranker/score_func \
#     --input_shape=1,96,96,3 

# python ~/work/tensorflow_1.13/tensorflow/python/tools/freeze_graph.py \
#     --input_binary=true \
#     --input_graph=/Users/kyfan/work/view-finding-network/models_lite/mvfn_096_sm/saved_model.pb \
#     --input_checkpoint=/Users/kyfan/work/view-finding-network/models_lite/mvfn_096_sm/variables \
#     --output_graph=/Users/kyfan/work/view-finding-network/models_lite/mvfn_096_static.pb \
#     --output_node_names="ranker/score_func"




# python ckpt_to_tflite.py \
#     --ckpt models_lite/mvfn_224/mvfn_224 \
#     --input_node "input_image" \
#     --input_batch 11 \
#     --input_dim 224 \
#     --quant true \
#     --output_node "ranker/score_func" \
#     --output_path models_lite/mvfn_224/mvfn_224_BATCH_11_QUANT