import tensorflow as tf
import mvfn
import argparse

def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')
parser = argparse.ArgumentParser()

parser.add_argument("--ckpt",help="path to *.ckpt file", type=str)
parser.add_argument("--quant",help="use quantization", type=str2bool)
parser.add_argument("--input_node",help="name of input node of model", type=str)
parser.add_argument("--input_batch", help="batch for input size", type=int, default=1)
parser.add_argument("--input_dim",help="input dimension of model", type=int)
parser.add_argument("--output_node",help="name of output node of model", type=str)
parser.add_argument("--output_path",help="<output_path>.pb, <output_path>.tflite", type=str)

args = parser.parse_args()

weight_path = args.ckpt
meta_path = args.ckpt + ".meta"
input_node_names = [args.input_node]
input_shape = [args.input_batch, args.input_dim, args.input_dim, 3]
input_shapes={}
input_shapes[args.input_node] = input_shape
output_node_names = [args.output_node]
pb_path = args.output_path + ".pb"
tflite_path = args.output_path + ".tflite"

with tf.Session() as sess:

    # Restore the graph
    # saver = tf.train.import_meta_graph(meta_path)

    # build graph

    image_placeholder = tf.placeholder(
      dtype=tf.float32,
      shape=input_shape,
      name="input_image")
    embedding_dim = 1000
    feature_vec = mvfn.load_mobilenet_v2(image_placeholder, is_training=False)
    with tf.variable_scope("ranker") as scope:
      score_func = mvfn.build_mvfn(feature_vec)
    sess.run(tf.global_variables_initializer())
    
    saver = tf.train.Saver(tf.global_variables())

    # Load weights
    saver.restore(sess, weight_path)

    # Freeze the graph
    frozen_graph_def = tf.graph_util.convert_variables_to_constants(
        sess,
        sess.graph_def,
        output_node_names)

    # Save the frozen graph
    with open(pb_path, 'wb') as f:
      f.write(frozen_graph_def.SerializeToString())

if tf.__version__[:4] == "1.13":
  converter = tf.lite.TFLiteConverter.from_frozen_graph(
    pb_path, input_node_names, output_node_names, 
      input_shapes=input_shapes)
  if args.quant:
    converter.optimizations = [tf.lite.Optimize.OPTIMIZE_FOR_SIZE]
else:
  converter = tf.contrib.lite.TFLiteConverter.from_frozen_graph(
    pb_path, input_node_names, output_node_names, 
      input_shapes=input_shapes)
  if args.quant:
    converter.post_training_quantize = True
tflite_model = converter.convert()
open(tflite_path, "wb").write(tflite_model)