
# %% ALEXNET
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import mvfn
from IPython.display import display, Image
import numpy as np
import tensorflow as tf
import tabulate
import scipy
import skimage.io as io
import skimage.transform as transform
import os
from os.path import join
import network as nw
import json
import time
import string
from interpreters import *

# %% setup networks
alexnet = AlexNet('./models/model-spp-max/model-spp-max')
networks = {
    "alexnet": alexnet,
    # "mvfn_096": MVFN("models/mvfn_096_revamp/snapshots/mvfn_96_201903041609_revamp_L_0_2222_acc_0_8572.ckpt-24", 96),
    # "mvfnpb_096": MVFNPB("models/mvfn_096_revamp/snapshots/mvfn_96_201903041609_revamp_L_0_2222_acc_0_8572.ckpt-24.pb", 96),
    "mvfnl_096": MVFNLite("models/mvfn_096_revamp/snapshots/mvfn_96_201903041609_revamp_L_0_2222_acc_0_8572.ckpt-24.tflite", 96),
    # "mvfn_160": MVFN("models/mvfn_160_revamp/snapshots/mvfn_160_201903050825_revamp_L_0_1278_acc_0_9410.ckpt-29", 160),
    # "mvfnpb_160": MVFNPB("models/mvfn_160_revamp/snapshots/mvfn_160_201903050825_revamp_L_0_1278_acc_0_9410.ckpt-29.pb", 160),
    "mvfnl_160": MVFNLite("models/mvfn_160_revamp/snapshots/mvfn_160_201903050825_revamp_L_0_1278_acc_0_9410.ckpt-29.tflite", 160),
    # "mvfn_224": MVFN("models/mvfn_224_revamp/snapshots/mvfn_224_201903041844_revamp_L_0_1478_acc_0_9102.ckpt-30", 224),
    # "mvfnpb_224": MVFNPB("models/mvfn_224_revamp/snapshots/mvfn_224_201903041844_revamp_L_0_1478_acc_0_9102.ckpt-30.pb", 224),
    "mvfnl_224": MVFNLite("models/mvfn_224_revamp/snapshots/mvfn_224_201903041844_revamp_L_0_1478_acc_0_9102.ckpt-30.tflite", 224),
}


# %%
imageDir = "test_images/test"

imageFilenames = [join(imageDir, i) for i in os.listdir(
    imageDir) if os.path.isfile(join(imageDir, i)) and string.lower(i).endswith(('.png', '.jpg'))]
imageFilenames = sorted(imageFilenames)

img_dict = dict([(img,  io.imread(img)) for img in imageFilenames])

def score(net, img_dict):
    return dict(zip(img_dict.keys(), net.run(img_dict.values())))

scores = {}
for name, net in networks.items():
    scores[name] = score(net, img_dict)

sortedScores = {}
for name, s in scores.items():
    sortedScores[name] = sorted(s, reverse=True)

# display images
origin_filename = imageFilenames[0]
origin_img = mpimg.imread(origin_filename)
for img_name in imageFilenames[1:]:
    header = ["img"]
    origin_r = [imageFilenames[0]]
    r = [img_name]
    for net in sorted(scores.keys()):
        s = scores[net]
        header.append(net)
        origin_score = s[origin_filename]
        origin_rank = sorted(s.values(), reverse=True).index(origin_score) + 1

        img_score = s[img_name]
        img_rank = sorted(s.values(), reverse=True).index(img_score) + 1

        origin_arrow = "<-" if origin_score > img_score else ""
        img_arrow = "<-" if origin_score < img_score else ""

        origin_r.append("{:.4f} ({}) {}".format(
            origin_score, origin_rank, origin_arrow))
        r.append("{:.4f} ({}) {}".format(img_score, img_rank, img_arrow))

    table = []
    table.append(header)
    table.append(origin_r)
    table.append(r)

    f, ax = plt.subplots(1, 2, figsize=(15, 15))
    ax[0].imshow(origin_img)
    ax[1].imshow(mpimg.imread(img_name))
    plt.show()
    print origin_filename, img_name
    print tabulate.tabulate(table)
    print ""

#%%
# table summary
table = []
header = ["img"] + sorted(scores.keys())
table.append(header)
for img_name in imageFilenames:
    r = [img_name]
    for net in sorted(scores.keys()):
        s = scores[net]
        img_score = s[img_name]
        img_rank = sorted(s.values(), reverse=True).index(img_score) + 1
        r.append("{:.4f} ({}) {}".format(img_score, img_rank, img_arrow))
    table.append(r)

print tabulate.tabulate(table)

# Scores statistics
#%%
table = []
header = ["stat"] + sorted(scores.keys())
table.append(header)
range_ = ["range"]
mean = ["mean"]
sd = ["s.d."]
for net in sorted(scores.keys()):
    nps = np.asarray(scores[net].values())
    range_.append("{:.4f}".format(nps.max() - nps.min()))
    mean.append("{:.4f}".format(nps.mean()))
    sd.append("{:.4f}".format(nps.std()))
    
table.append(range_)
table.append(mean)
table.append(sd)

print tabulate.tabulate(table)

# Rank Correlation
#%%
ranks = {}
for (net, ss) in scores.items():
    sorted_ss = sorted(ss.values(), reverse=True)
    ranks[net] = [sorted_ss.index(ss[x]) for x in imageFilenames]
nets = sorted(ranks.keys())
corr = {}
for a in nets:
    if a not in corr:
        corr[a] = {}
    for b in nets:
        footrule = 0
        ranka = ranks[a]
        rankb = ranks[b]
        for i in range(len(ranka)):
            footrule += abs(ranka[i] - rankb[i])
        corr[a][b] = footrule

# print ranks
#%%
table = []
for n in nets:
    table.append([n] + ranks[n])
print tabulate.tabulate(table)

# print correlation table
table = [[""]+nets]
for a in nets:
    row = [a]
    for b in nets:
        row.append(format(corr[a][b]))
    table.append(row)
print tabulate.tabulate(table)
"""
for each folder in  test_images/dim_224
    for each net
        infer each image
    build rank matrix by net

    sum rank base
"""

nets = sorted(ranks.keys())
corr_sum = {}
for a in nets:
    if a not in corr:
        corr[a] = {}
    for b in nets:
        corr[a][b] = 0
